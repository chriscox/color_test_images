//
// ccox - I had this marked as public domain code, but can't find the source or author anywhere
//
///////////////////////////////////////////////////////////////////////////////
//
// CIECAM97 Conversions
//

#include <iostream>
#include <cmath>
#include "ciecam97.h"

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declaration of Helper Functions
//

TRISTIMULUS MultiplyPostAdaption(TRISTIMULUS input, SCALAR Fl         );
TRISTIMULUS MultiplyByMhMbInv   (TRISTIMULUS input, SCALAR y          );
TRISTIMULUS MultiplyByMbMhInv   (TRISTIMULUS input, SCALAR y          );
TRISTIMULUS MultiplyByMb        (TRISTIMULUS input                    );
TRISTIMULUS MultiplyByMbInv     (TRISTIMULUS input                    );
TRISTIMULUS MultiplyByDp        (TRISTIMULUS input,
								 TRISTIMULUS white, SCALAR D, SCALAR p);

/* -------------------------------------------------------------- */

inline
SCALAR sgn(SCALAR input)
{
	return (input<0) ? -1.0 : 1.0;
}

/* -------------------------------------------------------------- */

SCALAR CalculatePrime(SCALAR in)
{
	SCALAR out;

	if ( (in-1.0) < 0.0) 
		out = -100.0*pow((2.0-2.0*in)/(39.0+in), 1.0/0.73);
	else
		out =  100.0*pow((2.0*in-2.0)/(41.0-in), 1.0/0.73);

	return out;
}

/* -------------------------------------------------------------- */

SCALAR CalculateE(SCALAR h)
{
	SCALAR result;
	SCALAR e1, e2, h1, h2;
	
	if (h <= 20.14)
	{
		e1 = 0.8565;
		e2 = 0.8;
		h1 = 0.0;
		h2 = 20.14;
	}
	else if (h <= 90.0) 
	{
		e1 = 0.8;
		e2 = 0.7;
		h1 = 20.14;
		h2 = 90.0;
	}
	else if (h <= 164.25)
	{
		e1 = 0.7;
		e2 = 1.0;
		h1 = 90.0;
		h2 = 164.25;
	}
	else if (h <= 237.53)
	{
		e1 = 1.0;
		e2 = 1.2;
		h1 = 164.25;
		h2 = 237.53;
	}
	else
	{
		e1 = 1.2;
		e2 = 0.8565;
		h1 = 237.53;
		h2 = 360.0;
	}


	result = e1 + (e2-e1)*(h-h1)/(h2-h1);

	return result;
}

/* -------------------------------------------------------------- */

///////////////////////////////////////////////////////////////////////////////
//
// Main Conversion Functions
//

TRISTIMULUS XYZ2JCH(TRISTIMULUS xyz, SCENE sn, VIEW vw)
{
	TRISTIMULUS jch;
	TRISTIMULUS rgb, rgbW, rgbp, rgbpW, rgba, rgbaW, rgbc, rgbcW;
	SCALAR      D, p, k, Fl, n, Nbb, Ncb, z, a, b, h, e, A, Aw, s;

	rgb   = MultiplyByMb(xyz );
	rgbW  = MultiplyByMb(sn.W);

	D     = vw.F - (vw.F / (1.0 + 2.0*pow(sn.La,0.25) + sn.La * sn.La / 300.0) );
	p     = pow(rgbW.b, 0.0834);

	rgbc  = MultiplyByDp(rgb , rgbW, D, p);
	rgbcW = MultiplyByDp(rgbW, rgbW, D, p);

	rgbp  = MultiplyByMhMbInv(rgbc ,  xyz.y);
	rgbpW = MultiplyByMhMbInv(rgbcW, sn.W.y);

	k     = 1.0 / (5.0 * sn.La + 1.0);
	Fl    = 0.2*pow(k,4.0)*5.0*sn.La + 0.1*pow(1.0-pow(k,4.0),2.0)*pow(5.0*sn.La,1.0/3.0);
    n     = sn.Yb / sn.W.y;

	Nbb   = 0.725*pow(1.0/n,0.2);
	Ncb   = Nbb;
	z     = 1.0 + vw.Fll*pow(n,0.5);

	rgba  = MultiplyPostAdaption(rgbp , Fl);
	rgbaW = MultiplyPostAdaption(rgbpW, Fl);

	a     = rgba.r - 12.0*rgba.g/11.0 + rgba.b/11.0;
    b     = (1.0/9.0)*(rgba.r + rgba.g - 2.0*rgba.b);

    h     = (180.0/M_PI)*atan2(b,a);
	h     = (h<0.0) ? h+360.0 : h;

	jch.h = h;

	e     = CalculateE(h);

	A     = (2.0*rgba.r  + rgba.g  + (1.0/20.0)*rgba.b  - 2.05)*Nbb;
    Aw    = (2.0*rgbaW.r + rgbaW.g + (1.0/20.0)*rgbaW.b - 2.05)*Nbb;

	jch.j = 100.0 * pow(A/Aw,vw.C*z);

	s     = (50.0*sqrt(a*a+b*b)*100.0*e*(10.0/13.0)*vw.Nc*Ncb) /
		    (rgba.r + rgba.g + (21.0/20.0)*rgba.b);

	jch.c = 2.44 * pow(s,0.69) * pow(jch.j/100.0,0.67*n) * (1.64-pow(0.29,n));

	return jch;
}

/* -------------------------------------------------------------- */

TRISTIMULUS JCH2XYZ(TRISTIMULUS jch, SCENE sn, VIEW vw)
{
	TRISTIMULUS xyz;
	TRISTIMULUS rgbW, rgbp, rgbpW, rgba, rgbaW, rgbc, rgbcW, me;
	SCALAR      D, p, k, Fl, n, Nbb, Ncb, z, a, b, h, e, A, Aw, s, sign, Yp, Yc;

	rgbW   = MultiplyByMb(sn.W);

	D      = vw.F - (vw.F / (1.0 + 2.0*pow(sn.La,0.25) + sn.La * sn.La / 300.0) );
	p      = pow(rgbW.b, 0.0834);

	rgbcW  = MultiplyByDp(rgbW, rgbW, D, p);

	rgbpW  = MultiplyByMhMbInv(rgbcW, sn.W.y);

	k      = 1.0 / (5.0 * sn.La + 1.0);
	Fl     = 0.2*pow(k,4.0)*5.0*sn.La + 0.1*pow(1.0-pow(k,4.0),2.0)*pow(5.0*sn.La,1.0/3.0);
    n      = sn.Yb / sn.W.y;

	Nbb    = 0.725*pow(1.0/n,0.2);
	Ncb    = Nbb;
	z      = 1.0 + vw.Fll*pow(n,0.5);

	rgbaW  = MultiplyPostAdaption(rgbpW, Fl);

    Aw     = (2.0*rgbaW.r + rgbaW.g + (1.0/20.0)*rgbaW.b - 2.05)*Nbb;

	h      = jch.h;

	e      = CalculateE(h);

	A      = pow(jch.j/100.0, 1.0/(vw.C*z)) * Aw;

	s      = pow(jch.c, 1.0/0.69) / pow(2.44*pow(jch.j/100.0, 0.67*n)*(1.64-pow(0.29,n)), 1.0/0.69);

	sign   = (h>90.0 && h<=270.0) ? -1.0 : 1.0;

	a      = (s*(A/Nbb + 2.05)) / (sign*sqrt(1.0+pow(tan(M_PI*h/180.0),2.0))*50000.0*e*vw.Nc*Ncb/13.0
		                           + (s*(11.0/23.0 + (108.0/23.0)*tan(M_PI*h/180.0))));
	b      = a*tan(M_PI*h/180.0);

    rgba.r = (20.0/61.0)*((A/Nbb)+2.05)+(41.0/61.0)*(11.0/23.0)*a+(288.0/61.0)*(  1.0/23.0)*b;
    rgba.g = (20.0/61.0)*((A/Nbb)+2.05)-(81.0/61.0)*(11.0/23.0)*a-(288.0/61.0)*(  1.0/23.0)*b;
    rgba.b = (20.0/61.0)*((A/Nbb)+2.05)-(20.0/61.0)*(11.0/23.0)*a-( 20.0/61.0)*(315.0/23.0)*b;

	rgbp.r = CalculatePrime(rgba.r);
	rgbp.g = CalculatePrime(rgba.g);
	rgbp.b = CalculatePrime(rgba.b);

	rgbc   = MultiplyByMbMhInv(rgbp, Fl);

	Yc     = 0.43231*rgbc.r + 0.51836*rgbc.g + 0.04929*rgbc.b;

	me.r   =  (rgbc.r/Yc) / (D*(1.0/rgbW.r) + 1.0 - D);
	me.g   =  (rgbc.g/Yc) / (D*(1.0/rgbW.g) + 1.0 - D);
	me.b   = ((rgbc.b/Yc) / (abs(rgbc.b/Yc))) * pow(abs(rgbc.b/Yc), 1.0/p) / 
		                    pow(D*(1.0/pow(rgbW.b,p) + 1.0 - D), 1.0/p);

	Yp	   = 0.43231*me.r*Yc + 0.51836*me.g*Yc + 0.04929*me.b*Yc;

	me.r   = me.r*Yc;
	me.g   = me.g*Yc;
	me.b   = me.b*Yc / pow(Yp/Yc, 1.0/p - 1.0);

	xyz    = MultiplyByMbInv(me);

	return xyz;
}

///////////////////////////////////////////////////////////////////////////////
//
// Helper Functions
//

TRISTIMULUS MultiplyPostAdaption(TRISTIMULUS input, SCALAR Fl)
{
	TRISTIMULUS result;

	result.r = (40.0*pow(Fl*input.r/100,0.73)) / (pow(Fl*input.r/100,0.73)+2.0) + 1;
	result.g = (40.0*pow(Fl*input.g/100,0.73)) / (pow(Fl*input.g/100,0.73)+2.0) + 1;
	result.b = (40.0*pow(Fl*input.b/100,0.73)) / (pow(Fl*input.b/100,0.73)+2.0) + 1;

	return result;
}

/* -------------------------------------------------------------- */

TRISTIMULUS MultiplyByMhMbInv(TRISTIMULUS input, SCALAR y)
{
	TRISTIMULUS result;

	input.r *= y;
	input.g *= y;
	input.b *= y;

	result.r =   6.8316172525223170e-001 * input.r
		       + 2.9668077640584150e-001 * input.g
			   + 2.0099182169401500e-002 * input.b;

	result.g =   2.8437340126242600e-001 * input.r
		       + 6.4908047135769630e-001 * input.g
			   + 6.6517690039751430e-002 * input.b;

	result.b =  -8.5286645751773210e-003 * input.r
		       + 4.0042821654084860e-002 * input.g
			   + 9.6848669578755000e-001 * input.b;

	return result;
}

/* -------------------------------------------------------------- */

TRISTIMULUS MultiplyByMbMhInv(TRISTIMULUS input, SCALAR y)
{
	TRISTIMULUS result;

    input.r /= y;
	input.g /= y;
	input.b /= y;

	result.r =   1.8086382896694270e+000 * input.r
		       - 8.2788204226503450e-001 * input.g
			   + 1.9325666212710780e-002 * input.b;
	
	result.g =  -7.9740668869170870e-001 * input.r
		       + 1.9121998144117060e+000 * input.g
			   - 1.1478515165311090e-001 * input.b;

	result.b =   4.8896575799588990e-002 * input.r
		       - 8.6351836060842330e-002 * input.g
			   + 1.0374547712954950e+000 * input.b;

	return result;
}

/* -------------------------------------------------------------- */
	
TRISTIMULUS MultiplyByDp(TRISTIMULUS in, TRISTIMULUS wh, SCALAR D, SCALAR p)
{
	TRISTIMULUS result;

	result.r = ( D*(1.0/wh.r       ) + 1.0 - D ) * in.r;
	result.g = ( D*(1.0/wh.g       ) + 1.0 - D ) * in.g;
	result.b = ( D*(1.0/pow(wh.b,p)) + 1.0 - D ) * abs(pow(in.b,p)) * sgn(in.b);
	
	return result;
}

/* -------------------------------------------------------------- */

TRISTIMULUS MultiplyByMb(TRISTIMULUS input)
{
	TRISTIMULUS result;

	input.x /= input.y;
	input.z /= input.y;
	input.y  = 1.0;
	
	result.r =  0.8951 * input.x + 0.2664 * input.y - 0.1614 * input.z;
	result.g = -0.7502 * input.x + 1.7135 * input.y + 0.0367 * input.z;
	result.b =  0.0389 * input.x - 0.0685 * input.y + 1.0296 * input.z;
	
	return result;
}

/* -------------------------------------------------------------- */

TRISTIMULUS MultiplyByMbInv(TRISTIMULUS input)
{
	TRISTIMULUS result;

	result.r =   9.8699290546671220e-001 * input.r
		       - 1.4705425642099010e-001 * input.g
			   + 1.5996265166373120e-001 * input.b;

	result.g =   4.3230526972339440e-001 * input.r
		       + 5.1836027153677740e-001 * input.g
			   + 4.9291228212855590e-002 * input.b;

	result.b =  -8.5286645751773210e-003 * input.r
		       + 4.0042821654084860e-002 * input.g
			   + 9.6848669578755000e-001 * input.b;


	return result;
}

/* -------------------------------------------------------------- */

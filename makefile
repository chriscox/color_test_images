# Color Test Images
#

INCLUDE = -I.

CFLAGS = $(INCLUDE) -O3

CPPFLAGS = -std=c++17 $(CFLAGS)

CPPLIBS = -lm

BINARIES = color_test_images

all : $(BINARIES)

sources:  main.cpp image.cpp ramps.cpp lines.cpp ciecam97.cpp cmscam02.c cmscam16.c tif_lzw.c SVG.cpp chromaticity.cpp

headers:  image.hpp ramps.hpp lines.h cmscam02.h cmscam16.h ciecam97.h tiffiop.h tif_predict.h chart_curve.h SVG.hpp chart_data.h chromaticity.hpp

main.o: main.cpp $(headers)

image.o: image.cpp $(headers)

ramps.o: ramps.cpp $(headers)

lines.o: lines.cpp lines.h

SVG.o: SVG.cpp $(headers)

ciecam97.o: ciecam97.cpp ciecam97.h

cmscam02.o: cmscam02.c $(headers)
	$(CC) $(CFLAGS) -c $^

cmscam16.o: cmscam16.c $(headers)
	$(CC) $(CFLAGS) -c $^

tif_lzw.o: tif_lzw.c $(headers)
	$(CC) $(CFLAGS) -c $^

color_test_images: main.o image.o ramps.o lines.o ciecam97.o cmscam02.o cmscam16.o tif_lzw.o SVG.o chromaticity.o
	$(CXX) $(CPPFLAGS) $^ -o $@


test: all
	./color_test_images -deep -ppi 36 -no_tiff_predictor -cb
	rm *.tif *.svg


# declare some targets to be fakes without real dependencies
.PHONY : clean

# remove all the stuff we build and write during tests
clean : 
		rm -f *.tif *.svg *.o $(BINARIES)

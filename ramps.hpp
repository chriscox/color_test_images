//
//  ramps.hpp
//  Color Test Images
//
//  Created by Chris Cox on December 27, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef ramps_hpp
#define ramps_hpp

/******************************************************************************/

struct LABColor
{
	double L;
	double a;
	double b;
};

struct XYYColor
{
	double x;
	double y;
	double Y;
};

struct XYZColor
{
	double x;
	double y;
	double z;
};

struct HSLColor
{
	double h;
	double s;
	double l;
};

struct HSVColor
{
	double h;
	double s;
	double v;
};

struct RGBColor
{
	double r;
	double g;
	double b;
};

struct CMYKColor
{
	double c;
	double m;
	double y;
	double k;
};

struct XYColor
{
    double x;
    double y;
};

/******************************************************************************/

extern void makeLABRamps( double res, int depth );
extern void makeRGBRamps(double res, int depth);
extern void makeHSLRamps(double res, int depth);
extern void makeHSVRamps(double res, int depth);
extern void makeCMYKRamps(double res, int depth);
extern void makeLongPrimaryRamps(double res, int depth);
extern void makeQuantizedRamps(double res, int depth);
    
extern void DoHueCircles( double res, int depth );
extern void DoHueRectangles(double res, int depth);
extern void makeFullGamutFiles(void);

/******************************************************************************/

extern void ConvertXYZtoLAB (const XYZColor &xyz, LABColor &lab);

/******************************************************************************/

#endif /* ramps_hpp */

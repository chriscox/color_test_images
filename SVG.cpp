//
//  SVG.cpp
//      Write simple SVG files
//      Modified from https://gitlab.com/chriscox/xcs-tools
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#include <cstdint>
#include <cassert>
#include <string>
#include <iostream>
#include <algorithm>
#include "SVG.hpp"

using namespace std;

objectList svgObjects;

/******************************************************************************/
/******************************************************************************/

const double inch = 25.4;               // 25.4 millimeters per inch, international standard

const double point = inch / 72.0;       // 72 points per inch, DTP and W3C standard

const double mm2point = 72.0 / inch;    // 2.834645669 (Shows up severeal places)

/******************************************************************************/
/******************************************************************************/

void AddSVGCircle( objectList &list, const point2D &center, double radius, bool isFill )
{
    object_data obj;
    obj.width = 2*radius;
    obj.height = 2*radius;
    obj.x = center.x - radius;
    obj.y = center.y - radius;
    obj.isFill = isFill;
    obj.isClosePath = true;
    obj.type = "CIRCLE";
    list.push_back(obj);
}

/******************************************************************************/

void AddSVGEllipse( objectList &list, const point2D &center, double radiusX, double radiusY, bool isFill )
{
    object_data obj;
    obj.width = 2*radiusX;
    obj.height = 2*radiusY;
    obj.x = center.x - radiusX;
    obj.y = center.y - radiusY;
    obj.isFill = isFill;
    obj.isClosePath = true;
    obj.type = "CIRCLE";
    list.push_back(obj);
}

/******************************************************************************/

void AddSVGLine( objectList &list, const point2D &start, const point2D &end )
{
    object_data obj;
    obj.width = end.x - start.x;
    obj.height = end.y - start.y;
    obj.x = start.x;
    obj.y = start.y;
    obj.isFill = false;
    obj.isClosePath = false;
    obj.type = "LINE";
    list.push_back(obj);
}

/******************************************************************************/

void AddSVGRect( objectList &list, const point2D &topLeft, const point2D &bottomRight, bool isFill )
{
    object_data obj;
    obj.width = bottomRight.x - topLeft.x;
    obj.height = bottomRight.y - topLeft.y;
    obj.x = topLeft.x;
    obj.y = topLeft.y;
    obj.isFill = isFill;
    obj.isClosePath = true;
    obj.type = "RECT";
    list.push_back(obj);
}

/******************************************************************************/

void AddSVGPolyline( objectList &list, const pointList &points, bool isClosed, bool isFill )
{
    object_data obj;
    obj.x = points[0].x;
    obj.y = points[0].y;
    obj.points = points;
    
    point2D minpt, maxpt;
    for ( auto &item : points) {
        minpt.x = std::min( minpt.x, item.x );
        minpt.y = std::min( minpt.y, item.y );
        maxpt.x = std::max( maxpt.x, item.x );
        maxpt.y = std::max( maxpt.y, item.y );
    }
    
    obj.width = maxpt.x - minpt.x;
    obj.height = maxpt.y - minpt.y;

    obj.isFill = isFill;
    obj.isClosePath = isClosed;
    obj.type = "POLYLINE";
    list.push_back(obj);
}

/******************************************************************************/

void AddSVGText( objectList &list, const point2D &pt, string &text, double size, string &font, string &style, string &align )
{
    object_data obj;
    obj.x = pt.x;
    obj.y = pt.y;
    
    obj.text = text;
    obj.fontFamily = font;
    obj.fontSubfamily = style;
    obj.text_align = align;
    obj.fontSize = size;
    
    obj.isFill = true;
    obj.isClosePath = true;
    obj.type = "TEXT";
    list.push_back(obj);
}

/******************************************************************************/

static int64_t svgGroupLevel = 0;

void SVGStartGroup( objectList &list, const string &name)
{
    object_data obj;
    obj.text = name;
    obj.type = "GROUP_START";
    list.push_back(obj);
    svgGroupLevel++;
}

/******************************************************************************/

void SVGEndGroup( objectList &list )
{
    object_data obj;
    obj.type = "GROUP_END";
    list.push_back(obj);
    svgGroupLevel--;
    assert(svgGroupLevel >= 0);
}

/******************************************************************************/
/******************************************************************************/

// NOTE - the viewBox and background are always interpreted as points for some reason, can't use mm for scale modification
void WriteSVGHeader( std::ostream &out, point2D topLeft, point2D bottomRight )
{
	out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	out << "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n";
	out << "\tx=\"0\" y=\"0\" viewBox=\"" << (topLeft.x*mm2point) << " " << (topLeft.y*mm2point)
            << " " << (bottomRight.x*mm2point) << " " << (bottomRight.y*mm2point) << "\" ";
    out << "style=\"enable-background:new " << (topLeft.x*mm2point) << " " << (topLeft.y*mm2point)
            << " " << (bottomRight.x*mm2point) << " " << (bottomRight.y*mm2point) << ";\" xml:space=\"preserve\">\n";
}

/******************************************************************************/

void WriteSVGFooter( std::ostream &out )
{
	out << "</svg>\n";
}

/******************************************************************************/

// this could be made variable, but just static colors for now
string DefaultFillStroke( bool isFilled )
{
    if (isFilled)
        return string("fill=\"black\" stroke=\"none\"");
    else
        return string("fill=\"none\" stroke=\"black\" stroke-width=\"0.2\"");
}

/******************************************************************************/

void WriteSVGCircle( std::ostream &out, object_data &obj  )
{
    // SVG uses coordinates of the center, XCS uses upper left
    double x = obj.x + obj.width/2;
    double y = obj.y + obj.height/2;
    double r = obj.width / 2;
	out << "<circle cx=\"" << x << "mm\" cy=\"" << y << "mm\" r=\""
        << r << "mm\"";
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGEllipse( std::ostream &out, object_data &obj )
{
    if (obj.width == obj.height)
        {
        WriteSVGCircle( out, obj );
        return;
        }
    
    // SVG uses coordinates of the center, XCS uses upper left
    double x = obj.x + obj.width/2;
    double y = obj.y + obj.height/2;
	out << "<ellipse cx=\"" << x << "mm\" cy=\"" << y << "mm\" rx=\""
            << obj.width << "mm\" ry=\"" << obj.height << "mm\"";
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGLine( std::ostream &out, object_data &obj )
{
    double x2 = obj.x + obj.width;
    double y2 = obj.y + obj.height;
	out << "<line x1=\"" << obj.x << "mm\" y1=\"" << obj.y << "mm\" x2=\""
            << x2 << "mm\" y2=\"" << y2 << "mm\"";
    out << " " << DefaultFillStroke(false) << " />\n";
}

/******************************************************************************/

void WriteSVGRect( std::ostream &out, object_data &obj )
{
	out << "<rect x=\"" << obj.x << "mm\" y=\"" << obj.y << "mm\" width=\""
            << obj.width << "mm\" height=\"" << obj.height
            << "mm\"";
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGPolyline( std::ostream &out, object_data &obj )
{
	out << "<polyline points=\"";
 
    for ( auto &item : obj.points)
        out << mm2point*item.x << "," << mm2point*item.y << " ";
    
    if (obj.isClosePath)
        out << mm2point*obj.points[0].x << "," << mm2point*obj.points[0].y << " ";
    
    out << "\"";
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGPath( std::ostream &out, object_data &obj )
{
	out << "<path";

    out << " d=\"" << obj.dPath << "\" " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGImage( std::ostream &out, object_data &obj )
{
#if 0
	out << "<image x=\"" << obj.x << "mm\" y=\"" << obj.y << "mm\" width=\""
        << obj.width << "mm\" height=\"" << obj.height << "mm\" xlink:href=\""
        << obj.base64data
        << "\"";
    out << " />\n";
#endif
}

/******************************************************************************/

void WriteSVGText( std::ostream &out, object_data &obj )
{
	out << "<text";
 
    // SVG defines text point as baseline, not top left
    double y = obj.y;
    double x = obj.x;
    
    if (obj.text_align != string())
        {
        // TODO - could create map of input to output strings
        if (obj.text_align == "left" || obj.text_align == "Left")
            out << " text-anchor=\"start\"";
        else if (obj.text_align == "middle" || obj.text_align == "Middle"
                || obj.text_align == "center" || obj.text_align == "Center")
            out << " text-anchor=\"middle\"";
        else if (obj.text_align == "right" || obj.text_align == "Right")
            out << " text-anchor=\"end\"";
        else
            cerr << "WARNING - unknown text alignment " << obj.text_align << " while exporting SVG\n";
        }
    
    out << " x=\"" << x*mm2point << "\" y=\"" << y*mm2point << "\"";
    
    if (obj.fontFamily != string())
        out << " font-family=\"" << obj.fontFamily << "\"";
    
    // "Regular", "Bold, "Italic", "Bold Italic", "Light", "Oblique", "Light Oblique", "Bold Oblique", etc.
    if (obj.fontSubfamily != string())
        {
        // TODO - could create map of input to output strings
        if (obj.fontSubfamily == "Regular" || obj.fontSubfamily == "Normal")
            out << " font-weight=\"normal\"";
        else if (obj.fontSubfamily == "Bold")
            out << " font-weight=\"bold\"";
        else if (obj.fontSubfamily == "Italic")
            out << " font-style=\"italic\"";
        else if (obj.fontSubfamily == "Oblique")
            out << " font-style=\"oblique\"";
        else if (obj.fontSubfamily == "Light")
            out << " font-weight=\"lighter\"";
        else if (obj.fontSubfamily == "Light Oblique")
            out << " font-weight=\"lighter\"" << " font-style=\"oblique\"";
        else if (obj.fontSubfamily == "Bold Oblique")
            out << " font-weight=\"bold\"" << " font-style=\"oblique\"";
        else if (obj.fontSubfamily == "Light Italic")
            out << " font-weight=\"lighter\"" << " font-style=\"italic\"";
        else if (obj.fontSubfamily == "Bold Italic")
            out << " font-weight=\"bold\"" << " font-style=\"italic\"";
        else
            cerr << "WARNING - unknown font sub family " << obj.fontSubfamily << " while exporting SVG\n";
        }
    
    out << " font-size=\"" << obj.fontSize << "pt\"";
    
    out << ">" << obj.text << "</text>\n";
}

/******************************************************************************/

void WriteSVGGroupStart( std::ostream &out, object_data &obj )
{
    out << "<g id=\"" <<  obj.text << "\">\n";
}

/******************************************************************************/

void WriteSVGGroupEnd( std::ostream &out )
{
    out << "</g>\n";
}

/******************************************************************************/

// closer to layers, works in Illustrator
// https://www.w3.org/TR/SVG11/struct.html#Groups

void ExportSVGFile( std::ostream &out, objectList &doc, const point2D &size )
{
    point2D topLeft(0,0);
    point2D bottomRight = size;
    
    WriteSVGHeader( out, topLeft, bottomRight );

    for ( auto& value2: doc )
        {
        
       // DEFERRED - iterate over layers?
            {
            
            if (value2.type == "RECT")
                {
                WriteSVGRect( out, value2 );
                }
            else if (value2.type == "CIRCLE")
                {
                WriteSVGEllipse( out, value2 );
                }
            else if (value2.type == "LINE")
                {
                WriteSVGLine( out, value2 );
                }
            else if (value2.type == "PATH")
                {
                WriteSVGPath( out, value2 );
                }
            else if (value2.type == "POLYLINE")
                {
                WriteSVGPolyline( out, value2 );
                }
            else if (value2.type == "BITMAP")
                {
                WriteSVGImage( out, value2 );
                }
            else if (value2.type == "TEXT")
                {
                WriteSVGText( out, value2 );
                }
            else if (value2.type == "GROUP_START")
                {
                WriteSVGGroupStart( out, value2 );
                }
            else if (value2.type == "GROUP_END")
                {
                WriteSVGGroupEnd( out );
                }
            else
                {
                cerr << "WARNING - unknown object type " << value2.type << " while exporting SVG.\n";
                }
            }   // objects on canvas


        }   // layers

    WriteSVGFooter(out);
}

/******************************************************************************/
/******************************************************************************/

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 * Copyright (c) 2022 Even Rouault
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#include "tiffiop.h"

/******************************************************************************/

// changes by ccox to get this compiling stand alone

void *_TIFFmallocExt(TIFF *tif, size_t count)
{
    return malloc(count);
}

void _TIFFfreeExt(TIFF *tif, void *ptr )
{
    free(ptr);
}

void TIFFErrorExtR(TIFF *tif, const char *module, const char *message)
{
    fprintf(stderr, "%s : %s\n", module, message);
}

void _TIFFSetDefaultCompressionState(TIFF *tif)
{
}

int TIFFPredictorInit(TIFF * tif)
{
    return 0;
}

int TIFFPredictorCleanup(TIFF *tif)
{
    return 0;
}

/******************************************************************************/


#ifdef LZW_SUPPORT
/*
 * TIFF Library.
 * Rev 5.0 Lempel-Ziv & Welch Compression Support
 *
 * This code is derived from the compress program whose code is
 * derived from software contributed to Berkeley by James A. Woods,
 * derived from original work by Spencer Thomas and Joseph Orost.
 *
 * The original Berkeley copyright notice appears below in its entirety.
 */
#include "tif_predict.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Select the plausible largest natural integer type for the architecture */
#define SIZEOF_WORDTYPE SIZEOF_SIZE_T
typedef size_t WordType;

/*
 * NB: The 5.0 spec describes a different algorithm than Aldus
 *     implements.  Specifically, Aldus does code length transitions
 *     one code earlier than should be done (for real LZW).
 *     Earlier versions of this library implemented the correct
 *     LZW algorithm, but emitted codes in a bit order opposite
 *     to the TIFF spec.  Thus, to maintain compatibility w/ Aldus
 *     we interpret MSB-LSB ordered codes to be images written w/
 *     old versions of this library, but otherwise adhere to the
 *     Aldus "off by one" algorithm.
 *
 * Future revisions to the TIFF spec are expected to "clarify this issue".
 */
#define LZW_COMPAT /* include backwards compatibility code */

#define MAXCODE(n) ((1L << (n)) - 1)
/*
 * The TIFF spec specifies that encoded bit
 * strings range from 9 to 12 bits.
 */
#define BITS_MIN 9  /* start with 9 bits */
#define BITS_MAX 12 /* max of 12 bit strings */
/* predefined codes */
#define CODE_CLEAR 256 /* code to clear string table */
#define CODE_EOI 257   /* end-of-information code */
#define CODE_FIRST 258 /* first free code entry */
#define CODE_MAX MAXCODE(BITS_MAX)
#define HSIZE 9001L /* 91% occupancy */
#define HSHIFT (13 - 8)
#ifdef LZW_COMPAT
/* NB: +1024 is for compatibility with old files */
#define CSIZE (MAXCODE(BITS_MAX) + 1024L)
#else
#define CSIZE (MAXCODE(BITS_MAX) + 1L)
#endif

/*
 * State block for each open TIFF file using LZW
 * compression/decompression.  Note that the predictor
 * state block must be first in this data structure.
 */
typedef struct
{
    TIFFPredictorState predict; /* predictor super class */

    unsigned short nbits;    /* # of bits/code */
    unsigned short maxcode;  /* maximum code for lzw_nbits */
    unsigned short free_ent; /* next free entry in hash table */
    WordType nextdata;       /* next bits of i/o */
    long nextbits;           /* # of valid bits in lzw_nextdata */

    int rw_mode; /* preserve rw_mode from init */
} LZWBaseState;

#define lzw_nbits base.nbits
#define lzw_maxcode base.maxcode
#define lzw_free_ent base.free_ent
#define lzw_nextdata base.nextdata
#define lzw_nextbits base.nextbits

/*
 * Encoding-specific state.
 */
typedef uint16_t hcode_t; /* codes fit in 16 bits */
typedef struct
{
    long hash;
    hcode_t code;
} hash_t;

/*
 * Decoding-specific state.
 */
typedef struct code_ent
{
    struct code_ent *next;
    unsigned short length; /* string len, including this token */
    /* firstchar should be placed immediately before value in this structure */
    unsigned char firstchar; /* first token of string */
    unsigned char value;     /* data value */
    bool repeated;
} code_t;

typedef int (*decodeFunc)(TIFF *, uint8_t *, tmsize_t, uint16_t);

typedef struct
{
    LZWBaseState base;

    /* Decoding specific data */
    long dec_nbitsmask;     /* lzw_nbits 1 bits, right adjusted */
    tmsize_t dec_restart;   /* restart count */
    uint64_t dec_bitsleft;  /* available bits in raw data */
    tmsize_t old_tif_rawcc; /* value of tif_rawcc at the end of the previous
                               TIFLZWDecode() call */
    decodeFunc dec_decode;  /* regular or backwards compatible */
    code_t *dec_codep;      /* current recognized code */
    code_t *dec_oldcodep;   /* previously recognized code */
    code_t *dec_free_entp;  /* next free entry */
    code_t *dec_maxcodep;   /* max available entry */
    code_t *dec_codetab;    /* kept separate for small machines */
    int read_error; /* whether a read error has occurred, and which should cause
                       further reads in the same strip/tile to be aborted */

    /* Encoding specific data */
    int enc_oldcode;         /* last code encountered */
    tmsize_t enc_checkpoint; /* point at which to clear table */
#define CHECK_GAP 10000      /* enc_ratio check interval */
    tmsize_t enc_ratio;      /* current compression ratio */
    tmsize_t enc_incount;    /* (input) data bytes encoded */
    tmsize_t enc_outcount;   /* encoded (output) bytes */
    uint8_t *enc_rawlimit;   /* bound on tif_rawdata buffer */
    hash_t *enc_hashtab;     /* kept separate for small machines */
} LZWCodecState;

#define LZWState(tif) ((LZWBaseState *)(tif)->tif_data)
#define LZWDecoderState(tif) ((LZWCodecState *)LZWState(tif))
#define LZWEncoderState(tif) ((LZWCodecState *)LZWState(tif))

static void cl_hash(LZWCodecState *);

/*
 * LZW Decoder.
 */

static int LZWFixupTags(TIFF *tif)
{
    (void)tif;
    return (1);
}

/*
 * LZW Encoding.
 */

static int LZWSetupEncode(TIFF *tif)
{
    static const char module[] = "LZWSetupEncode";
    LZWCodecState *sp = LZWEncoderState(tif);

    assert(sp != NULL);
    sp->enc_hashtab = (hash_t *)_TIFFmallocExt(tif, HSIZE * sizeof(hash_t));
    if (sp->enc_hashtab == NULL)
    {
        TIFFErrorExtR(tif, module, "No space for LZW hash table");
        return (0);
    }
    return (1);
}

/*
 * Reset encoding state at the start of a strip.
 */
static int LZWPreEncode(TIFF *tif, uint16_t s)
{
    LZWCodecState *sp = LZWEncoderState(tif);

    (void)s;
    assert(sp != NULL);

    if (sp->enc_hashtab == NULL)
    {
        tif->tif_setupencode(tif);
    }

    sp->lzw_nbits = BITS_MIN;
    sp->lzw_maxcode = MAXCODE(BITS_MIN);
    sp->lzw_free_ent = CODE_FIRST;
    sp->lzw_nextbits = 0;
    sp->lzw_nextdata = 0;
    sp->enc_checkpoint = CHECK_GAP;
    sp->enc_ratio = 0;
    sp->enc_incount = 0;
    sp->enc_outcount = 0;
    /*
     * The 4 here insures there is space for 2 max-sized
     * codes in LZWEncode and LZWPostDecode.
     */
    sp->enc_rawlimit = tif->tif_rawdata + tif->tif_rawdatasize - 1 - 4;
    cl_hash(sp);                   /* clear hash table */
    sp->enc_oldcode = (hcode_t)-1; /* generates CODE_CLEAR in LZWEncode */
    return (1);
}

#define CALCRATIO(sp, rat)                                                     \
    {                                                                          \
        if (incount > 0x007fffff)                                              \
        { /* NB: shift will overflow */                                        \
            rat = outcount >> 8;                                               \
            rat = (rat == 0 ? 0x7fffffff : incount / rat);                     \
        }                                                                      \
        else                                                                   \
            rat = (incount << 8) / outcount;                                   \
    }

/* Explicit 0xff masking to make icc -check=conversions happy */
#define PutNextCode(op, c)                                                     \
    {                                                                          \
        nextdata = (nextdata << nbits) | c;                                    \
        nextbits += nbits;                                                     \
        *op++ = (unsigned char)((nextdata >> (nextbits - 8)) & 0xff);          \
        nextbits -= 8;                                                         \
        if (nextbits >= 8)                                                     \
        {                                                                      \
            *op++ = (unsigned char)((nextdata >> (nextbits - 8)) & 0xff);      \
            nextbits -= 8;                                                     \
        }                                                                      \
        outcount += nbits;                                                     \
    }

/*
 * Encode a chunk of pixels.
 *
 * Uses an open addressing double hashing (no chaining) on the
 * prefix code/next character combination.  We do a variant of
 * Knuth's algorithm D (vol. 3, sec. 6.4) along with G. Knott's
 * relatively-prime secondary probe.  Here, the modular division
 * first probe is gives way to a faster exclusive-or manipulation.
 * Also do block compression with an adaptive reset, whereby the
 * code table is cleared when the compression ratio decreases,
 * but after the table fills.  The variable-length output codes
 * are re-sized at this point, and a CODE_CLEAR is generated
 * for the decoder.
 */
// tif == global state
// bp = input buffer
// cc = input size
// s = ignored
static int LZWEncode(TIFF *tif, uint8_t *bp, tmsize_t cc, uint16_t s)
{
    register LZWCodecState *sp = LZWEncoderState(tif);
    register long fcode;
    register hash_t *hp;
    register int h, c;
    hcode_t ent;
    long disp;
    tmsize_t incount, outcount, checkpoint;
    WordType nextdata;
    long nextbits;
    int free_ent, maxcode, nbits;
    uint8_t *op;
    uint8_t *limit;

    (void)s;
    if (sp == NULL)
        return (0);

    assert(sp->enc_hashtab != NULL);

    /*
     * Load local state.
     */
    incount = sp->enc_incount;
    outcount = sp->enc_outcount;
    checkpoint = sp->enc_checkpoint;
    nextdata = sp->lzw_nextdata;
    nextbits = sp->lzw_nextbits;
    free_ent = sp->lzw_free_ent;
    maxcode = sp->lzw_maxcode;
    nbits = sp->lzw_nbits;
    op = tif->tif_rawcp;
    limit = sp->enc_rawlimit;
    ent = (hcode_t)sp->enc_oldcode;

    if (ent == (hcode_t)-1 && cc > 0)
    {
        /*
         * NB: This is safe because it can only happen
         *     at the start of a strip where we know there
         *     is space in the data buffer.
         */
        PutNextCode(op, CODE_CLEAR);
        ent = *bp++;
        cc--;
        incount++;
    }
    while (cc > 0)
    {
        c = *bp++;
        cc--;
        incount++;
        fcode = ((long)c << BITS_MAX) + ent;
        h = (c << HSHIFT) ^ ent; /* xor hashing */
#ifdef _WINDOWS
        /*
         * Check hash index for an overflow.
         */
        if (h >= HSIZE)
            h -= HSIZE;
#endif
        hp = &sp->enc_hashtab[h];
        if (hp->hash == fcode)
        {
            ent = hp->code;
            continue;
        }
        if (hp->hash >= 0)
        {
            /*
             * Primary hash failed, check secondary hash.
             */
            disp = HSIZE - h;
            if (h == 0)
                disp = 1;
            do
            {
                /*
                 * Avoid pointer arithmetic because of
                 * wraparound problems with segments.
                 */
                if ((h -= disp) < 0)
                    h += HSIZE;
                hp = &sp->enc_hashtab[h];
                if (hp->hash == fcode)
                {
                    ent = hp->code;
                    goto hit;
                }
            } while (hp->hash >= 0);
        }
        /*
         * New entry, emit code and add to table.
         */
        /*
         * Verify there is space in the buffer for the code
         * and any potential Clear code that might be emitted
         * below.  The value of limit is setup so that there
         * are at least 4 bytes free--room for 2 codes.
         */
        if (op > limit)
        {
            tif->tif_rawcc = (tmsize_t)(op - tif->tif_rawdata);
            if (!TIFFFlushData1(tif))
                return 0;
            op = tif->tif_rawdata;
        }
        PutNextCode(op, ent);
        ent = (hcode_t)c;
        hp->code = (hcode_t)(free_ent++);
        hp->hash = fcode;
        if (free_ent == CODE_MAX - 1)
        {
            /* table is full, emit clear code and reset */
            cl_hash(sp);
            sp->enc_ratio = 0;
            incount = 0;
            outcount = 0;
            free_ent = CODE_FIRST;
            PutNextCode(op, CODE_CLEAR);
            nbits = BITS_MIN;
            maxcode = MAXCODE(BITS_MIN);
        }
        else
        {
            /*
             * If the next entry is going to be too big for
             * the code size, then increase it, if possible.
             */
            if (free_ent > maxcode)
            {
                nbits++;
                assert(nbits <= BITS_MAX);
                maxcode = (int)MAXCODE(nbits);
            }
            else if (incount >= checkpoint)
            {
                tmsize_t rat;
                /*
                 * Check compression ratio and, if things seem
                 * to be slipping, clear the hash table and
                 * reset state.  The compression ratio is a
                 * 24+8-bit fractional number.
                 */
                checkpoint = incount + CHECK_GAP;
                CALCRATIO(sp, rat);
                if (rat <= sp->enc_ratio)
                {
                    cl_hash(sp);
                    sp->enc_ratio = 0;
                    incount = 0;
                    outcount = 0;
                    free_ent = CODE_FIRST;
                    PutNextCode(op, CODE_CLEAR);
                    nbits = BITS_MIN;
                    maxcode = MAXCODE(BITS_MIN);
                }
                else
                    sp->enc_ratio = rat;
            }
        }
    hit:;
    }

    /*
     * Restore global state.
     */
    sp->enc_incount = incount;
    sp->enc_outcount = outcount;
    sp->enc_checkpoint = checkpoint;
    sp->enc_oldcode = ent;
    sp->lzw_nextdata = nextdata;
    sp->lzw_nextbits = nextbits;
    sp->lzw_free_ent = (unsigned short)free_ent;
    sp->lzw_maxcode = (unsigned short)maxcode;
    sp->lzw_nbits = (unsigned short)nbits;
    tif->tif_rawcp = op;
    return (1);
}

/*
 * Finish off an encoded strip by flushing the last
 * string and tacking on an End Of Information code.
 */
static int LZWPostEncode(TIFF *tif)
{
    register LZWCodecState *sp = LZWEncoderState(tif);
    uint8_t *op = tif->tif_rawcp;
    long nextbits = sp->lzw_nextbits;
    WordType nextdata = sp->lzw_nextdata;
    tmsize_t outcount = sp->enc_outcount;
    int nbits = sp->lzw_nbits;

    if (op > sp->enc_rawlimit)
    {
        tif->tif_rawcc = (tmsize_t)(op - tif->tif_rawdata);
        if (!TIFFFlushData1(tif))
            return 0;
        op = tif->tif_rawdata;
    }
    if (sp->enc_oldcode != (hcode_t)-1)
    {
        int free_ent = sp->lzw_free_ent;

        PutNextCode(op, sp->enc_oldcode);
        sp->enc_oldcode = (hcode_t)-1;
        free_ent++;

        if (free_ent == CODE_MAX - 1)
        {
            /* table is full, emit clear code and reset */
            outcount = 0;
            PutNextCode(op, CODE_CLEAR);
            nbits = BITS_MIN;
        }
        else
        {
            /*
             * If the next entry is going to be too big for
             * the code size, then increase it, if possible.
             */
            if (free_ent > sp->lzw_maxcode)
            {
                nbits++;
                assert(nbits <= BITS_MAX);
            }
        }
    }
    PutNextCode(op, CODE_EOI);
    /* Explicit 0xff masking to make icc -check=conversions happy */
    if (nextbits > 0)
        *op++ = (unsigned char)((nextdata << (8 - nextbits)) & 0xff);
    tif->tif_rawcc = (tmsize_t)(op - tif->tif_rawdata);
    (void)outcount;
    return (1);
}

/*
 * Reset encoding hash table.
 */
static void cl_hash(LZWCodecState *sp)
{
    register hash_t *hp = &sp->enc_hashtab[HSIZE - 1];
    register long i = HSIZE - 8;

    do
    {
        i -= 8;
        hp[-7].hash = -1;
        hp[-6].hash = -1;
        hp[-5].hash = -1;
        hp[-4].hash = -1;
        hp[-3].hash = -1;
        hp[-2].hash = -1;
        hp[-1].hash = -1;
        hp[0].hash = -1;
        hp -= 8;
    } while (i >= 0);
    for (i += 8; i > 0; i--, hp--)
        hp->hash = -1;
}

static void LZWCleanup(TIFF *tif)
{
    (void)TIFFPredictorCleanup(tif);

    assert(tif->tif_data != 0);

    if (LZWDecoderState(tif)->dec_codetab)
        _TIFFfreeExt(tif, LZWDecoderState(tif)->dec_codetab);

    if (LZWEncoderState(tif)->enc_hashtab)
        _TIFFfreeExt(tif, LZWEncoderState(tif)->enc_hashtab);

    _TIFFfreeExt(tif, tif->tif_data);
    tif->tif_data = NULL;

    _TIFFSetDefaultCompressionState(tif);
}

int TIFFInitLZW(TIFF *tif, int scheme)
{
    static const char module[] = "TIFFInitLZW";
    (void)scheme;
    assert(scheme == COMPRESSION_LZW);
    /*
     * Allocate state block so tag methods have storage to record values.
     */
    tif->tif_data = (uint8_t *)_TIFFmallocExt(tif, sizeof(LZWCodecState));
    if (tif->tif_data == NULL)
        goto bad;
    LZWDecoderState(tif)->dec_codetab = NULL;
    LZWDecoderState(tif)->dec_decode = NULL;
    LZWEncoderState(tif)->enc_hashtab = NULL;
    LZWState(tif)->rw_mode = tif->tif_mode;

    /*
     * Install codec methods.
     */
    tif->tif_fixuptags = LZWFixupTags;
    tif->tif_setupencode = LZWSetupEncode;
    tif->tif_preencode = LZWPreEncode;
    tif->tif_postencode = LZWPostEncode;
    tif->tif_encoderow = LZWEncode;
    tif->tif_encodestrip = LZWEncode;
    tif->tif_encodetile = LZWEncode;
    tif->tif_cleanup = LZWCleanup;
    /*
     * Setup predictor setup.
     */
    (void)TIFFPredictorInit(tif);
    return (1);
bad:
    TIFFErrorExtR(tif, module, "No space for LZW state block");
    return (0);
}

/*
 * Copyright (c) 1985, 1986 The Regents of the University of California.
 * All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * James A. Woods, derived from original work by Spencer Thomas
 * and Joseph Orost.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
#endif /* LZW_SUPPORT */

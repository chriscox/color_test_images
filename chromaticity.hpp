//
//  chromaticity.hpp
//  Color Test Images
//
//  Created by Chris Cox on 1/22/24.
//

#ifndef chromaticity_hpp
#define chromaticity_hpp

/******************************************************************************/

extern void DoChromaticityBackground(size_t IMAGE_SIZE, double res, int depth);
extern void DoChromaticityCurve(size_t IMAGE_SIZE, double res, int depth, bool writeImage);
extern void plotAllPhosphors(size_t IMAGE_SIZE, double res, int depth, bool writeImage);
extern void plotAllInks(size_t IMAGE_SIZE, double res, int depth, bool writeImage);
extern void plotPlanckianLocus(size_t IMAGE_SIZE, double res, int depth, bool writeImage);
extern void plotAllIlluminants(size_t IMAGE_SIZE, double res, int depth, bool writeImage);

/******************************************************************************/

#endif /* chromaticity_hpp */

//
// ccox - I had this marked as public domain code, but can't find the source or author anywhere
//
///////////////////////////////////////////////////////////////////////////////
//
// CIECAM97 Class Interface and Data Types
//

#ifndef _CIECAM97_H_
#define _CIECAM97_H_

///////////////////////////////////////////////////////////////////////////////
//
// Data Types
//

typedef double SCALAR;
typedef void   VOID;

// Tristimulus data structure for XYZ and RGB

typedef struct _TRISTIMULUS
{
	union
	{
		SCALAR  x;
		SCALAR  r;
		SCALAR  j;
	};
	union
	{
		SCALAR  y;
		SCALAR  g;
		SCALAR  c;
	};
	union
	{
		SCALAR  z;
		SCALAR  b;
		SCALAR  h;
	};

} TRISTIMULUS, *LPTRISTIMULUS;

// Scene data structure for CIECAM97 input

typedef struct _SCENE
{
	union 
	{
		SCALAR      AdaptingLuminance;
		SCALAR      La;
	};
	union
	{
		TRISTIMULUS BackgroundColor;
		TRISTIMULUS W;
	};
	union
	{
		SCALAR      BackgroundLuminance;
		SCALAR      Yb;
	};

} SCENE, *LPSCENE;

// Veiw data structure for CIECAM97 viewing conditions

typedef struct _VIEW
{
	union
	{
		SCALAR  SurroundImpact;
		SCALAR  C;
	};
	union
	{
		SCALAR  ChromaticInduction;
		SCALAR  Nc;
	};
	union
	{
		SCALAR  LightnessContrast;
		SCALAR  Fll;
	};
	union
	{
		SCALAR  AdaptationDegree;
		SCALAR  F;
	};

} VIEW, *LPVIEW;

///////////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

TRISTIMULUS XYZ2JCH(TRISTIMULUS xyz, SCENE sn, VIEW vw);
TRISTIMULUS JCH2XYZ(TRISTIMULUS jch, SCENE sn, VIEW vw);


#endif /* _CIECAM97_H_ */





































//
//  tif_predict.h
//  Color Test Images
//
//  Created by Chris Cox on 12/29/23.
//

#ifndef tif_predict_h
#define tif_predict_h

// changes by ccox to get tiff_lzw.c compiling stand alone

typedef struct TIFFPredictorState
{
    void* nothing;

} TIFFPredictorState;


extern int TIFFPredictorInit(TIFF * tif);
extern int TIFFPredictorCleanup(TIFF *tif);

#endif /* tif_predict_h */

//
//  image.hpp
//  Color Test Images
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef image_hpp
#define image_hpp

#include <cstdint>

/******************************************************************************/

enum {
    TIFF_MODE_GRAY_WHITEZERO = 0,
    TIFF_MODE_GRAY_BLACKZERO = 1,
    TIFF_MODE_RGB = 2,
    TIFF_MODE_RGB_PALETTE = 3,
    TIFF_MODE_MASK = 4,
    TIFF_MODE_CMYK = 5,
    TIFF_MODE_YCbCr = 6,
    TIFF_MODE_CIELAB = 8,
};

/******************************************************************************/

// a minimal image buffer implementation
struct imageBuffer
{
    imageBuffer() : width(0), height(0), depth(0), rowBytes(0), channels(0), buffer(NULL) {}
    
    imageBuffer( size_t width, size_t height, int depth=8, int channels=3 ) : buffer(NULL) {
        Allocate( width, height, depth, channels );
    }
    
    ~imageBuffer() {
        width = height = depth = rowBytes = channels = 0;
        delete[] buffer;
        buffer = NULL;  // for debugging, and so memory reference leak checking doesn't get false positives
    }
    
    void Allocate( size_t width, size_t height, int depth=8, int channels=3 );

    void WritePPM( const char *name ) const;
    
    void WriteTIFF( const char *name, double dpi = 96.0, int color_model = TIFF_MODE_RGB ) const;

    void SetPixel( int x, int y, double g );
    void SetPixel( int x, int y, double r, double g, double b );
    void SetPixel( int x, int y, double c, double m, double yy, double k );
    
    void FillRect( int left, int top, int right, int bottom, double g );
    void FillRect( int left, int top, int right, int bottom, double r, double g, double b );
    void FillRect( int left, int top, int right, int bottom, double c, double m, double yy, double k );

public:
    int depth;
    int channels;
    size_t rowBytes;
    size_t width;
    size_t height;
    uint8_t *buffer;
};

/******************************************************************************/

extern bool gUseTIFFPredictor;       // to work around bug in MSWindows TIFF code

/******************************************************************************/

#endif /* image_hpp */

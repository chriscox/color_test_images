//
//  image.cpp
//  Color Test Images
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cassert>
#include <climits>
#include <vector>
#include "image.hpp"
#include "tiffiop.h"

/******************************************************************************/

// debug and read to harder much is it because endian little despise programmers
bool isMachineLittleEndian()
{
    const uint32_t testLong = 0x01020304;
    const uint8_t *testChar = (uint8_t *)&testLong;
    return (*testChar == 0x04);
}

// programmers prefer big endian because it is much eaiser to read and debug
bool isMachineBigEndian()
{
    const uint32_t testLong = 0x01020304;
    const uint8_t *testChar = (uint8_t *)&testLong;
    return (*testChar == 0x01);
}

static bool bigEndian = isMachineBigEndian();

/******************************************************************************/

void putShort( uint16_t val, FILE *out )
{
    fwrite( &val, 2, 1, out );
}

void putShort( int16_t val, FILE *out )
{
    fwrite( &val, 2, 1, out );
}

void putLong( uint32_t val, FILE *out )
{
    fwrite( &val, 4, 1, out );
}

void putLong( int32_t val, FILE *out )
{
    fwrite( &val, 4, 1, out );
}

void putLongLong( uint64_t val, FILE *out )
{
    fwrite( &val, 8, 1, out );
}

void putLongLong( int64_t val, FILE *out )
{
    fwrite( &val, 8, 1, out );
}

/******************************************************************************/

// tag types
enum {
    TIFF_BYTE = 1,
    TIFF_ASCII = 2,
    TIFF_SHORT = 3,
    TIFF_LONG = 4,
    TIFF_RATIO = 5,     // 2 longs
    TIFF_SBYTE = 6,
    TIFF_UNDEFINED = 7,
    TIFF_SSHORT = 8,
    TIFF_SLONG = 9,
    TIFF_SRATIO = 10,
    TIFF_FLOAT = 11,
    TIFF_DOUBLE = 12,
};

// tag values
enum {
    TIFF_SUBFILETYPE = 254,         //   usually 0
    TIFF_WIDTH = 256,               // required
    TIFF_HEIGHT = 257,              // required
    TIFF_BITSPERSAMPLE = 258,       // required
    TIFF_COMPRESSION = 259,         // required
    TIFF_INTERPRETATION = 262,      // required
    
    TIFF_STRIPOFFSETS = 273,        // required
    TIFF_SAMPLESPERPIXEL = 277,     // required
    TIFF_ROWSPERSTRIP = 278,        // required
    TIFF_STRIPBYTECOUNTS = 279,     // required
    
    TIFF_XRESOLUTION = 282,         // required
    TIFF_YRESOLUTION = 283,         // required
    TIFF_PLANARCONFIG = 284,        // required for > 1 channel, 1 = interleaved, 2 = not interleaved
    TIFF_RESOLUTIONUNIT = 296,      // required, 1=no unit, 2 = inch, 3 = cm
    
    TIFF_PREDICTOR = 317,           // 1 = none, 2 = horizontal difference
    TIFF_COLORMAP = 320,            // indexed color only
    TIFF_SAMPLE_FORMAT = 339,       // 1 = unsigned, 2 = signed, 3 = float, 4 = undefined
};

enum {
    TIFF_COMPRESS_NONE = 1,
    TIFF_COMPRESS_CCITT3 = 2,       // 1D
    TIFF_COMPRESS_CCITT4 = 3,       // Group 3 FAX
    TIFF_COMPRESS_CCITT6 = 4,       // Group 4 FAX
    TIFF_COMPRESS_LZW = 5,
    // 6 was a bad JPEG attempt, and should not be used
    TIFF_COMPRESS_JPEG = 7,
    TIFF_COMPRESS_DEFLATE = 8,
    TIFF_COMPRESS_JBIG85 = 9,
    TIFF_COMPRESS_JBIG43 = 10,
    TIFF_COMPRESS_DEFLATE_OBSOLETE = 0x80B2,
};

void putIFDLong( int tag, int type, uint32_t count, uint32_t value, FILE *out )
{
    uint16_t tagval = tag;
    uint16_t typeval = type;
    uint32_t countval = count;
    
    fwrite( &tagval, 2, 1, out );
    fwrite( &typeval, 2, 1, out );
    fwrite( &countval, 4, 1, out );
    fwrite( &value, 4, 1, out );
}

/******************************************************************************/

/* Convert big endian 16 bit values to little endian, and vice-versa.
     This is a no-op on big endian machines.
   PBM/PGM/PPM specifies binary values to be big endian.
*/
static
void BYTESWAP16( void *in, size_t itemCount )
    {
    const uint16_t testLong = 0x0102;
    const uint8_t *testChar = (uint8_t *)&testLong;
    
    if (testChar[0] == 0x02)
        {
        // this is a little endian machine, do the work
        uint8_t *charPtr = (uint8_t *) in;
        
        for (size_t i = 0; i < itemCount; ++i )
            {
            uint8_t val0 = charPtr[0];
            uint8_t val1 = charPtr[1];
    
            charPtr[0] = val1;
            charPtr[1] = val0;
            
            charPtr += 2;
            }
        }
    }

/******************************************************************************/

// unsigned 0..128..255 -> signed -127..0..127
void shiftTIFFLAB(  uint8_t const *in, uint8_t *out, size_t count )
{
    for ( size_t i = 0; i < count; ++i )
        {
        size_t index = 3*i;
        uint8_t l = in[index+0];
        int a = in[index+1];
        int b = in[index+2];
        out[index+0] = l; // just copy
        out[index+1] = uint8_t(a - 128);
        out[index+2] = uint8_t(b - 128);
        }
}

/******************************************************************************/

// unsigned 0..32768..65535 -> signed -32767..0..32767
void shiftTIFFLAB( uint16_t const *in, uint16_t *out, size_t count )
{
    for ( size_t i = 0; i < count; ++i )
        {
        size_t index = 3*i;
        uint16_t l = in[index+0];
        int a = in[index+1];
        int b = in[index+2];
        
        out[index+0] = l; // just copy
        out[index+1] = uint16_t(a - 0x8000);
        out[index+2] = uint16_t(b - 0x8000);
        }
}


/******************************************************************************/

/// Allocate the image buffer, set the image size
void imageBuffer::Allocate( size_t width, size_t height, int depth, int channels )
{
    assert( depth == 8 || depth == 16 );
    
    size_t bytes = depth / 8;
    
    this->depth = depth;
    this->channels = channels;
    this->width = width;
    this->height = height;
    this->rowBytes = channels * this->width * bytes;
    
    delete [] this->buffer; // just in case it was already allocated
    
    size_t totalBytes = this->rowBytes * height;
    this->buffer = new uint8_t[ totalBytes ];
    
    // be safe
    memset( this->buffer, 0, totalBytes );
}

/******************************************************************************/

/// Write the RGB or gray image buffer to a Portable Pixel Map (PPM) file
void imageBuffer::WritePPM( const char *name )  const
{
    FILE *outfile = NULL;
    
    assert( this->channels == 1 || this->channels == 3 );
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
    // write PPM file header
    if (this->channels == 1)
        fprintf(outfile,"P5\n%ld %ld\n%d\n", this->width, this->height,( (this->depth == 16) ? 65535 : 255 ) );
    else
        fprintf(outfile,"P6\n%ld %ld\n%d\n", this->width, this->height,( (this->depth == 16) ? 65535 : 255 ) );
    
    // write the image buffer
    size_t rowBytes = this->channels * this->width * ( this->depth / 8 );
    
    // swap byte order to big endian for PPM format
    if (depth == 16)
        BYTESWAP16( this->buffer, this->channels * this->width * this->height );
    
    fwrite( this->buffer, rowBytes, this->height, outfile );
    
    // swap byte order back in case it will be reused
    if (depth == 16)
        BYTESWAP16( this->buffer, this->channels * this->width * this->height );
    
    // and close the file
    fclose(outfile);
}

/******************************************************************************/

// apply predictor in-place
// assumes interleaved channels
void doHorizontalPredictor( uint8_t *buffer, int depth, int channels, size_t width, size_t height )
{
    size_t rowOffset = width * channels;
    if (depth == 8)  {
        for (size_t row = 0; row < height; ++row) {
            for (int c = 0; c < channels; ++c) {
                size_t index = row*rowOffset + c + 0;
                uint8_t lastval = buffer[ index ];
                for (size_t col = 1; col < width; ++col) {
                    uint8_t curval = buffer[ index + channels*col ];
                    buffer[ index + channels*col ] = uint8_t(curval - lastval);
                    lastval = curval;
                }
            }
        }
    }
    else if (depth == 16)  {
        uint16_t *buffer16 = (uint16_t *)buffer;
        for (size_t row = 0; row < height; ++row) {
            for (int c = 0; c < channels; ++c)  {
                size_t index = row*rowOffset + c + 0;
                uint16_t lastval = buffer16[ index ];
                for (size_t col = 1; col < width; ++col) {
                    uint16_t curval = buffer16[ index + channels*col ];
                    buffer16[ index + channels*col ] = uint16_t(curval - lastval);
                    lastval = curval;
                }
            }
        }
    }
    else {
        fprintf(stderr,"unknown predictor bit depth");
        assert(false);
    }

}

/******************************************************************************/

// reverse predictor in-place
// assumes interleaved channels
/*
Microsoft's mistake - I'm guessing they failed to account for channels correctly
Perhaps someone over-optimized the code without reading the TIFF spec?
So their broken code looks something like this:
    for (size_t row = 0; row < height; ++row) {
        size_t index = row*rowOffset;
        uint8_t lastval = buffer[ index ];
        for (size_t col = 1; col < width*channels; ++col) {
            uint8_t curval = buffer[ index + col ];
            lastval = uint8_t(curval + lastval);
            buffer[ index + col ] = lastval;
        }
    }
 */
void undoHorizontalPredictor( uint8_t *buffer, int depth, int channels, size_t width, size_t height )
{
    size_t rowOffset = width * channels;
    if (depth == 8)  {
        for (size_t row = 0; row < height; ++row) {
            for (int c = 0; c < channels; ++c) {
                size_t index = row*rowOffset + c + 0;
                uint8_t lastval = buffer[ index ];
                for (size_t col = 1; col < width; ++col) {
                    uint8_t curval = buffer[ index + channels*col ];
                    lastval = uint8_t(curval + lastval);
                    buffer[ index + channels*col ] = lastval;
                }
            }
        }
    }
    else if (depth == 16)  {
        uint16_t *buffer16 = (uint16_t *)buffer;
        for (size_t row = 0; row < height; ++row) {
            for (int c = 0; c < channels; ++c)  {
                size_t index = row*rowOffset + c + 0;
                uint16_t lastval = buffer16[ index ];
                for (size_t col = 1; col < width; ++col) {
                    uint16_t curval = buffer16[ index + channels*col ];
                    lastval = uint16_t(curval + lastval);
                    buffer16[ index + channels*col ] = lastval;
                }
            }
        }
    }
    else {
        fprintf(stderr,"unknown reverse predictor bit depth");
        assert(false);
    }

}

/******************************************************************************/

bool gUseTIFFPredictor = true;       // disable to work around bug in MSWindows TIFF code

/******************************************************************************/

/// Write the RGB image buffer to a TIFF (.tif) file
void imageBuffer::WriteTIFF( const char *name, double dpi, int color_model )  const
{
    bool usePredictor = gUseTIFFPredictor;      // to work around bug in MSWindows TIFF code
    
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL) {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
    }
    
    // TIFF header, and byte order indicator
    if (bigEndian) {
        putc('M',outfile);
        putc('M',outfile);
    } else {
        putc('I',outfile);
        putc('I',outfile);
    }

    putShort( (uint16_t)42, outfile );
    putLong( (uint32_t)8, outfile );    // offset to first IFD, from start of file

    // IFD
    // number of entries
// TODO: make a data structure to store IFD entries, sort, then write values and offsets
    uint16_t tagCount = 14;
    putShort( tagCount, outfile );
    
    uint32_t width32 = uint32_t(this->width);
    putIFDLong( TIFF_WIDTH, TIFF_LONG, 1, width32, outfile );
    uint32_t height32 = uint32_t(this->height);
    putIFDLong( TIFF_HEIGHT, TIFF_LONG, 1, height32, outfile );
    
    uint16_t bits = this->depth;
    
    uint32_t ifd_end = 8 + 2 + 4 + tagCount*12;
    uint32_t align_bytes = (4 - (ifd_end & 0x03)) & 0x03;
    uint32_t start_data = ifd_end + align_bytes;     // align to 4 byte boundary
    
    uint32_t bits_offset = start_data;
    uint32_t xres_offset = bits_offset + this->channels*2;
    uint32_t yres_offset = xres_offset + 8;
    
    // some readers break if the bitsPerSample is not a short value
    if (this->channels == 1)
        putIFDLong( TIFF_BITSPERSAMPLE, TIFF_LONG, 1, bits, outfile );
    else
        putIFDLong( TIFF_BITSPERSAMPLE, TIFF_SHORT, this->channels, bits_offset, outfile );

    putIFDLong( TIFF_COMPRESSION, TIFF_LONG, 1, TIFF_COMPRESS_LZW, outfile );

    const size_t maxStripSize = 32*1024*1024L;      // reasonable buffer on 2020s computer
    size_t rowBytes = this->channels * this->width * ( this->depth / 8 );
    size_t rowsPerBuffer = maxStripSize / rowBytes; // fit whole rows
    size_t stripCount = (this->height+rowsPerBuffer-1) / rowsPerBuffer;
    stripCount = std::min( this->height, std::max( size_t(1), stripCount) );
    assert( stripCount >= 1 );
    assert( stripCount <= UINT_MAX );
    
    size_t rowsPerStrip = rowsPerBuffer;
    rowsPerStrip = std::min( this->height, std::max( size_t(1), rowsPerStrip) );
    assert( rowsPerStrip >= 1 );
    assert( rowsPerStrip <= this->height );
    assert( rowsPerStrip <= UINT_MAX );
    size_t stripBytes = rowsPerStrip * rowBytes;
    assert( stripBytes <= maxStripSize );
    
    uint32_t stripOffset_offset = yres_offset + 8;
    
    size_t stripCountSize = stripCount * 4;
    assert( (stripOffset_offset + stripCountSize) <= UINT_MAX );
    uint32_t stripByteCount_offset = uint32_t( stripOffset_offset + stripCountSize );
    
    assert( (stripByteCount_offset + stripCountSize) <= UINT_MAX );
    uint32_t pixelData_offset = uint32_t ( stripByteCount_offset + stripCountSize );

    putIFDLong( TIFF_INTERPRETATION, TIFF_LONG, 1, color_model, outfile );

    // using an offset for offsets and bytecounts trips up tiffinfo
    if (stripCount > 1)
        putIFDLong( TIFF_STRIPOFFSETS, TIFF_LONG, uint32_t(stripCount), stripOffset_offset, outfile );
    else
        putIFDLong( TIFF_STRIPOFFSETS, TIFF_LONG, 1, pixelData_offset, outfile );
    
    putIFDLong( TIFF_SAMPLESPERPIXEL, TIFF_LONG, 1, this->channels, outfile );
    putIFDLong( TIFF_ROWSPERSTRIP, TIFF_LONG, 1, uint32_t(rowsPerStrip), outfile );

    long byteCountOffset = ftell( outfile );
    if (stripCount > 1)
        putIFDLong( TIFF_STRIPBYTECOUNTS, TIFF_LONG, uint32_t(stripCount), stripByteCount_offset, outfile );
    else
        putIFDLong( TIFF_STRIPBYTECOUNTS, TIFF_LONG, 1, 0, outfile );

    uint32_t resDenom32 = 1000;
    uint32_t resRatio32 = dpi * resDenom32;
    putIFDLong( TIFF_XRESOLUTION, TIFF_RATIO, 1, xres_offset, outfile );
    putIFDLong( TIFF_YRESOLUTION, TIFF_RATIO, 1, yres_offset, outfile );
    putIFDLong( TIFF_PLANARCONFIG, TIFF_LONG, 1, 1, outfile );
    putIFDLong( TIFF_RESOLUTIONUNIT, TIFF_LONG, 1, 2, outfile );    // inches
    
    if (usePredictor) {     // Windows has a bug in their predictor reading code
        putIFDLong( TIFF_PREDICTOR, TIFF_LONG, 1, 2, outfile );    // horizontal predictor
    } else {
        putIFDLong( TIFF_PREDICTOR, TIFF_LONG, 1, 1, outfile );    // no predictor
    }

    putLong( (uint32_t)0, outfile );    // offset to next IFD
    
    // align to 4 byte boundary
    for (int i = 0; i < align_bytes; ++i)
        putc( 0, outfile );

// bits_offset:
    // bits per sample, because some readers break if it's just a byte instead of short
    for (int i = 0; i < this->channels; ++i)
        putShort( bits, outfile );
    
    // resolution ratios
// xres_offset:
    putLong( resRatio32, outfile );    // X dpi
    putLong( resDenom32, outfile );    // denominator

// yres_offset:
    putLong( resRatio32, outfile );    // Y dpi
    putLong( resDenom32, outfile );    // denominator

// stripOffset_offset
    // file with zeros, then backfill once we compress the strips
    for (size_t i = 0; i < stripCount; ++i)
        putLong( 0, outfile );

// stripByteCount_offset
    // file with zeros, then backfill once we compress the strips
    for (size_t i = 0; i < stripCount; ++i)
        putLong( 0, outfile );
    
// pixelData_offset:
    // Pixel Data


    std::vector<uint32_t> stripOffsetList( stripCount );
    std::vector<uint32_t> stripSizeList( stripCount );

    TIFF tiff_data;
    TIFFInitLZW( &tiff_data, COMPRESSION_LZW );
    
    const size_t huge_block = 2 * 1024 * 1024;      // large-ish buffer for streaing LZW output
    tiff_data.tif_rawdata = (uint8_t *)malloc( huge_block );
    tiff_data.tif_setupencode( &tiff_data );

    uint8_t *stripBuffer = new uint8_t[ stripBytes ];
    
    for (size_t strip = 0; strip < stripCount; ++strip) {
    
        size_t startRow = strip * rowsPerStrip;
        size_t rowCount = rowsPerStrip;
        if (startRow + rowsPerStrip > this->height)
            rowCount = this->height - startRow;

        size_t stripBytes = rowCount * rowBytes;
        size_t offset = startRow * rowBytes;
        memcpy( stripBuffer, this->buffer + offset, stripBytes );

        if (color_model == TIFF_MODE_CIELAB) {
            if (this->depth == 8) {
                shiftTIFFLAB( this->buffer  + offset, stripBuffer, this->width * rowCount );
            } else if (this->depth == 16) {
                shiftTIFFLAB( (uint16_t *)(this->buffer + offset) , (uint16_t *)stripBuffer, this->width * rowCount );
            }
        }

        if (usePredictor) {     // to work around bug in MSWindows TIFF code
            // Apply Preditor to buffer (in-place)
            doHorizontalPredictor( stripBuffer, this->depth, this->channels, this->width, rowCount );
        }

        long stripStart = ftell( outfile );

        tiff_data.totalCompressedSize = 0;
        tiff_data.output = outfile;

        // allocate buffer for output
        tiff_data.tif_rawdatasize = huge_block;
        tiff_data.tif_rawcp = tiff_data.tif_rawdata;
        tiff_data.tif_rawcc = 0;    // tiff_data.tif_rawdatasize;

        tiff_data.tif_preencode( &tiff_data, 0 );
        tiff_data.tif_encodestrip( &tiff_data, stripBuffer, stripBytes, 0 );
        tiff_data.tif_postencode( &tiff_data );
        TIFFFlushData1( &tiff_data );
        
        long stripEnd = ftell( outfile );
        assert( stripStart < UINT_MAX );
        stripOffsetList[strip] = uint32_t(stripStart);
        size_t len = stripEnd - stripStart;
        assert ( len < UINT_MAX );
        stripSizeList[strip] = uint32_t(len);

    }   // for strip

    // delete raw buffers
    tiff_data.tif_cleanup( &tiff_data );
    free(tiff_data.tif_rawdata);
    delete[] stripBuffer;

    // and update our strip offsets and sizes
    if (stripCount == 1) {
        fseek(outfile, byteCountOffset, SEEK_SET);
        assert( tiff_data.totalCompressedSize < UINT_MAX );
        uint32_t compressed = uint32_t(tiff_data.totalCompressedSize);
        putIFDLong( TIFF_STRIPBYTECOUNTS, TIFF_LONG, 1, compressed, outfile );
    } else {
        fseek(outfile, stripOffset_offset, SEEK_SET);
        for (size_t i = 0; i < stripCount; ++i)
            putLong( stripOffsetList[i], outfile );
    
        fseek(outfile, stripByteCount_offset, SEEK_SET);
        for (size_t i = 0; i < stripCount; ++i)
            putLong( stripSizeList[i], outfile );
    }

    
    // and close the file
    fclose(outfile);
}

/******************************************************************************/

extern "C" int TIFFFlushData1(TIFF *tif)
{
    if (tif->tif_rawcc > 0) {
        // write current data to file
        size_t result = fwrite(tif->tif_rawdata, 1, tif->tif_rawcc, tif->output );
        if (result != tif->tif_rawcc)
            return 0;   // error!
        
        // update state for size of compressed data written
        tif->totalCompressedSize += tif->tif_rawcc;
        
        // reset pointers
        tif->tif_rawcc = 0;
        tif->tif_rawcp = tif->tif_rawdata;
    }
    return 1;   // success
}

/******************************************************************************/

void imageBuffer::SetPixel( int x, int y, double g )
{
    if (x < 0 || x >= width)
        return;
    if (y < 0 || y >= height)
        return;
    
    assert(this->channels == 1);
    
    size_t index = 1 * (y * width + x);

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)buffer;
        buffer16[ index+0 ] = uint16_t(floor(g*65535.0));
    } else {
        buffer[ index+0 ] = uint8_t(floor(g*255.0));
    }
}

/******************************************************************************/

void imageBuffer::SetPixel( int x, int y, double r, double g, double b )
{
    if (x < 0 || x >= width)
        return;
    if (y < 0 || y >= height)
        return;
        
    assert(this->channels == 3);
    
    size_t index = 3 * (y * width + x);

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)buffer;
        buffer16[ index+0 ] = uint16_t(floor(r*65535.0));
        buffer16[ index+1 ] = uint16_t(floor(g*65535.0));
        buffer16[ index+2 ] = uint16_t(floor(b*65535.0));
    } else {
        buffer[ index+0 ] = uint8_t(floor(r*255.0));
        buffer[ index+1 ] = uint8_t(floor(g*255.0));
        buffer[ index+2 ] = uint8_t(floor(b*255.0));
    }
}

/******************************************************************************/

void imageBuffer::SetPixel( int x, int y, double c, double m, double yy, double k )
{
    if (x < 0 || x >= width)
        return;
    if (y < 0 || y >= height)
        return;
    
    assert(this->channels == 4);
    
    size_t index = 4 * (size_t(y) * width + x);

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)buffer;
        buffer16[ index+0 ] = uint16_t(floor(c*65535.0));
        buffer16[ index+1 ] = uint16_t(floor(m*65535.0));
        buffer16[ index+2 ] = uint16_t(floor(yy*65535.0));
        buffer16[ index+3 ] = uint16_t(floor(k*65535.0));
    } else {
        buffer[ index+0 ] = uint8_t(floor(c*255.0));
        buffer[ index+1 ] = uint8_t(floor(m*255.0));
        buffer[ index+2 ] = uint8_t(floor(yy*255.0));
        buffer[ index+3 ] = uint8_t(floor(k*255.0));
    }
}

/******************************************************************************/

void imageBuffer::FillRect( int left, int top, int right, int bottom, double g )
{
    top = std::max( top, (int)0 );
    left = std::max( left, (int)0 );
    bottom = std::min( bottom, (int)(this->height) );
    right = std::min( right, (int)(this->width) );
    
    assert(this->channels == 1);
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)this->buffer;
        uint16_t value16 = uint16_t(floor(g*65535.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x)
                buffer16[ y * this->width + x ] = value16;
    } else {
        uint8_t value8 = uint8_t(floor(g*255.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x)
                this->buffer[ y * this->width + x ] = value8;
    }
}

/******************************************************************************/

void imageBuffer::FillRect( int left, int top, int right, int bottom, double r, double g, double b )
{
    top = std::max( top, (int)0 );
    left = std::max( left, (int)0 );
    bottom = std::min( bottom, (int)(this->height) );
    right = std::min( right, (int)(this->width) );
    
    assert(this->channels == 3);
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)buffer;
        uint16_t red16 = uint16_t(floor(r*65535.0));
        uint16_t green16 = uint16_t(floor(g*65535.0));
        uint16_t blue16 = uint16_t(floor(b*65535.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x) {
                buffer16[ y * this->width*3 + x*3 + 0 ] = red16;
                buffer16[ y * this->width*3 + x*3 + 1 ] = green16;
                buffer16[ y * this->width*3 + x*3 + 2 ] = blue16;
            }
    } else {
        uint8_t red8 = uint8_t(floor(r*255.0));
        uint8_t green8 = uint8_t(floor(g*255.0));
        uint8_t blue8 = uint8_t(floor(b*255.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x) {
                buffer[ y * this->width*3 + x*3 + 0 ] = red8;
                buffer[ y * this->width*3 + x*3 + 1 ] = green8;
                buffer[ y * this->width*3 + x*3 + 2 ] = blue8;
            }
    }
}

/******************************************************************************/

void imageBuffer::FillRect( int left, int top, int right, int bottom, double c, double m, double yy, double k )
{
    top = std::max( top, (int)0 );
    left = std::max( left, (int)0 );
    bottom = std::min( bottom, (int)(this->height) );
    right = std::min( right, (int)(this->width) );
    
    assert(this->channels == 4);
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );

    // store the valuess
    if (depth == 16) {
        uint16_t *buffer16 = (uint16_t *)buffer;
        uint16_t cyan16 = uint16_t(floor(c*65535.0));
        uint16_t magenta16 = uint16_t(floor(m*65535.0));
        uint16_t yellow16 = uint16_t(floor(yy*65535.0));
        uint16_t black16 = uint16_t(floor(k*65535.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x) {
                buffer16[ y * this->width*4 + x*4 + 0 ] = cyan16;
                buffer16[ y * this->width*4 + x*4 + 1 ] = magenta16;
                buffer16[ y * this->width*4 + x*4 + 2 ] = yellow16;
                buffer16[ y * this->width*4 + x*4 + 3 ] = black16;
            }
    } else {
        uint8_t cyan8 = uint8_t(floor(c*255.0));
        uint8_t magenta8 = uint8_t(floor(m*255.0));
        uint8_t yellow8 = uint8_t(floor(yy*255.0));
        uint8_t black8 = uint8_t(floor(k*255.0));
        for (size_t y = top; y < bottom; ++y)
            for (size_t x = left; x < right; ++x) {
                buffer[ y * this->width*4 + x*4 + 0 ] = cyan8;
                buffer[ y * this->width*4 + x*4 + 1 ] = magenta8;
                buffer[ y * this->width*4 + x*4 + 2 ] = yellow8;
                buffer[ y * this->width*4 + x*4 + 3 ] = black8;
            }
    }
}

/******************************************************************************/
/******************************************************************************/

# Color Test Images

This project generates many of the images that I use for testing color profiles and color conversions, plus a few requested for teaching/education.  Currently generates 82 files with default options, and a lot more if you choose to plot all the chromaticity data.

![very colorful, much chroma.](Logo400.jpg  "composite example of some generated images")

The images are output as LZW compressed TIFF files: RGB, LAB, CMYK, and grayscale.
Chromaticity diagram parts are output as a single, layered SVG file.<P>
The default resolution is 100dpi, but can be changed from the command line.  Most images are about 8 inches on a side.<P>
The default pixel depth is 8 bit/channel, but the depth can be changed to 16 bit/channel on the command line.<P>
This should be buildable on most platforms from the command line using the included makefile. An XCode project is included to make it easier for me to debug and test.<P>


Windows users: Microsoft applications appear to have a bug in their TIFF code and fail to read many of the TIFF files generated.
These same files work without error in LibTIFF, Photoshop, GIMP, Apple apps, Linux, MS Office on MacOS (?!?!), etc.
The bug appears to be in the horizontal difference predictor of Microsoft's TIFF reading code.
I have filed a bug report ( https://aka.ms/AAol5q2 ), but have no idea how long it will take Microsoft to fix their code and ship the fixes.
In the meantime, I have added a flag "no_tiff_predictor" for Windows users to work around the bug, but the size of the files increases quite a bit (8.2MB -> 190MB at default settings).

Windows users: Microsoft applications do not support images in LAB color mode.


Additional documentation and examples of using some of the test images can be found at https://gitlab.com/chriscox/color_test_images/-/wikis/home


## License
MIT License


## Credits
1) tif_lzw.c is from libtiff
   - https://gitlab.com/libtiff/libtiff
   - MIT License

2) cmscam02.c and cmscam16.c are from Little-CMS
   - https://github.com/mm2/Little-CMS
   - MIT License


## Apologies
My apologies in advance to any coworkers for whom this brings back memories of (numerous) color conversion bug reports.


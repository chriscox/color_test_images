//
//  main.cpp
//  Color Test Images
//
//  Created by Chris Cox on December 27, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include "image.hpp"
#include "ramps.hpp"
#include "SVG.hpp"
#include "chromaticity.hpp"

using namespace std;

/******************************************************************************/

bool use16Bit = false;

double resolution = 100.0;  // 100 ppi, screen res

bool writeChromaticityDetailTIFF = false;

/******************************************************************************/

static
void print_usage(const char *argv[])
{
    cout << "Usage: " << argv[0] << " <args>\n";
    cout << "\t-deep       Use 16 bit/channel (default " << use16Bit << ")\n";
    cout << "\t-ppi D      Resolution in pixels/inch (default " << resolution << ")\n";
    cout << "\t-no_tiff_predictor    Disable TIFF predictor to work around bug in MSWindows (default " << !gUseTIFFPredictor << ")\n";
    cout << "\t-cb         Write images for all chromaticity details (default " << writeChromaticityDetailTIFF << ")\n";
}

/******************************************************************************/

static
void parse_arguments( int argc, const char *argv[] )
{
    for ( int c = 1; c < argc; ++c )
        {
        
        if ( strcmp( argv[c], "-no_tiff_predictor" ) == 0
            || strcmp( argv[c], "-NO_TIFF_PREDICTOR" ) == 0
            || strcmp( argv[c], "-No_Tiff_Predictor" ) == 0 )
            {
            gUseTIFFPredictor = false;
            }
        else if ( strcmp( argv[c], "-deep" ) == 0
            || strcmp( argv[c], "-Deep" ) == 0
            || strcmp( argv[c], "-DEEP" ) == 0 )
            {
            use16Bit = true;
            }
        else if ( strcmp( argv[c], "-cb" ) == 0
            || strcmp( argv[c], "-Cb" ) == 0
            || strcmp( argv[c], "-CB" ) == 0 )
            {
            writeChromaticityDetailTIFF = true;
            }
        else if ( (strcmp( argv[c], "-ppi" ) == 0
                || strcmp( argv[c], "-PPI" ) == 0
                || strcmp( argv[c], "-dpi" ) == 0
                || strcmp( argv[c], "-DPI" ) == 0)
                && c < (argc-1) )
            {
            resolution = atof( argv[c+1] );
            ++c;
            }
        else if ( strcmp( argv[c], "-help" ) == 0
                || strcmp( argv[c], "--help" ) == 0 )
            {
            print_usage( argv );
            exit (0);
            }
        else if (argv[c][0] == '-')
            {
            // unrecognized option
            cout << "Unrecognized or unterminated option: " << argv[c] << "\n";
            print_usage( argv );
            exit (1);
            }
        else
            {
            //infiles.push_back( argv[c] );
            }
        
        }   // end for argc
}

/******************************************************************************/
/******************************************************************************/

int main(int argc, const char * argv[]) {

    parse_arguments( argc, argv );

    int depth = use16Bit ? 16 : 8;


    // color/profile converstion testing images
    makeLABRamps( resolution, depth );
    makeRGBRamps( resolution, depth );
    makeHSLRamps( resolution, depth );
    makeHSVRamps( resolution, depth );
    makeCMYKRamps( resolution, depth );
    makeLongPrimaryRamps( resolution, depth );
    makeQuantizedRamps( resolution, depth );

    DoHueCircles( resolution, depth );
    DoHueRectangles( resolution, depth );
    makeFullGamutFiles();
    
    
    // xy chromaticity diagram and related masks
    // use a global size to match bitmaps and SVG
    size_t chromaticity_plot_size = 8.0 * resolution;
    
    DoChromaticityBackground( chromaticity_plot_size, resolution, depth );
    DoChromaticityCurve( chromaticity_plot_size, resolution, depth, writeChromaticityDetailTIFF );
    plotAllPhosphors( chromaticity_plot_size, resolution, depth, writeChromaticityDetailTIFF );
    plotAllInks( chromaticity_plot_size, resolution, depth, writeChromaticityDetailTIFF );
    plotPlanckianLocus( chromaticity_plot_size, resolution, depth, writeChromaticityDetailTIFF );
    plotAllIlluminants( chromaticity_plot_size, resolution, depth, writeChromaticityDetailTIFF );
    // memory colors?   not useful without text, and ellipses or area definition
    // named regions?   needs polygon definitions that are consistent, and not useful without text.

    std::ofstream out( "Chromaticity.svg" );
    point2D size( chromaticity_plot_size, chromaticity_plot_size );
    ExportSVGFile( out, svgObjects, size );
    out.close();

    return 0;
}

/******************************************************************************/
/******************************************************************************/

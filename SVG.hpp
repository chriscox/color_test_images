//
//  SVG.hpp
//      Write simple SVG files
//      from https://gitlab.com/chriscox/xcs-tools
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#include <cstdint>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

/******************************************************************************/

struct point2D {
    point2D(double xx, double yy) : x(xx), y(yy) {}
    point2D() : x(0.0), y(0.0) {}
    double x, y;

public:

#if __cplusplus >= 202000UL
    auto operator<=>(const point2D&) const = default;
#else
    bool operator==(const point2D& b) const
                { return x == b.x && y == b.y; }
    bool operator!=(const point2D& b) const
                { return x != b.x || y != b.y; }
#endif

};

inline point2D operator+(const point2D& xx, const point2D& yy) {
    return point2D(xx.x + yy.x, xx.y + yy.y);
}

inline point2D operator-(const point2D& xx, const point2D& yy) {
    return point2D(xx.x - yy.x, xx.y - yy.y);
}

inline point2D operator*(const point2D& xx, const point2D& yy) {
    return point2D(xx.x * yy.x, xx.y * yy.y);
}

inline point2D operator/(const point2D& xx, const point2D& yy) {
    return point2D(xx.x / yy.x, xx.y / yy.y);
}

inline point2D operator*(const double s, const point2D& yy) {
    return point2D(s * yy.x, s * yy.y);
}

typedef vector<point2D> pointList;

/******************************************************************************/

struct rect2D {
    rect2D(double xmin, double ymin, double xmax, double ymax) :
            minX(xmin), maxX(xmax), minY(ymin), maxY(ymax) {}
    
    rect2D() : minX(0), maxX(0), minY(0), maxY(0) {}
    
    bool isNull() const { return minX == 0.0 && minY == 0.0 && maxX == 0.0 && maxY == 0.0; }
    
    double minX, maxX, minY, maxY;
};

/******************************************************************************/

struct object_data {

    object_data() : height(0), width(0), x(0), y(0), isClosePath(true),
                    isFill(false)
                    {}

    double height;
    double width;
    double x;
    double y;
    bool isClosePath;
    bool isFill;
    string  type;   // CIRCLE, RECT, LINE, PATH, PEN, TEXT, BITMAP

    pointList points;           // POLYLINE
    string text;                // TEXT only
    string fontFamily;          // TEXT only
    string fontSubfamily;       // TEXT only
    string text_align;          // TEXT only
    double fontSize;            // TEXT only
    string dPath;               // PATH only
    
};

typedef vector<object_data> objectList;

extern objectList svgObjects;

/******************************************************************************/

extern void AddSVGCircle( objectList &list, const point2D &center, double radius, bool isFill );
extern void AddSVGEllipse( objectList &list, const point2D &center, double radiusX, double radiusY, bool isFill );
extern void AddSVGLine( objectList &list, const point2D &start, const point2D &end );
extern void AddSVGRect( objectList &list, const point2D &topLeft, const point2D &bottomRight, bool isFill );
extern void AddSVGPolyline( objectList &list, const pointList &points, bool isClosed, bool isFill );
extern void AddSVGText( objectList &list, const point2D &pt, string &text, double size, string &font, string &style, string &align );
extern void SVGStartGroup( objectList &list, const string &name );
extern void SVGEndGroup( objectList &list );

/******************************************************************************/

void ExportSVGFile( std::ostream &out, objectList &doc, const point2D &size );

/******************************************************************************/
/******************************************************************************/

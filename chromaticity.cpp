//
//  chromaticity.cpp
//  Color Test Images
//
//  Created by Chris Cox on 1/22/24.
//


#include <cstdio>
#include <cmath>
#include <iostream>
#include <cassert>
#include <cstring>
#include <vector>
#include <algorithm>

#include "ramps.hpp"
#include "image.hpp"
#include "lines.h"
#include "SVG.hpp"

#include "chromaticity.hpp"

/******************************************************************************/
/******************************************************************************/

const double ChromaticityBackgroundBrightness = 0.4;
    
void ConvertXYYtoXYZ (const XYYColor &in, XYZColor &out)
{
    double tempY = in.Y;
	double newX = in.x;
	double newY = in.y;

	double scale = newX + newY;

	if (scale > 1.0) {
		newX /= scale;
		newY /= scale;
	}

	double tempX = 0.0;
    double tempZ = 0.0;
	if ( fabs(newY) > 1.0e-12 ) {
        double scaleY = tempY / newY;
		tempX = newX				* scaleY;
		tempZ = (1.0 - newX - newY) * scaleY;
    } else {
		tempX = 0.0;
		tempZ = 0.0;
    }
	
	// Clip to valid XYZ range
	double clipValue = std::max( tempY, tempZ );
	clipValue = std::max( tempX, clipValue );

#if 1
	// this may be necessary for the chromaticity chart
	// but not for the other charts!
	if (clipValue > ChromaticityBackgroundBrightness)
		{
		double scale = ChromaticityBackgroundBrightness / clipValue;
		tempX = tempX * scale;
		tempY = tempY * scale;
		tempZ = tempZ * scale;
		}
#else
	if (clipValue > 1.0)
		{
		double scale = 1.0 / clipValue;
		tempX = tempX * scale;
		tempY = tempY * scale;
		tempZ = tempZ * scale;
		}
#endif

	// some few values are still out of range -- at the limits of FP precision
    tempX = std::max( 0.0, std::min( 1.0, tempX ));
    tempY = std::max( 0.0, std::min( 1.0, tempY ));
    tempZ = std::max( 0.0, std::min( 1.0, tempZ ));

	out.x = tempX;
	out.y = tempY;
	out.z = tempZ;
}

/******************************************************************************/

const double chromaticityChartHorizScale = 0.9;
const double chromaticityChartVertScale = 0.8;

void DoChromaticityBackground(size_t IMAGE_SIZE, double res, int depth)
{
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

	// calculate a few useful values
	double row_scale = chromaticityChartHorizScale / (double)IMAGE_SIZE;
	double col_scale = chromaticityChartVertScale / (double)IMAGE_SIZE;

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
            XYYColor	XYY;
            XYZColor    XYZ;
            LABColor	Lab;
		
			// setup coordinates based on image position
			XYY.x = (double)col * col_scale;
			XYY.y = (double)(IMAGE_SIZE-row) * row_scale;
			XYY.Y = ChromaticityBackgroundBrightness;
			
			// do the conversions
			ConvertXYYtoXYZ( XYY, XYZ );
   
            XYZ.x *= 100.0;
            XYZ.y *= 100.0;
            XYZ.z *= 100.0;
            
			ConvertXYZtoLAB( XYZ, Lab );
				
            image.SetPixel( col, row, Lab.L/255.0, Lab.a/255.0, Lab.b/255.0 );
		}
	}

    image.WriteTIFF( "chromaticity_background.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

#include "chart_curve.h"
#include "chart_data.h"

void DoChromaticityCurve(size_t IMAGE_SIZE, double res, int depth, bool writeImage)
{
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 1 );

    GFrameBuffer	myFrameBuffer;

    // setup the framebuffer
    myFrameBuffer.BufferPtr = image.buffer;
    myFrameBuffer.RowBytes = IMAGE_SIZE;
    myFrameBuffer.BufferRect.top = 0;
    myFrameBuffer.BufferRect.left = 0;
    myFrameBuffer.BufferRect.bottom = IMAGE_SIZE;
    myFrameBuffer.BufferRect.right = IMAGE_SIZE;

    // calculate a few useful values
    double row_scale = (double)IMAGE_SIZE / chromaticityChartHorizScale;
    double col_scale = (double)IMAGE_SIZE / chromaticityChartVertScale;
 
    pointList svgList;

    // scan over the curve and plot the lines
    int lastX = CurveData2degree[0].x * col_scale;
    int lastY = IMAGE_SIZE - (CurveData2degree[0].y * row_scale);
    svgList.push_back( point2D(lastX,lastY) );
	
	for ( int i = 1; i < (sizeof(CurveData2degree)/sizeof(spectral_curve)); i++ ) {
		
        double fx = CurveData2degree[i].x * col_scale;
        double fy = IMAGE_SIZE - (CurveData2degree[i].y * row_scale);
		int thisX = floor(fx);
		int thisY = floor(fy);
		DrawLine( &myFrameBuffer, lastX, lastY, thisX, thisY, 0xFFFFFFFF );
        svgList.push_back( point2D(fx,fy) );
        
		lastX = thisX;
		lastY = thisY;
	}
	
    // and finally the magenta line
    int thisX = CurveData2degree[0].x * col_scale;
    int thisY = IMAGE_SIZE - (CurveData2degree[0].y * row_scale);
    DrawLine( &myFrameBuffer, lastX, lastY, thisX, thisY, 0xFFFFFFFF );

    SVGStartGroup( svgObjects, "Spectral Locus" );
    AddSVGPolyline( svgObjects,svgList, true, false );
    SVGEndGroup( svgObjects );

    if (writeImage)
        image.WriteTIFF( "chromaticity_curve.tif", res, TIFF_MODE_GRAY_BLACKZERO );
}

/******************************************************************************/

void plotPhosphorSet( GFrameBuffer *myFrameBuffer, DisplayPrimaries &phosphors )
{
    const long image_size = myFrameBuffer->BufferRect.bottom - myFrameBuffer->BufferRect.top;

    double row_scale = (double)image_size / chromaticityChartHorizScale;
    double col_scale = (double)image_size / chromaticityChartVertScale;

    size_t primaryCount = phosphors.values.size();

    pointList svgList;
    SVGStartGroup( svgObjects, phosphors.name );
    
    for ( size_t i = 0; i < primaryCount; ++i )
        {
        double fx = phosphors.values[i].x * col_scale;
        double fy = image_size - phosphors.values[i].y * row_scale;
        int startX = floor(fx);
        int startY = floor(fy);
        svgList.push_back( point2D(fx,fy) );

        // next primary, or wrap back to start
        size_t next_index = i + 1;
        if (next_index >= primaryCount)
            next_index = 0;

        int endX = phosphors.values[ next_index ].x * col_scale;
        int endY = image_size - phosphors.values[ next_index ].y * row_scale;
        DrawLine( myFrameBuffer, startX, startY, endX, endY, 0xFFFFFFFF );
        }
 
    AddSVGPolyline( svgObjects, svgList, true, false );
    point2D labelPt(  phosphors.values[1].x * col_scale,
                    image_size - phosphors.values[1].y * row_scale );
    string labelText = phosphors.name;
    string font = "Arial";
    string style = "Bold";
    string align = "Left";
    double fontSize = 24.0 * image_size / 800.0;
    AddSVGText( svgObjects, labelPt, labelText, fontSize, font, style, align );
    SVGEndGroup( svgObjects );
}

/******************************************************************************/

void plotAllPhosphors(size_t IMAGE_SIZE, double res, int depth, bool writeImage)
{
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 1 );

    GFrameBuffer	myFrameBuffer;

    // setup the framebuffer
    myFrameBuffer.BufferPtr = image.buffer;
    myFrameBuffer.RowBytes = IMAGE_SIZE;
    myFrameBuffer.BufferRect.top = 0;
    myFrameBuffer.BufferRect.left = 0;
    myFrameBuffer.BufferRect.bottom = IMAGE_SIZE;
    myFrameBuffer.BufferRect.right = IMAGE_SIZE;
    
    SVGStartGroup( svgObjects, "Displays and Colorspaces" );
	for ( size_t i = 0; i < DisplayPrimaryList.size(); ++i ) {
        const size_t filename_limit = 512;
        char filename[filename_limit];

        filename[0] = 0;
        snprintf(filename, filename_limit, "display %s.tif", DisplayPrimaryList[i].name );

        // zero the bitmap
        memset( myFrameBuffer.BufferPtr, 0, IMAGE_SIZE * IMAGE_SIZE );

        // plot the phosphors
        plotPhosphorSet( &myFrameBuffer, DisplayPrimaryList[i] );

        if (writeImage)
            image.WriteTIFF( filename, res, TIFF_MODE_GRAY_BLACKZERO );
	}
    SVGEndGroup( svgObjects );
}

/******************************************************************************/

void plotInkSet( GFrameBuffer *myFrameBuffer, CMYKprimaries &inks )
{
    const long image_size = myFrameBuffer->BufferRect.bottom - myFrameBuffer->BufferRect.top;

    double row_scale = (double)image_size / (chromaticityChartHorizScale * 10000.0);
    double col_scale = (double)image_size / (chromaticityChartVertScale * 10000.0);

    pointList svgList;

    int CyanX = inks.CyanX * col_scale;
    int CyanY = image_size - (inks.CyanY * row_scale);
    svgList.push_back( point2D(CyanX,CyanY) );

    int BlueX = inks.BlueX * col_scale;
    int BlueY = image_size - (inks.BlueY * row_scale);
    svgList.push_back( point2D(BlueX,BlueY) );

    int MagentaX = inks.MagentaX * col_scale;
    int MagentaY = image_size - (inks.MagentaY * row_scale);
    svgList.push_back( point2D(MagentaX,MagentaY) );

    int RedX = inks.RedX * col_scale;
    int RedY = image_size - (inks.RedY * row_scale);
    svgList.push_back( point2D(RedX,RedY) );

    int YellowX = inks.YellowX * col_scale;
    int YellowY = image_size - (inks.YellowY * row_scale);
    svgList.push_back( point2D(YellowX,YellowY) );

    int GreenX = inks.GreenX * col_scale;
    int GreenY = image_size - (inks.GreenY * row_scale);
    svgList.push_back( point2D(GreenX,GreenY) );

    DrawLine( myFrameBuffer, CyanX, CyanY, BlueX, BlueY, 0xFFFFFFFF );
    DrawLine( myFrameBuffer, BlueX, BlueY, MagentaX, MagentaY, 0xFFFFFFFF );
    DrawLine( myFrameBuffer, MagentaX, MagentaY, RedX, RedY, 0xFFFFFFFF );
    DrawLine( myFrameBuffer, RedX, RedY, YellowX, YellowY, 0xFFFFFFFF );
    DrawLine( myFrameBuffer, YellowX, YellowY, GreenX, GreenY, 0xFFFFFFFF );
    DrawLine( myFrameBuffer, GreenX, GreenY, CyanX, CyanY, 0xFFFFFFFF );

    SVGStartGroup( svgObjects,  inks.name);
    AddSVGPolyline( svgObjects, svgList, true, false );
    point2D labelPt( CyanX, CyanY);
    string labelText = inks.name;
    string font = "Arial";
    string style = "Bold";
    string align = "Right";
    double fontSize = 24.0 * image_size / 800.0;
    AddSVGText( svgObjects, labelPt, labelText, fontSize, font, style, align );
    SVGEndGroup( svgObjects );
}

/******************************************************************************/

void plotAllInks(size_t IMAGE_SIZE, double res, int depth, bool writeImage)
{
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 1 );

    GFrameBuffer	myFrameBuffer;

    // setup the framebuffer
    myFrameBuffer.BufferPtr = image.buffer;
    myFrameBuffer.RowBytes = IMAGE_SIZE;
    myFrameBuffer.BufferRect.top = 0;
    myFrameBuffer.BufferRect.left = 0;
    myFrameBuffer.BufferRect.bottom = IMAGE_SIZE;
    myFrameBuffer.BufferRect.right = IMAGE_SIZE;

    SVGStartGroup( svgObjects, "Inks" );
	for ( size_t i = 0; i < PrinterPrimaries.size(); ++i ) {
        char filename[256];

        snprintf(filename,256, "inks %s.tif", PrinterPrimaries[i].name );

        // zero the bitmap
        memset( myFrameBuffer.BufferPtr, 0, IMAGE_SIZE * IMAGE_SIZE );

        // plot the phosphors
        plotInkSet( &myFrameBuffer, PrinterPrimaries[i] );

        if (writeImage)
            image.WriteTIFF( filename, res, TIFF_MODE_GRAY_BLACKZERO );
	}
    SVGEndGroup( svgObjects );
}

/******************************************************************************/

// https://en.wikipedia.org/wiki/Planckian_locus
XYColor approx_planck( double t )
{
    const double c3a = -0.2661239;
    const double c2a = -0.2343589;
    const double c1a =  0.8776956;
    const double c0a =  0.179910;
    
    const double c3b = -3.0258469;
    const double c2b =  2.1070379;
    const double c1b =  0.2226347;
    const double c0b =  0.240390;
    
    const double k3a = -1.1063814;
    const double k2a = -1.34811020;
    const double k1a =  2.18555832;
    const double k0a = -0.20219683;
    
    const double k3b = -0.9549476;
    const double k2b = -1.37418593;
    const double k1b =  2.09137015;
    const double k0b = -0.16748867;
    
    const double k3c =  3.0817580;
    const double k2c = -5.87338670;
    const double k1c =  3.75112997;
    const double k0c = -0.37001483;
    
    double t2 = t*t;
    double t3 = t*t*t;
    
    double x = 0.0;
    
    if (t < 4000.0) {
        x = c3a*(1e9/t3) + c2a*(1e6/t2) + c1a*(1e3/t) + c0a;
    } else {
        x = c3b*(1e9/t3) + c2b*(1e6/t2) + c1b*(1e3/t) + c0b;
    }
    
    double x2 = x*x;
    double x3 = x*x*x;
    
    double y = 0.0;
    
    if (t < 2222.0) {
        y = k3a*x3 + k2a*x2 + k1a*x + k0a;
    } else if (t < 4000.0) {
        y = k3b*x3 + k2b*x2 + k1b*x + k0b;
    } else {
        y = k3c*x3 + k2c*x2 + k1c*x + k0c;
    }
    
    XYColor result = {x,y};
    return result;
}

/******************************************************************************/

void plotPlanckianLocus(size_t IMAGE_SIZE, double res, int depth, bool writeImage)
{
    const double start_temp = 1667.0;   // degrees Kelvin
    const double end_temp = 25000.0;
    const double temp_step = 200.0;

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 1 );

    GFrameBuffer	myFrameBuffer;

    // setup the framebuffer
    myFrameBuffer.BufferPtr = image.buffer;
    myFrameBuffer.RowBytes = IMAGE_SIZE;
    myFrameBuffer.BufferRect.top = 0;
    myFrameBuffer.BufferRect.left = 0;
    myFrameBuffer.BufferRect.bottom = IMAGE_SIZE;
    myFrameBuffer.BufferRect.right = IMAGE_SIZE;

    // calculate a few useful values
    double row_scale = (double)IMAGE_SIZE / chromaticityChartHorizScale;
    double col_scale = (double)IMAGE_SIZE / chromaticityChartVertScale;

    pointList svgList;

	// scan over the curve and plot the lines
    for (double temp = start_temp; temp <= end_temp; temp += temp_step ) {
        XYColor current = approx_planck( temp );
        XYColor next = approx_planck( temp+temp_step );
        double fx = current.x * col_scale;
        double fy = IMAGE_SIZE - (current.y * row_scale);
        int thisX = floor(fx);
        int thisY = floor(fy);
        svgList.push_back( point2D(fx,fy) );
        int nextX = next.x * col_scale;
        int nextY = IMAGE_SIZE - (next.y * row_scale);
        DrawLine( &myFrameBuffer, thisX, thisY, nextX, nextY, 0xFFFFFFFF );
    }
    
    SVGStartGroup( svgObjects, "Planckian/WhitePoint Locus" );
    AddSVGPolyline( svgObjects, svgList, false, false );
    SVGEndGroup( svgObjects );

    if (writeImage)
        image.WriteTIFF( "chromaticity_planck_locus.tif", res, TIFF_MODE_GRAY_BLACKZERO );
}

/******************************************************************************/

void plotOneIlluminant( imageBuffer &image, IlluminantData &illum )
{
    const long image_size = image.height;

    double row_scale = (double)image_size / (chromaticityChartHorizScale);
    double col_scale = (double)image_size / (chromaticityChartVertScale);

    pointList svgList;
    SVGStartGroup( svgObjects, illum.name );

    double fx = illum.value.x * col_scale;
    double fy = image_size - illum.value.y * row_scale;
    int startX = floor(fx);
    int startY = floor(fy);
    point2D labelPt( fx, fy );
    
    // 3x3 rect
    image.FillRect( startX-1, startY-1, startX+1, startY+1, 1.0 );
    
    AddSVGCircle( svgObjects, labelPt, 2.0, true );
    string labelText = illum.name;
    string font = "Arial";
    string style = "Bold";
    string align = "Left";
    double fontSize = 24.0 * image_size / 800.0;
    AddSVGText( svgObjects, labelPt + point2D(2,-2), labelText, fontSize, font, style, align );
    SVGEndGroup( svgObjects );
}

/******************************************************************************/

void plotAllIlluminants( size_t IMAGE_SIZE, double res, int depth, bool writeImage )
{
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 1 );
    
    SVGStartGroup( svgObjects, "Illuminants" );
	for ( size_t i = 0; i < IlluminantList.size(); ++i ) {
        const size_t filename_limit = 512;
        char filename[filename_limit];

        filename[0] = 0;
        snprintf(filename, filename_limit, "illuminant %s.tif", IlluminantList[i].name );

        // zero the bitmap
        memset( image.buffer, 0, IMAGE_SIZE * IMAGE_SIZE );

        // plot the illuminant
        plotOneIlluminant( image, IlluminantList[i] );

        if (writeImage)
            image.WriteTIFF( filename, res, TIFF_MODE_GRAY_BLACKZERO );
	}
    SVGEndGroup( svgObjects );
}

/******************************************************************************/
/******************************************************************************/

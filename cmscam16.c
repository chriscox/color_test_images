//
// MIT License
//---------------------------------------------------------------------------------
//
//  Little Color Management System
//  Copyright (c) 1998-2023 Marti Maria Saguer
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software
// is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//---------------------------------------------------------------------------------
//

/******************************************************************************/
// original at https://github.com/mm2/Little-CMS/blob/master/src/cmscam02.c

// changes by ccox to get this compiling stand alone

//#include "lcms2_internal.h"

#include "math.h"
#include "memory.h"
#include "cmscam16.h"

extern void * _cmsMallocZero(cmsContext ContextID, size_t size );
extern void _cmsFree(cmsContext ContextID, void *ptr );

/******************************************************************************/

// CIECAM 16 appearance model. Hacked from the CIECAM02 code by Chris Cox

// ---------- Implementation --------------------------------------------

typedef struct  {

    cmsFloat64Number XYZ[3];
    cmsFloat64Number RGB[3];
    cmsFloat64Number RGBc[3];
    cmsFloat64Number RGBp[3];
    cmsFloat64Number RGBpa[3];
    cmsFloat64Number a, b, h, e, H, A, J, Q, s, t, C, M;
    cmsFloat64Number abC[2];
    cmsFloat64Number abs[2];
    cmsFloat64Number abM[2];

} CAM16COLOR;

typedef struct  {

    CAM16COLOR adoptedWhite;
    cmsFloat64Number LA, Yb;
    cmsFloat64Number F, c, Nc;
    cmsUInt32Number surround;
    cmsFloat64Number n, Nbb, Ncb, z, FL, D;

    cmsContext ContextID;

} cmsCIECAM16;


static
cmsFloat64Number compute_n(cmsCIECAM16* pMod)
{
// CIECAM16 eqn 5.8
    return (pMod -> Yb / pMod -> adoptedWhite.XYZ[1]);
}

static
cmsFloat64Number compute_z(cmsCIECAM16* pMod)
{
// CIECAM16 eqn 5.10
    return (1.48 + sqrt(pMod -> n));  // sqrt(n) == pow(n,0.5)
}

static
cmsFloat64Number computeNbb(cmsCIECAM16* pMod)
{
// CIECAM16 eqn 5.7
    return (0.725 * pow((1.0 / pMod -> n), 0.2));
}

static
cmsFloat64Number computeFL(cmsCIECAM16* pMod)
{
    cmsFloat64Number k, FL;

// CIECAM16 eqn 3.7, 5.6
    k = 1.0 / ((5.0 * pMod->LA) + 1.0);
    
// CIECAM16 eqn 3.6, 5.5
    FL = 0.2 * pow(k, 4.0) * (5.0 * pMod->LA) + 0.1 *
        (pow((1.0 - pow(k, 4.0)), 2.0)) *
        (pow((5.0 * pMod->LA), (1.0 / 3.0)));

    return FL;
}

static
cmsFloat64Number computeD(cmsCIECAM16* pMod)
{
    cmsFloat64Number D, temp;

// CIECAM16 eqn 4.3, 5.3  same as CIECAM02
    temp = 1.0 - ((1.0/3.6) * exp((-pMod ->LA-42) / 92.0));
    D = pMod->F * temp;
    return D;
}


static
CAM16COLOR XYZtoCAT16(CAM16COLOR clr)
{
// CIECAM16 eqn 3.1, 5.2
    clr.RGB[0] = (clr.XYZ[0] *  0.401288) + (clr.XYZ[1] *  0.650173) + (clr.XYZ[2] * -0.051461);
    clr.RGB[1] = (clr.XYZ[0] * -0.250268) + (clr.XYZ[1] *  1.204414) + (clr.XYZ[2] *  0.045854);
    clr.RGB[2] = (clr.XYZ[0] * -0.002079) + (clr.XYZ[1] *  0.048952) + (clr.XYZ[2] *  0.953127);

    return clr;
}

static
CAM16COLOR ChromaticAdaptation(CAM16COLOR clr, cmsCIECAM16* pMod)
{
    cmsUInt32Number i;

// CIECAM16 eqn 5.11, mixed with 5.4
    for (i = 0; i < 3; i++) {
        clr.RGBp[i] = ((pMod -> adoptedWhite.XYZ[1] *
            (pMod->D / pMod -> adoptedWhite.RGB[i])) +
            (1.0 - pMod->D)) * clr.RGB[i];
    }

    return clr;
}

static
cmsFloat64Number NonLinearCIECAM16_F_Deriv( cmsFloat64Number q, cmsFloat64Number FL )
{
    cmsFloat64Number temp, result, num, denom;

// CIECAM16 eqn 3.5, 5.21
    temp = (FL * q / 100.0);
    num = 1.68 * 27.13 * FL * pow(temp, -0.58);
    denom = 27.13 + pow(temp, 0.42);
    result = num / (denom*denom);       // num / pow(denom,2.0);
    return result;
}

static
cmsFloat64Number NonLinearCIECAM16_F( cmsFloat64Number q, cmsFloat64Number FL )
{
    cmsFloat64Number temp, result;

// CIECAM16 eqn 3.4, 5.18, 5.19, 5.20
    temp = pow((FL * q / 100.0), 0.42);
    result = (400.0 * temp) / (temp + 27.13);
    return result;
}

static
cmsFloat64Number NonLinearCIECAM16( cmsFloat64Number q, cmsFloat64Number FL )
{
    cmsFloat64Number temp, result, temp2;
    cmsFloat64Number qL = 0.26;     // defined in CIECAM16 end of section 3
    cmsFloat64Number qU = 150.0;    // TODO - should max with RGBWc
    
// CIECAM16 eqn 3.3, 5.17
    if (q > qU) {
        temp = NonLinearCIECAM16_F(qU,FL);
        temp2 = NonLinearCIECAM16_F_Deriv(qU,FL);
        result = temp + temp2 * (q - qU);
    }
    else if (q < qL) {
        result = NonLinearCIECAM16_F(qL,FL) * q / qL;
    }
    else {
        result = NonLinearCIECAM16_F(q,FL);
    }

// CIECAM16 eqn 3.2, 5.16
    return result + 0.1;
}

static
CAM16COLOR NonlinearCompression(CAM16COLOR clr, cmsCIECAM16* pMod)
{
    cmsUInt32Number i;

    for (i = 0; i < 3; i++) {
        if (clr.RGBp[i] < 0.0)
            clr.RGBpa[i] = -NonLinearCIECAM16( -clr.RGBp[i], pMod->FL );
        else
            clr.RGBpa[i] = NonLinearCIECAM16( clr.RGBp[i], pMod->FL );
    }

// CIECAM16 eqn 5.13, 5.27
    clr.A = ((2.0 * clr.RGBpa[0]) + clr.RGBpa[1] +
             (clr.RGBpa[2] / 20.0) - 0.305) * pMod->Nbb;

    return clr;
}

static
CAM16COLOR ComputeCorrelates(CAM16COLOR clr, cmsCIECAM16* pMod)
{
    cmsFloat64Number a, b, temp, e, t, r2d, d2r;

// CIECAM16 eqn 5.22
    a = clr.RGBpa[0] - (12.0 * clr.RGBpa[1] / 11.0) + (clr.RGBpa[2] / 11.0);
    
// CIECAM16 eqn 5.23
    b = (clr.RGBpa[0] + clr.RGBpa[1] - (2.0 * clr.RGBpa[2])) / 9.0;

// CIECAM16 eqn 5.24, with added safety
    r2d = (180.0 / M_PI);
    if (a == 0) {
        if (b == 0)     clr.h = 0;
        else if (b > 0) clr.h = 90;
        else            clr.h = 270;
    }
    else if (a > 0) {
        temp = b / a;
        if (b > 0)       clr.h = (r2d * atan(temp));
        else if (b == 0) clr.h = 0;
        else             clr.h = (r2d * atan(temp)) + 360;
    }
    else {
        temp = b / a;
        clr.h = (r2d * atan(temp)) + 180;
    }

// CIECAM16 eqn 5.25
    d2r = (M_PI / 180.0);
    e = (1.0/4.0) * (cos((clr.h * d2r + 2.0)) + 3.8);

    if (clr.h < 20.14) {
        temp = ((clr.h + 122.47)/1.2) + ((20.14 - clr.h)/0.8);
        clr.H = 300 + (100*((clr.h + 122.47)/1.2)) / temp;
    }
    else if (clr.h < 90.0) {
        temp = ((clr.h - 20.14)/0.8) + ((90.00 - clr.h)/0.7);
        clr.H = (100*((clr.h - 20.14)/0.8)) / temp;
    }
    else if (clr.h < 164.25) {
        temp = ((clr.h - 90.00)/0.7) + ((164.25 - clr.h)/1.0);
        clr.H = 100 + ((100*((clr.h - 90.00)/0.7)) / temp);
    }
    else if (clr.h < 237.53) {
        temp = ((clr.h - 164.25)/1.0) + ((237.53 - clr.h)/1.2);
        clr.H = 200 + ((100*((clr.h - 164.25)/1.0)) / temp);
    }
    else {
        temp = ((clr.h - 237.53)/1.2) + ((360 - clr.h + 20.14)/0.8);
        clr.H = 300 + ((100*((clr.h - 237.53)/1.2)) / temp);
    }

// CIECAM16 eqn 5.28
    clr.J = 100.0 * pow((clr.A / pMod->adoptedWhite.A),
        (pMod->c * pMod->z));

// CIECAM16 eqn 5.29
    clr.Q = (4.0 / pMod->c) * pow((clr.J / 100.0), 0.5) *
        (pMod->adoptedWhite.A + 4.0) * pow(pMod->FL, 0.25);

// CIECAM16 eqn 5.30
// pow(((a * a) + (b * b)), 0.5) = hypot(a,b), just faster
    t = (50000.0/13.0) * pMod->Nc * pMod->Ncb *
        (e * hypot(a,b)) /
        (clr.RGBpa[0] + clr.RGBpa[1] +
         ((21.0 / 20.0) * clr.RGBpa[2]));

// CIECAM16 eqn 5.31
    clr.C = pow(t, 0.9) * pow((clr.J / 100.0), 0.5) *
        pow((1.64 - pow(0.29, pMod->n)), 0.73);

// CIECAM16 eqn 5.32
    clr.M = clr.C * pow(pMod->FL, 0.25);

// CIECAM16 eqn 5.33
    clr.s = 100.0 * pow((clr.M / clr.Q), 0.5);

    return clr;
}


static
CAM16COLOR InverseCorrelates(CAM16COLOR clr, cmsCIECAM16* pMod)
{
    cmsFloat64Number t, e, p1, p2, p3, p4, p5, hr, d2r;
    cmsFloat64Number sinhr, coshr;
    
    d2r = M_PI / 180.0;

// CIECAM16 eqn 6.8
    t = pow( (clr.C / (pow((clr.J / 100.0), 0.5) *
        (pow((1.64 - pow(0.29, pMod->n)), 0.73)))),
        (1.0 / 0.9) );

// CIECAM eqn 6.6
    e = (1.0/4.0) * (cos((clr.h * d2r + 2.0)) + 3.8);

// CIECAM16 eqn 6.7
    clr.A = pMod->adoptedWhite.A * pow(
           (clr.J / 100.0),
           (1.0 / (pMod->c * pMod->z)));

// CIECAM16 eqn 6.9
    assert( t >= 0.0 );
    p1 = (50000/13.0) * pMod->Nc * pMod->Ncb * e / t;
    p2 = (clr.A / pMod->Nbb) + 0.305;
    p3 = 21.0 / 20.0;


// CIECAM16 Inverse Step 3
    hr = clr.h * d2r;
    sinhr = sin(hr);
    coshr = cos(hr);

    if (fabs(sinhr) >= fabs(coshr)) {
// CIECAM16 eqn 6.10, 6.11
        p4 = p1 / sinhr;
        clr.b = (p2 * (2.0 + p3) * (460.0 / 1403.0)) /
            (p4 + (2.0 + p3) * (220.0 / 1403.0) *
            (coshr / sinhr) - (27.0 / 1403.0) +
            p3 * (6300.0 / 1403.0));
        clr.a = clr.b * (coshr / sinhr);
    }
    else {
// CIECAM16 eqn 6.12, 6.13
        p5 = p1 / coshr;
        clr.a = (p2 * (2.0 + p3) * (460.0 / 1403.0)) /
            (p5 + (2.0 + p3) * (220.0 / 1403.0) -
            ((27.0 / 1403.0) - p3 * (6300.0 / 1403.0)) *
            (sinhr / coshr));
        clr.b = clr.a * (sinhr / coshr);
    }

// CIECAM16 eqn 6.14
    clr.RGBpa[0] = ((460.0 / 1403.0) * p2) +
              ((451.0 / 1403.0) * clr.a) +
              ((288.0 / 1403.0) * clr.b);
    clr.RGBpa[1] = ((460.0 / 1403.0) * p2) -
              ((891.0 / 1403.0) * clr.a) -
              ((261.0 / 1403.0) * clr.b);
    clr.RGBpa[2] = ((460.0 / 1403.0) * p2) -
              ((220.0 / 1403.0) * clr.a) -
              ((6300.0 / 1403.0) * clr.b);

    return clr;
}


static
cmsFloat64Number NonLinearInverseCIECAM16( cmsFloat64Number r, cmsFloat64Number FL )
{
    cmsFloat64Number temp, result;
    cmsFloat64Number qL = 0.26;     // defined in CIECAM16 end of section 3
    cmsFloat64Number qU = 150.0;    // TODO - should max with RGBWc
    cmsFloat64Number FqL = NonLinearCIECAM16_F(qL,FL);
    cmsFloat64Number FqU = NonLinearCIECAM16_F(qU,FL);
    cmsFloat64Number FqUprime = NonLinearCIECAM16_F_Deriv(qU,FL);

// CIECAM16 eqn 6.15
    if (r > FqU) {
        result = qU + ((r - FqU) / FqUprime);
    }
    else if (r < FqL) {
        result = qL * r / FqL;
    }
    else {
        temp = 27.13 * r / (400.0-r);
        result = 100.0 / FL * pow( temp, 1.0/0.42 );
    }

    return result;
}

static
CAM16COLOR InverseNonlinearity(CAM16COLOR clr, cmsCIECAM16* pMod)
{
    cmsUInt32Number i;

    for (i = 0; i < 3; i++) {
        cmsFloat64Number r = clr.RGBpa[i] - 0.1;
        if (r < 0.0)
            clr.RGBc[i] = -NonLinearInverseCIECAM16( -r, pMod->FL );
        else
            clr.RGBc[i] = NonLinearInverseCIECAM16( r, pMod->FL );
    }

    return clr;
}

static
CAM16COLOR InverseChromaticAdaptation(CAM16COLOR clr,  cmsCIECAM16* pMod)
{
    cmsUInt32Number i;

// CIECAM16 eqn 6.16, mixed with eqn 5.4
    for (i = 0; i < 3; i++) {
        clr.RGB[i] = clr.RGBc[i] /
            ((pMod->adoptedWhite.XYZ[1] * pMod->D / pMod->adoptedWhite.RGB[i]) + 1.0 - pMod->D);
    }
    return clr;
}


static
CAM16COLOR CAT16toXYZ(CAM16COLOR clr)
{
/*
CIECAM16 eqn 6.18
inverse CAT16
        [ 1.86206786, -1.01125463,  0.14918677],
        [ 0.38752654,  0.62144744, -0.00897398],
        [-0.01584150, -0.03412294,  1.04996444],
*/
    clr.XYZ[0] = (clr.RGB[0] *  1.86206786) + (clr.RGB[1] * -1.01125463) + (clr.RGB[2] *  0.14918677);
    clr.XYZ[1] = (clr.RGB[0] *  0.38752654) + (clr.RGB[1] *  0.62144744) + (clr.RGB[2] * -0.00897398);
    clr.XYZ[2] = (clr.RGB[0] * -0.01584150) + (clr.RGB[1] * -0.03412294) + (clr.RGB[2] *  1.04996444);

    return clr;
}


cmsHANDLE  CMSEXPORT cmsCIECAM16Init(cmsContext ContextID, const cmsViewingConditions* pVC)
{
    cmsCIECAM16* lpMod;

    _cmsAssert(pVC != NULL);

    if((lpMod = (cmsCIECAM16*) _cmsMallocZero(ContextID, sizeof(cmsCIECAM16))) == NULL) {
        return NULL;
    }

    lpMod ->ContextID = ContextID;

    lpMod ->adoptedWhite.XYZ[0] = pVC ->whitePoint.X;
    lpMod ->adoptedWhite.XYZ[1] = pVC ->whitePoint.Y;
    lpMod ->adoptedWhite.XYZ[2] = pVC ->whitePoint.Z;

    lpMod -> LA       = pVC ->La;
    lpMod -> Yb       = pVC ->Yb;
    lpMod -> D        = pVC ->D_value;
    lpMod -> surround = pVC ->surround;

    switch (lpMod -> surround) {


    case CUTSHEET_SURROUND:
        lpMod->F = 0.8;
        lpMod->c = 0.41;
        lpMod->Nc = 0.8;
        break;

    case DARK_SURROUND:
        lpMod -> F  = 0.8;
        lpMod -> c  = 0.525;
        lpMod -> Nc = 0.8;
        break;

    case DIM_SURROUND:
        lpMod -> F  = 0.9;
        lpMod -> c  = 0.59;
        lpMod -> Nc = 0.95;
        break;

    default:
        // Average surround
        lpMod -> F  = 1.0;
        lpMod -> c  = 0.69;
        lpMod -> Nc = 1.0;
    }

    lpMod -> n   = compute_n(lpMod);
    lpMod -> z   = compute_z(lpMod);
    lpMod -> Nbb = computeNbb(lpMod);
    lpMod -> FL  = computeFL(lpMod);

    if (lpMod -> D == D_CALCULATE) {
        lpMod -> D   = computeD(lpMod);
    }

    lpMod -> Ncb = lpMod -> Nbb;

    lpMod -> adoptedWhite = XYZtoCAT16(lpMod -> adoptedWhite);
    lpMod -> adoptedWhite = ChromaticAdaptation(lpMod -> adoptedWhite, lpMod);
    lpMod -> adoptedWhite = NonlinearCompression(lpMod -> adoptedWhite, lpMod);

    return (cmsHANDLE) lpMod;

}

void CMSEXPORT cmsCIECAM16Done(cmsHANDLE hModel)
{
    cmsCIECAM16* lpMod = (cmsCIECAM16*) hModel;

    if (lpMod) _cmsFree(lpMod ->ContextID, lpMod);
}


void CMSEXPORT cmsCIECAM16Forward(cmsHANDLE hModel, const cmsCIEXYZ* pIn, cmsJCh* pOut)
{
    CAM16COLOR clr;
    cmsCIECAM16* lpMod = (cmsCIECAM16*) hModel;
  
    _cmsAssert(lpMod != NULL);
    _cmsAssert(pIn != NULL);
    _cmsAssert(pOut != NULL);

    memset(&clr, 0, sizeof(clr));

    clr.XYZ[0] = pIn ->X;
    clr.XYZ[1] = pIn ->Y;
    clr.XYZ[2] = pIn ->Z;

    clr = XYZtoCAT16(clr);
    clr = ChromaticAdaptation(clr, lpMod);
    clr = NonlinearCompression(clr, lpMod);
    clr = ComputeCorrelates(clr, lpMod);

    pOut ->J = clr.J;
    pOut ->C = clr.C;
    pOut ->h = clr.h;
}

void CMSEXPORT cmsCIECAM16Reverse(cmsHANDLE hModel, const cmsJCh* pIn, cmsCIEXYZ* pOut)
{
    CAM16COLOR clr;
    cmsCIECAM16* lpMod = (cmsCIECAM16*) hModel;
    
    _cmsAssert(lpMod != NULL);
    _cmsAssert(pIn != NULL);
    _cmsAssert(pOut != NULL);

    memset(&clr, 0, sizeof(clr));

    clr.J = pIn -> J;
    clr.C = pIn -> C;
    clr.h = pIn -> h;
    
    // make sure the hue value is in our valid range
    clr.h = fmod( clr.h, 360.0 );
    if (clr.h < 0.0)
        clr.h += 360.0;

    clr = InverseCorrelates(clr, lpMod);
    clr = InverseNonlinearity(clr, lpMod);
    clr = InverseChromaticAdaptation(clr, lpMod);
    clr = CAT16toXYZ(clr);

    pOut ->X = clr.XYZ[0];
    pOut ->Y = clr.XYZ[1];
    pOut ->Z = clr.XYZ[2];
}

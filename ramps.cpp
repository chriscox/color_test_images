//
//  ramps.cpp
//  Color Test Images
//
//  Created by Chris Cox on December 27, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

/*
What to add next?
CIECAM models aren't good enough for slices/boxes/ramps yet.
32 bit/channel float?       No CMYK or LAB support.
OKLab?  Seems primitive, but good for gradients.
    https://bottosson.github.io/posts/oklab/
    https://bottosson.github.io/posts/colorpicker/


https://people.csail.mit.edu/jaffer/Color/Dictionaries#L*a*b*%20Space
https://people.csail.mit.edu/jaffer/Color/Dictionaries#nbs-anthus

*/

#include <cstdio>
#include <cmath>
#include <iostream>
#include <cassert>
#include <cstring>
#include <vector>
#include <algorithm>

#include "ramps.hpp"
#include "image.hpp"
#include "lines.h"
#include "ciecam97.h"
#include "cmscam02.h"
#include "cmscam16.h"
#include "SVG.hpp"


/******************************************************************************/

XYZColor whitePointE = { 100.0, 100.0, 100.0 };
XYZColor whitePointD50 = { 96.4212, 100.0, 82.5188 };
XYZColor whitePointD65 = { 94.811, 100.0, 107.304 };

/******************************************************************************/

inline
double Pin255( double value )
{
    value = std::max( 0.0, value );
    value = std::min( value, 255.0 );
    return value;
}

/******************************************************************************/

inline double
lerp ( double a, double b, double fraction )
{
    return a + (b-a) * fraction;
}

/******************************************************************************/

inline double CIELABFUNC (double x)
	{
	const double lab_cutover = pow( 6.0/29.0, 3.0 ); // 0.0088564516790356311
	const double lab_cut2 = pow( 6.0/29.0, 2.0 );    // 0.042806183115338882
	const double lab_add = 4.0/29.0;                 // 0.13793103448275862
	double y = 0.0;
	
	if (x > lab_cutover)
		y = cbrt (x);
	else
		y = x/(3.0*lab_cut2) + lab_add;
    
    return y;
	}

/******************************************************************************/

// XYZ values 0..100 for XYZ, though sometimes we get some slight negatives
// LAB values 0..255 -- need to fix that, but wanted to match Photoshop encoding
// Always uses D50 white point, same as ICC standard
void ConvertXYZtoLAB (const XYZColor &xyz, LABColor &lab)
{
    const double xD50 = whitePointD50.x;
    const double yD50 = whitePointD50.y;
    const double zD50 = whitePointD50.z;
    
    // negative luminance should not exist
    double yy = std::max( xyz.y, 0.0 );
    double xx = xyz.x;
    double zz = xyz.z;

#if 0
    // negative chromaticity should not exist, but can cause clipping errors
    xx = std::max( xx, 0.0 );
    zz = std::max( zz, 0.0 );
#endif

    const double fx = CIELABFUNC( xx / xD50 );
    const double fy = CIELABFUNC( yy / yD50 );
    const double fz = CIELABFUNC( zz / zD50 );
    
    lab.L = (255.0 / 100.0) * (116.0 * fy - 16.0);
    lab.a = 128.0 + 500.0 * (fx - fy);
    lab.b = 128.0 + 200.0 * (fy - fz);
    
    lab.L = Pin255( lab.L );
    lab.a = Pin255( lab.a );
    lab.b = Pin255( lab.b );
}

/******************************************************************************/

double hueToRgb( double p, double q, double t ) {
  if (t < 0) t += 1.0;
  if (t > 1) t -= 1.0;
  if (t < 1/6.0) return p + (q - p) * 6.0 * t;
  if (t < 1/2.0) return q;
  if (t < 2/3.0) return p + (q - p) * (2.0/3.0 - t) * 6.0;
  return p;
}

// values 0..1
void ConvertHSLtoRGB( const HSLColor &HSL, RGBColor &RGB ) {
    double hh = HSL.h;
    double ss = HSL.s;
    double ll = HSL.l;

    if (hh > 1.0)   hh -= 1.0;
    if (hh < 0.0)   hh += 1.0;
    ss = std::min( std::max( ss, 0.0 ), 1.0 );
    ll = std::min( std::max( ll, 0.0 ), 1.0 );

    if (HSL.s == 0) {
        RGB.r = RGB.g = RGB.b = ll;      // gray
    } else {
        double q = (ll < 0.5) ? (ll * (1 + ss)) : (ll + ss - ll * ss);
        double p = 2 * ll - q;
        RGB.r = hueToRgb(p, q, hh + 1/3.0);
        RGB.g = hueToRgb(p, q, hh);
        RGB.b = hueToRgb(p, q, hh - 1/3.0);
    }
}

/******************************************************************************/

// values 0..1
// convert to HSL, then to RGB
void ConvertHSVtoRGB( const HSVColor &HSV, RGBColor &RGB ) {
    HSLColor HSL;
    HSL.h = HSV.h;
    HSL.l = HSV.v * (1.0-HSV.s/2.0);
    
    double sat = 0.0;
    if ( (HSL.l > 0.0) && (HSL.l < 1.0) ) {
        sat = (HSV.v - HSL.l) / std::min( HSL.l, 1.0 - HSL.l);
    }
    HSL.s = sat;

    ConvertHSLtoRGB( HSL, RGB );
}

/******************************************************************************/

// grayscale patches, and various slices of LAB space.
// This is similar to another test file I found, but redone for higher precision and scaleability.
void makeLABBoxes(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const int PAGE_HEIGHT = (int)(8.0 * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*8.0);
		double tileY = yfraction*8.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            LABColor LAB;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					LAB.L = page * pageScale;
					LAB.a = 128 / 255.0;
					LAB.b = 128 / 255.0;
					break;
				
				case 1:
					LAB.L = (page+horizPages) * pageScale;
					LAB.a = 128 / 255.0;
					LAB.b = 128 / 255.0;
					break;

				case 2:
					LAB.L = page * pageScale;
					LAB.a = tileX;
					LAB.b = (1.0 - tileY);
					break;
				
				case 3:
					LAB.L = (page+horizPages) * pageScale;
					LAB.a = tileX;
					LAB.b = (1.0 - tileY);
					break;

				case 4:
					LAB.L = (1.0 - tileY);
					LAB.a = tileX;
					LAB.b = page * pageScale;
					break;
				
				case 5:
					LAB.L = (1.0 - tileY);
					LAB.a = tileX;
					LAB.b = (page+horizPages) * pageScale;
					break;
				
				case 6:
					LAB.L = (1.0 - tileY);
					LAB.a = page * pageScale;
					LAB.b = tileX;
					break;
				
                default:
				case 7:
					LAB.L = (1.0 - tileY);
					LAB.a = (page+horizPages) * pageScale;
					LAB.b = tileX;
					break;
				
				}
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LABBoxes.tif", res, TIFF_MODE_CIELAB );

}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
// Plus ab slices down the L axis at the bottom of the image.
void makeLABRamp1(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            LABColor LAB;
			
			switch( (int)tile )
				{
				case 0:
					LAB.L = xfraction;
					LAB.a = (128 + tileY * 100.0)/255.0;
					LAB.b = 128 / 255.0;
					break;
				
				case 1:
					LAB.L = xfraction;
					LAB.a = (128 + tileY * 100.0)/255.0;
					LAB.b = (128 + tileY * 100.0)/255.0;
					break;
				
				case 2:
					LAB.L = xfraction;
					LAB.a = 128 / 255.0;
					LAB.b = (128 + tileY * 100.0)/255.0;
					break;
				
				case 3:
					LAB.L = xfraction;
					LAB.a = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					LAB.b = (128 + tileY * 100.0)/255.0;
					break;
				
				case 4:
					LAB.L = xfraction;
					LAB.a = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					LAB.b = 128 / 255.0;
					break;
				
				case 5:
					LAB.L = xfraction;
					LAB.a = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					LAB.b = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					break;
				
				case 6:
					LAB.L = xfraction;
					LAB.a = 128 / 255.0;
					LAB.b = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					break;
				
				case 7:
					LAB.L = xfraction;
					LAB.a = (128 + tileY * 100.0)/255.0;
					LAB.b = (22.0 + (1.0 - tileY) * 107.0)/255.0;
					break;
				
				case 8:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					LAB.L = 5.0/255.0 + (240.0/(9.0*255.0))*(2.0*page);
					LAB.a = (22.0 + tileX * 207.0)/255.0;
					LAB.b = (22.0 + (1.0 - tileY) * 207.0)/255.0;
					}
					break;
				
                default:
				case 9:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					LAB.L = 5.0/255.0 + (240.0/(9.0*255.0))*(1.0 + 2.0*page);
					LAB.a = (22.0 + tileX * 207.0)/255.0;
					LAB.b = (22.0 + (1.0 - tileY) * 207.0)/255.0;
					}
					break;
				
				}
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );

		}
	}
    
    image.WriteTIFF( "LABRamp1.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

// Different hue slices near neutral -- where many profiles show errors.
void makeLABRamp2(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            LABColor LAB;
			
			switch( (int)tile )
				{
				case 0:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = 128 / 255.0;
					break;
				
				case 1:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = (108 + tileY * 40.0)/255.0;
					break;
				
				case 2:
					LAB.L = xfraction;
					LAB.a = 128 / 255.0;
					LAB.b = (108 + tileY * 40.0)/255.0;
					break;
				
				case 3:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = 120 / 255.0;
					break;
				
				case 4:
					LAB.L = xfraction;
					LAB.a = 120 / 255.0;
					LAB.b = (108 + tileY * 40.0)/255.0;
					break;
				
				case 5:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = 136 / 255.0;
					break;
				
				case 6:
					LAB.L = xfraction;
					LAB.a = 136 / 255.0;
					LAB.b = (108 + tileY * 40.0)/255.0;
					break;
				
				case 7:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = 108 / 255.0;
					break;
				
				case 8:
					LAB.L = xfraction;
					LAB.a = 108 / 255.0;
					LAB.b = (108 + tileY * 40.0)/255.0;
					break;
				
                default:
				case 9:
					LAB.L = xfraction;
					LAB.a = (108 + tileY * 40.0)/255.0;
					LAB.b = 148 / 255.0;
					break;
				
				}
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LABRamp2.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void makeLABRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );

    makeLABBoxes(res,depth);
    makeLABRamp1(res,depth);
    makeLABRamp2(res,depth);
}

/******************************************************************************/

// grayscale patches, and various slices of RGB space.
void makeRGBBoxes(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const int PAGE_HEIGHT = (int)(8.0 * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*8.0);
		double tileY = yfraction*8.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            RGBColor RGB;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					RGB.r = page * pageScale;
					RGB.g = page * pageScale;
					RGB.b = page * pageScale;
					break;
				
				case 1:
					RGB.r = (page+horizPages) * pageScale;
					RGB.g = (page+horizPages) * pageScale;
					RGB.b = (page+horizPages) * pageScale;
					break;

				case 2:
					RGB.r = page * pageScale;
					RGB.g = tileX;
					RGB.b = (1.0 - tileY);
					break;
				
				case 3:
					RGB.r = (page+horizPages) * pageScale;
					RGB.g = tileX;
					RGB.b = (1.0 - tileY);
					break;

				case 4:
					RGB.r = (1.0 - tileY);
					RGB.g = tileX;
					RGB.b = page * pageScale;
					break;
				
				case 5:
					RGB.r = (1.0 - tileY);
					RGB.g = tileX;
					RGB.b = (page+horizPages) * pageScale;
					break;
				
				case 6:
					RGB.r = (1.0 - tileY);
					RGB.g = page * pageScale;
					RGB.b = tileX;
					break;
				
                default:
				case 7:
					RGB.r = (1.0 - tileY);
					RGB.g = (page+horizPages) * pageScale;
					RGB.b = tileX;
					break;
				
				}
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBBoxes.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
void makeRGBRamp1(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            RGBColor RGB;
			
			switch( (int)tile )
				{
				case 0:
					RGB.r = xfraction;                      // red
					RGB.g = xfraction * (1.0-tileY);
					RGB.b = xfraction * (1.0-tileY);
					break;
				
				case 1:
					RGB.r = xfraction;                      // orange
					RGB.g = xfraction * ((255.0-140.0)/255.0 + (140.0/255.0) * (1.0-tileY));
					RGB.b = xfraction * (1.0-tileY);
					break;
				
				case 2:
					RGB.r = xfraction;                      // yellow
					RGB.g = xfraction;
					RGB.b = xfraction * (1.0-tileY);
					break;
				
				case 3:
					RGB.r = xfraction * (1.0-tileY);        // green
					RGB.g = xfraction;
					RGB.b = xfraction * (1.0-tileY);
					break;
				
				case 4:
					RGB.r = xfraction * (1.0-tileY);        // cyan
					RGB.g = xfraction;
					RGB.b = xfraction;
					break;
				
				case 5:
					RGB.r = xfraction * (1.0-tileY);        // blue
					RGB.g = xfraction * (1.0-tileY);
					RGB.b = xfraction;
					break;
				
				case 6:
					RGB.r = xfraction * ((255.0-135.0)/255.0 + (135.0/255.0) * (1.0-tileY));
					RGB.g = xfraction * (1.0-tileY);        // violet
					RGB.b = xfraction;
					break;
				
				case 7:
					RGB.r = xfraction;                      // magenta
					RGB.g = xfraction * (1.0-tileY);
					RGB.b = xfraction;
					break;
				
				case 8:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					RGB.r = (2.0*page)/9.0;
					RGB.g = tileX;
					RGB.b = tileY;
					}
					break;
				
                default:
				case 9:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					RGB.r = (1.0 + 2.0*page)/9.0;
					RGB.g = tileX;
					RGB.b = tileY;
					}
					break;
				
				}
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );

		}
	}
    
    image.WriteTIFF( "RGBRamp1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

// Different slices around neutral -- where many profiles show errors.
void makeRGBRamp2(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
        double cy = tileY - 0.5;                // -0.5..0.5
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH; // 0..1
            double cx = xfraction - 0.5;        // -0.5..0.5
            const double range = 50;
            const double range2 = 90;
            const double rangeScale = std::min( 128.0 + 0.5*range, 255.0 );
            const double rangeScale2 = std::min( 128.0 + range2, 255.0 );
            const double rangeScale3 = std::min( 140.0 + 0.5*range, 255.0 );
            RGBColor RGB;

			switch( (int)tile )
				{
				case 0:
					RGB.r = xfraction * (128 + range*cy)/rangeScale;        // cyan-red
					RGB.g = xfraction * (128 - range*cy)/rangeScale;
					RGB.b = xfraction * (128 - range*cy)/rangeScale;
					break;
				
				case 1:
					RGB.r = xfraction * (128 + range*cy)/rangeScale;        // violet-orange
					RGB.g = xfraction * (140/255.0)*(128 + range*cy)/rangeScale;
					RGB.b = xfraction * (128 - range*cy)/rangeScale;
					break;
				
				case 2:
					RGB.r = xfraction * (128 + range*cy)/rangeScale;        // blue-yellow
					RGB.g = xfraction * (128 + range*cy)/rangeScale;
					RGB.b = xfraction * (128 - range*cy)/rangeScale;
					break;
				
				case 3:
					RGB.r = xfraction * (128 - range*cy)/rangeScale;         // magenta-green
					RGB.g = xfraction * (128 + range*cy)/rangeScale;
					RGB.b = xfraction * (128 - range*cy)/rangeScale;
					break;
				
				case 4:
					RGB.r = xfraction * (128 - range*cy)/rangeScale;          // orange-green
					RGB.g = xfraction * (128 + range*cy)/rangeScale;
					RGB.b = xfraction * (140/255.0)*(128 + range*cy)/rangeScale;
					break;
				
				case 5:
					RGB.r = xfraction * (140 - range*cy)/rangeScale3;         // rose-mint
					RGB.g = xfraction * (140 + range*cy)/rangeScale3;
					RGB.b = xfraction * (128 + range*cy)/rangeScale3;
					break;
				
				case 6:
					RGB.r = xfraction * (135/255.0)*(128 + range*cy)/rangeScale;
					RGB.g = xfraction * (128 - range*cy)/rangeScale;          // green-blue
					RGB.b = xfraction * (128 + range*cy)/rangeScale;
					break;
				
				case 7:
					RGB.r = xfraction * (200/255.0)*(128 + range*cy)/rangeScale;
					RGB.g = xfraction * (128 + range*cy)/rangeScale;         // sky-mint
					RGB.b = xfraction * (128 - (range/2)*cy)/rangeScale;
					break;
				
				case 8:
					RGB.r = (128 - range2*cy)/rangeScale2;
					RGB.g = 128 / 255.0;
					RGB.b = (128 - range2*cx)/rangeScale2;
					break;
				
                default:
				case 9:
					RGB.r = (128 + range2*cx)/rangeScale2;
					RGB.g = (128 - range2*cy)/rangeScale2;
					RGB.b = 128 / 255.0;
					break;
				
				}
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBRamp2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void makeRGBRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );

    makeRGBBoxes(res,depth);
    makeRGBRamp1(res,depth);
    makeRGBRamp2(res,depth);
}

/******************************************************************************/

// grayscale patches, and various slices of HSL space.
void makeHSLBoxes(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const int PAGE_HEIGHT = (int)(8.0 * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*8.0);
		double tileY = yfraction*8.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSLColor HSL;
            RGBColor RGB;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					HSL.h = 0;
					HSL.s = 0;
					HSL.l = page * pageScale;
					break;
				
				case 1:
					HSL.h = 0;
					HSL.s = 0;
					HSL.l = (page+horizPages) * pageScale;
					break;

				case 2:
					HSL.h = page * pageScale;
					HSL.s = tileX;
					HSL.l = (1.0 - tileY);
					break;
				
				case 3:
					HSL.h = (page+horizPages) * pageScale;
					HSL.s = tileX;
					HSL.l = (1.0 - tileY);
					break;

				case 4:
					HSL.h = tileX;
					HSL.s = (1.0 - tileY);
					HSL.l = page * pageScale;
					break;
				
				case 5:
					HSL.h = tileX;
					HSL.s = (1.0 - tileY);
					HSL.l = (page+horizPages) * pageScale;
					break;
				
				case 6:
					HSL.h = tileX;
					HSL.s = page * pageScale;
					HSL.l = (1.0 - tileY);
					break;
				
                default:
				case 7:
					HSL.h = tileX;
					HSL.s = (page+horizPages) * pageScale;
					HSL.l = (1.0 - tileY);
					break;
				
				}

            ConvertHSLtoRGB( HSL, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLBoxes.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
void makeHSLRamp1(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSLColor HSL;
            RGBColor RGB;
			
			switch( (int)tile )
				{
				case 0:
					HSL.h = 0/360.0;                        // red
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 1:
					HSL.h = 30/360.0;                       // orange
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 2:
					HSL.h = 60/360.0;                       // yellow
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 3:
					HSL.h = 120/360.0;                      // green
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 4:
					HSL.h = 180/360.0;                      // cyan
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 5:
					HSL.h = 240/360.0;                      // blue
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 6:
					HSL.h = 270/360.0;                      // violet
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 7:
					HSL.h = 300/360.0;                      // magenta
					HSL.s = tileY;
					HSL.l = xfraction;
					break;
				
				case 8:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					HSL.l = 0.05 + (2.0*page)/10.0;
					HSL.h = tileX;
					HSL.s = tileY;
					}
					break;
				
                default:
				case 9:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					HSL.l = 0.05 + (1.0 + 2.0*page)/10.0;
					HSL.h = tileX;
					HSL.s = tileY;
					}
					break;
				
				}

            ConvertHSLtoRGB( HSL, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );

		}
	}
    
    image.WriteTIFF( "HSLRamp1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

// Different slices around neutral -- where many profiles show errors.
void makeHSLRamp2(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
        double cy = tileY - 0.5;                // -0.5..0.5
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSLColor HSL;
            RGBColor RGB;
            
            HSL.h = (tile/20.0) + ((cy < 0) ? 0 : 0.5);
            HSL.s = 0.5*fabs(cy);
            HSL.l = xfraction;

            ConvertHSLtoRGB( HSL, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLRamp2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void makeHSLRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );

    makeHSLBoxes(res,depth);
    makeHSLRamp1(res,depth);
    makeHSLRamp2(res,depth);
}

/******************************************************************************/

// grayscale patches, and various slices of HSV space.
void makeHSVBoxes(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const int PAGE_HEIGHT = (int)(8.0 * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*8.0);
		double tileY = yfraction*8.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSVColor HSV;
            RGBColor RGB;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					HSV.h = 0;
					HSV.s = 0;
					HSV.v = page * pageScale;
					break;
				
				case 1:
					HSV.h = 0;
					HSV.s = 0;
					HSV.v = (page+horizPages) * pageScale;
					break;

				case 2:
					HSV.h = page * pageScale;
					HSV.s = tileX;
					HSV.v = (1.0 - tileY);
					break;
				
				case 3:
					HSV.h = (page+horizPages) * pageScale;
					HSV.s = tileX;
					HSV.v = (1.0 - tileY);
					break;

				case 4:
					HSV.h = tileX;
					HSV.s = (1.0 - tileY);
					HSV.v = page * pageScale;
					break;
				
				case 5:
					HSV.h = tileX;
					HSV.s = (1.0 - tileY);
					HSV.v = (page+horizPages) * pageScale;
					break;
				
				case 6:
					HSV.h = tileX;
					HSV.s = page * pageScale;
					HSV.v = (1.0 - tileY);
					break;
				
                default:
				case 7:
					HSV.h = tileX;
					HSV.s = (page+horizPages) * pageScale;
					HSV.v = (1.0 - tileY);
					break;
				
				}

            ConvertHSVtoRGB( HSV, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSVBoxes.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
void makeHSVRamp1(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSVColor HSV;
            RGBColor RGB;
			
			switch( (int)tile )
				{
				case 0:
					HSV.h = 0/360.0;                        // red
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 1:
					HSV.h = 30/360.0;                       // orange
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 2:
					HSV.h = 60/360.0;                       // yellow
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 3:
					HSV.h = 120/360.0;                      // green
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 4:
					HSV.h = 180/360.0;                      // cyan
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 5:
					HSV.h = 240/360.0;                      // blue
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 6:
					HSV.h = 270/360.0;                      // violet
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 7:
					HSV.h = 300/360.0;                      // magenta
					HSV.s = tileY;
					HSV.v = xfraction;
					break;
				
				case 8:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					HSV.v = 0.05 + (2.0*page)/10.0;
					HSV.h = tileX;
					HSV.s = tileY;
					}
					break;
				
                default:
				case 9:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					HSV.v = 0.05 + (1.0 + 2.0*page)/10.0;
					HSV.h = tileX;
					HSV.s = tileY;
					}
					break;
				
				}

            ConvertHSVtoRGB( HSV, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );

		}
	}
    
    image.WriteTIFF( "HSVRamp1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

// Different slices around neutral -- where many profiles show errors.
void makeHSVRamp2(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
        double cy = tileY - 0.5;                // -0.5..0.5
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            HSVColor HSV;
            RGBColor RGB;
            
            HSV.h = (tile/20.0) + ((cy < 0) ? 0 : 0.5);
            HSV.s = 0.5*fabs(cy);
            HSV.v = xfraction;

            ConvertHSVtoRGB( HSV, RGB );
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSVRamp2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void makeHSVRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );

    makeHSVBoxes(res,depth);
    makeHSVRamp1(res,depth);
    makeHSVRamp2(res,depth);
}

/******************************************************************************/

// clean grayscale patches, and various slices of CMYK space.
void makeCMYKBoxes1(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    
    const int vertPages = 8;
    const int PAGE_HEIGHT = (int)(vertPages * res);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*vertPages);
		double tileY = yfraction*vertPages- tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            CMYKColor CMYK;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					CMYK.c = 0;
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = page * pageScale;
					break;
				
				case 1:
					CMYK.c = 0;
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = (page+horizPages) * pageScale;
					break;

				case 2:
					CMYK.c = page * pageScale;
					CMYK.m = tileX;
					CMYK.y = (1.0 - tileY);
					CMYK.k = 0;
					break;
				
				case 3:
					CMYK.c = (page+horizPages) * pageScale;
					CMYK.m = tileX;
					CMYK.y = (1.0 - tileY);
					CMYK.k = 0;
					break;

				case 4:
					CMYK.c = (1.0 - tileY);
					CMYK.m = tileX;
					CMYK.y = page * pageScale;
					CMYK.k = 0;
					break;
				
				case 5:
					CMYK.c = (1.0 - tileY);
					CMYK.m = tileX;
					CMYK.y = (page+horizPages) * pageScale;
					CMYK.k = 0;
					break;
				
				case 6:
					CMYK.c = (1.0 - tileY);
					CMYK.m = page * pageScale;
					CMYK.y = tileX;
					CMYK.k = 0;
					break;
				
                default:
				case 7:
					CMYK.c = (1.0 - tileY);
					CMYK.m = (page+horizPages) * pageScale;
					CMYK.y = tileX;
					CMYK.k = 0;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKBoxes1.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// muddy grayscale patches, and various slices of CMYK space.
void makeCMYKBoxes2(double res, int depth)
{
    const int horizPages = 8;
    const int PAGE_WIDTH = (int)(horizPages * res);
    const double pageScale = 1.0 / double(horizPages*2-1);
    
    const int vertPages = 8;
    const int PAGE_HEIGHT = (int)(vertPages * res);
    // vertical page count is not as easily changed

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );
	
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*vertPages);
		double tileY = yfraction*vertPages- tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            CMYKColor CMYK;
			
			double page = floor(xfraction*horizPages);
			double tileX = xfraction*horizPages - page;
			
			switch( (int)tile )
				{
				case 0:
					CMYK.c = page * pageScale;
					CMYK.m = page * pageScale;
					CMYK.y = page * pageScale;
					CMYK.k = 0;
					break;
				
				case 1:
					CMYK.c = (page+horizPages) * pageScale;
					CMYK.m = (page+horizPages) * pageScale;
					CMYK.y = (page+horizPages) * pageScale;
					CMYK.k = 0;
					break;

				case 2:
					CMYK.c = 0;
					CMYK.m = tileX;
					CMYK.y = (1.0 - tileY);
					CMYK.k = page * pageScale;
					break;
				
				case 3:
					CMYK.c = 0;
					CMYK.m = tileX;
					CMYK.y = (1.0 - tileY);
					CMYK.k = (page+horizPages) * pageScale;
					break;

				case 4:
					CMYK.c = (1.0 - tileY);
					CMYK.m = tileX;
					CMYK.y = 0;
					CMYK.k = page * pageScale;
					break;
				
				case 5:
					CMYK.c = (1.0 - tileY);
					CMYK.m = tileX;
					CMYK.y = 0;
					CMYK.k = (page+horizPages) * pageScale;
					break;
				
				case 6:
					CMYK.c = (1.0 - tileY);
					CMYK.m = 0;
					CMYK.y = tileX;
					CMYK.k = page * pageScale;
					break;
				
                default:
				case 7:
					CMYK.c = (1.0 - tileY);
					CMYK.m = 0;
					CMYK.y = tileX;
					CMYK.k = (page+horizPages) * pageScale;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKBoxes2.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
void makeCMYKRamp1(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            CMYKColor CMYK;
			
			switch( (int)tile )
				{
				case 0:
					CMYK.c = 1.0 - xfraction;                      // red
					CMYK.m = 1.0 - xfraction*(1.0-tileY);
					CMYK.y = 1.0 - xfraction*(1.0-tileY);
					CMYK.k = 0;
					break;
				
				case 1:
					CMYK.c = 1.0 - xfraction;                      // orange
					CMYK.m = 1.0 - xfraction * ((255.0-140.0)/255.0 + (140.0/255.0) * (1.0-tileY));
					CMYK.y = 1.0 - xfraction*(1.0-tileY);
					CMYK.k = 0;
					break;
				
				case 2:
					CMYK.c = 1.0 - xfraction;                      // yellow
					CMYK.m = 1.0 - xfraction;
					CMYK.y = 1.0 - xfraction*(1.0-tileY);
					CMYK.k = 0;
					break;
				
				case 3:
					CMYK.c = 1.0 - xfraction*(1.0-tileY);        // green
					CMYK.m = 1.0 - xfraction;
					CMYK.y = 1.0 - xfraction*(1.0-tileY);
					CMYK.k = 0;
					break;
				
				case 4:
					CMYK.c = 1.0 - xfraction*(1.0-tileY);        // cyan
					CMYK.m = 1.0 - xfraction;
					CMYK.y = 1.0 - xfraction;
					CMYK.k = 0;
					break;
				
				case 5:
					CMYK.c = 1.0 - xfraction*(1.0-tileY);        // blue
					CMYK.m = 1.0 - xfraction*(1.0-tileY);
					CMYK.y = 1.0 - xfraction;
					CMYK.k = 0;
					break;
				
				case 6:
					CMYK.c = 1.0 - xfraction * ((255.0-135.0)/255.0 + (135.0/255.0) * (1.0-tileY));
					CMYK.m = 1.0 - xfraction*(1.0-tileY);        // violet
					CMYK.y = 1.0 - xfraction;
					CMYK.k = 0;
					break;
				
				case 7:
					CMYK.c = 1.0 - xfraction;                      // magenta
					CMYK.m = 1.0 - xfraction*(1.0-tileY);
					CMYK.y = 1.0 - xfraction;
					CMYK.k = 0;
					break;
				
				case 8:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					CMYK.c = 1.0 - (2.0*page)/9.0;
					CMYK.m = 1.0 - tileX;
					CMYK.y = 1.0 - tileY;
					CMYK.k = 0;
					}
					break;
				
                default:
				case 9:
					{
					double page = floor(xfraction*5.0);
					double tileX = xfraction*5.0 - page;
					CMYK.c = 1.0 - (1.0 + 2.0*page)/9.0;
					CMYK.m = 1.0 - tileX;
					CMYK.y = 1.0 - tileY;
					CMYK.k = 0;
					}
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );

		}
	}
    
    image.WriteTIFF( "CMYKRamp1.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// Different hue slices from low to high saturation, black to white.
void makeCMYKRamp2(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            double page = floor(xfraction*5.0);
            double tileX = xfraction*5.0 - page;
            CMYKColor CMYK;
			
			switch( (int)tile )
				{
				case 0:
					CMYK.c = 0;                      // red
					CMYK.m = tileY;
					CMYK.y = tileY;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 1:
					CMYK.c = 0;                      // orange
					CMYK.m = (140.0/255.0) * tileY;
					CMYK.y = tileY;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 2:
					CMYK.c = 0;                      // yellow
					CMYK.m = 0;
					CMYK.y = tileY;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 3:
					CMYK.c = tileY;                 // green
					CMYK.m = 0;
					CMYK.y = tileY;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 4:
					CMYK.c = tileY;                 // cyan
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 5:
					CMYK.c = tileY;                 // blue
					CMYK.m = tileY;
					CMYK.y = 0;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 6:
					CMYK.c = (135.0/255.0) * tileY;
					CMYK.m = tileY;                   // violet
					CMYK.y = 0;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 7:
					CMYK.c = 0;                      // magenta
					CMYK.m = tileY;
					CMYK.y = 0;
					CMYK.k = 1.0 - xfraction;
					break;
				
				case 8:
					CMYK.c = 0;
					CMYK.m = tileX;
					CMYK.y = tileY;
					CMYK.k = 1.0 - (2.0*page)/9.0;
					break;
				
                default:
				case 9:
					CMYK.c = 0;
					CMYK.m = tileX;
					CMYK.y = tileY;
					CMYK.k = 1.0 - (1.0 + 2.0*page)/9.0;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );

		}
	}
    
    image.WriteTIFF( "CMYKRamp2.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// Different slices around neutral -- where many profiles show errors.
void makeCMYKRamp3(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
        double cy = tileY - 0.5;                // -0.5..0.5
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            double cx = xfraction - 0.5;        // -0.5..0.5
            const double range = 100;
            const double rangeScale = std::min( 128.0 + 0.5*range, 255.0 );
            CMYKColor CMYK;

			switch( (int)tile )
				{
				case 0:
					CMYK.c = 1.0 - xfraction*(128 - range*cy)/rangeScale;          // red-cyan
					CMYK.m = 1.0 - xfraction*(128 + range*cy)/rangeScale;
					CMYK.y = 1.0 - xfraction*(128 + range*cy)/rangeScale;
                    CMYK.k = 0;
					break;
				
				case 1:
					CMYK.c = 1.0 - xfraction * (128 - range*cy)/rangeScale;        // orange-violet
					CMYK.m = 1.0 - xfraction * (140/255.0);
					CMYK.y = 1.0 - xfraction * (128 + range*cy)/rangeScale;
                    CMYK.k = 0;
					break;
				
				case 2:
					CMYK.c = 1.0 - xfraction * (128 - range*cy)/rangeScale;        // yellow-blue
					CMYK.m = 1.0 - xfraction * (128 - range*cy)/rangeScale;
					CMYK.y = 1.0 - xfraction * (128 + range*cy)/rangeScale;
                    CMYK.k = 0;
					break;
				
				case 3:
					CMYK.c = 1.0 - xfraction * (128 + range*cy)/rangeScale;        // green-magenta
					CMYK.m = 1.0 - xfraction * (128 - range*cy)/rangeScale;
					CMYK.y = 1.0 - xfraction * (128 + range*cy)/rangeScale;
                    CMYK.k = 0;
					break;
				
				case 4:
					CMYK.c = 1.0 - xfraction * (128 + range*cy)/rangeScale;        // green-red
					CMYK.m = 1.0 - xfraction * (128 - range*cy)/rangeScale;
					CMYK.y = 1.0 - xfraction * (140/255.0);
                    CMYK.k = 0;
					break;
				
				case 5:
					CMYK.c = 1.0 - xfraction * (128 + range*cy)/rangeScale;        // teal-peach
					CMYK.m = 1.0 - xfraction * (140/255.0);
					CMYK.y = 1.0 - xfraction * (140/255.0);
                    CMYK.k = 0;
					break;
				
				case 6:
					CMYK.c = 1.0 - xfraction * (135/255.0);
					CMYK.m = 1.0 - xfraction * (128 + range*cy)/rangeScale;        // violet-green
					CMYK.y = 1.0 - xfraction * (128 - range*cy)/rangeScale;
                    CMYK.k = 0;
					break;
				
				case 7:
					CMYK.c = 1.0 - xfraction * (128 - range*cy)/rangeScale;        // rose-teal
					CMYK.m = 1.0 - xfraction * (128 + (range/2)*cy)/rangeScale;
					CMYK.y = 1.0 - xfraction * (140/255.0);
                    CMYK.k = 0;
					break;
				
				case 8:
					CMYK.c = (128 + range*cx)/255.0;
					CMYK.m = 128 / 255.0;
					CMYK.y = (128 + range*cy)/255.0;
                    CMYK.k = 0;
					break;
				
                default:
				case 9:
					CMYK.c = (128 + range*cx)/255.0;
					CMYK.m = (128 + range*cy)/255.0;
					CMYK.y = 128 / 255.0;
                    CMYK.k = 0;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKRamp3.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// Different slices around neutral -- where many profiles show errors.
void makeCMYKRamp4(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
        double cy = tileY - 0.5;                // -0.5..0.5
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            double cx = xfraction - 0.5;        // -0.5..0.5
            const double range = 100;
            CMYKColor CMYK;
            double x3 = 0.8 * sqrt(xfraction);

			switch( (int)tile )
				{
				case 0:
					CMYK.c = x3*(128 + range*cy)/255.0;         // red-cyan
					CMYK.m = x3*(128 - range*cy)/255.0;
					CMYK.y = x3*(128 - range*cy)/255.0;
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 1:
					CMYK.c = x3 * (128 + range*cy)/255.0;        // orange-violet
					CMYK.m = x3 * 140/255.0;
					CMYK.y = x3 * (128 - range*cy)/255.0;
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 2:
					CMYK.c = x3 * (128 + range*cy)/255.0;        // yellow-blue
					CMYK.m = x3 * (128 + range*cy)/255.0;
					CMYK.y = x3 * (128 - range*cy)/255.0;
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 3:
					CMYK.c = x3 * (128 - range*cy)/255.0;         // green-magenta
					CMYK.m = x3 * (128 + range*cy)/255.0;
					CMYK.y = x3 * (128 - range*cy)/255.0;
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 4:
					CMYK.c = x3 * (128 - range*cy)/255.0;         // green-red
					CMYK.m = x3 * (128 + range*cy)/255.0;
					CMYK.y = x3 * (140/255.0);
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 5:
					CMYK.c = x3 * (128 - range*cy)/255.0;         // teal-peach
					CMYK.m = x3 * (140/255.0);
					CMYK.y = x3 * (140/255.0);
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 6:
					CMYK.c = x3 * (135/255.0);
					CMYK.m = x3 * (128 - range*cy)/255.0;         // violet-green
					CMYK.y = x3 * (128 + range*cy)/255.0;
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 7:
					CMYK.c = x3 * (128 + range*cy)/255.0;         // rose-teal
					CMYK.m = x3 * (128 - (range/2)*cy)/255.0;
					CMYK.y = x3 * (140/255.0);
                    CMYK.k = 1.0 - xfraction;
					break;
				
				case 8:
					CMYK.c = (128 + range*cx)/255.0;
					CMYK.m = 128 / 255.0;
					CMYK.y = (128 + range*cy)/255.0;
                    CMYK.k = 0.35;
					break;
				
                default:
				case 9:
					CMYK.c = (128 + range*cx)/255.0;
					CMYK.m = (128 + range*cy)/255.0;
					CMYK.y = 128 / 255.0;
                    CMYK.k = 0.35;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKRamp4.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// Different hue slices from low to high saturation with just 1 or 2 inks
void makeCMYKRamp5(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(10.0 * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*10.0);
		double tileY = yfraction*10.0 - tile;   // 0..1
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col/ (double)PAGE_WIDTH;
            CMYKColor CMYK;
			
			switch( (int)tile )
				{
				case 0:
					CMYK.c = xfraction;          // cyan
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = 0;
					break;
				
				case 1:
					CMYK.c = 0;                  // magenta
					CMYK.m = xfraction;
					CMYK.y = 0;
					CMYK.k = 0;
					break;
				
				case 2:
					CMYK.c = 0;                  // yellow
					CMYK.m = 0;
					CMYK.y = xfraction;
					CMYK.k = 0;
					break;
				
				case 3:
					CMYK.c = 0;                  // black
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = xfraction;
					break;
				
				case 4:
					CMYK.c = 0;                  // red
					CMYK.m = xfraction;
					CMYK.y = tileY;
					CMYK.k = 0;
					break;
				
				case 5:
					CMYK.c = tileY;              // green
					CMYK.m = 0;
					CMYK.y = xfraction;
					CMYK.k = 0;
					break;
				
				case 6:
					CMYK.c = xfraction;
					CMYK.m = tileY;              // blue
					CMYK.y = 0;
					CMYK.k = 0;
					break;
				
				case 7:
					CMYK.c = xfraction;          // CK
					CMYK.m = 0;
					CMYK.y = 0;
					CMYK.k = tileY;
					break;
				
				case 8:
					CMYK.c = 0;                 // MK
					CMYK.m = xfraction;
					CMYK.y = 0;
					CMYK.k = tileY;
					break;
				
                default:
				case 9:
					CMYK.c = 0;                 // YK
					CMYK.m = 0;
					CMYK.y = xfraction;
					CMYK.k = tileY;
					break;
				
				}
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );

		}
	}
    
    image.WriteTIFF( "CMYKRamp5.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

void makeCMYKRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );

    makeCMYKBoxes1(res,depth);
    makeCMYKBoxes2(res,depth);
    makeCMYKRamp1(res,depth);
    makeCMYKRamp2(res,depth);
    makeCMYKRamp3(res,depth);
    makeCMYKRamp4(res,depth);
    makeCMYKRamp5(res,depth);
}

/******************************************************************************/

const int kLong_vertical_steps = 10;
const double kLong_seperation_fraction = 0.05;

/******************************************************************************/

void makeLongGrayRamp(double res, int depth, bool folded = false)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;
    const double half = (depth == 16) ? (0.5/65535.0) : (0.5/255.0);
    const double sep_fraction = folded ? 0.0 : kLong_seperation_fraction;

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 1 );
    
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
        int iTile = tile;
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            double gray;
            
            if (folded && (iTile & 1) == 1)
                xfraction = 1.0 - xfraction;
			
            if (tileY < sep_fraction)
                gray = 1.0;     // white
            else
                gray = (tile + xfraction) * tile_fraction + half;
            
            image.SetPixel( col, row, gray );
		}
	}
    
    const char *outName = folded ? "GrayFoldedRamp.tif" : "GrayLongRamp.tif";
    image.WriteTIFF( outName, res, TIFF_MODE_GRAY_BLACKZERO );
}

/******************************************************************************/

void makeLongRGBRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;
    const double half = (depth == 16) ? (0.5/65535.0) : (0.5/255.0);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            double gray;
			
            if (tileY < kLong_seperation_fraction)
                gray = 1.0;     // white
            else
                gray = (tile + xfraction) * tile_fraction + half;
            
            RGBColor RGB;
            RGB.r = RGB.g = RGB.b = gray;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBGrayLongRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            RGBColor RGB;
			RGB.g = RGB.b = 0.0;
            
            if (tileY < kLong_seperation_fraction)
                RGB.r = RGB.g = RGB.b = 1.0;     // white
            else
                RGB.r = (tile + xfraction) * tile_fraction + half;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBRedLongRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            RGBColor RGB;
			RGB.r = RGB.b = 0.0;
            
            if (tileY < kLong_seperation_fraction)
                RGB.r = RGB.g = RGB.b = 1.0;     // white
            else
                RGB.g = (tile + xfraction) * tile_fraction + half;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBGreenLongRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            RGBColor RGB;
			RGB.r = RGB.g = 0.0;
            
            if (tileY < kLong_seperation_fraction)
                RGB.r = RGB.g = RGB.b = 1.0;     // white
            else
                RGB.b = (tile + xfraction) * tile_fraction + half;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "RGBBlueLongRamp.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

void makeLongLABRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;
    const double half = (depth == 16) ? (0.5/65535.0) : (0.5/255.0);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            LABColor LAB;
            LAB.a = LAB.b = 0.5;
            
            if (tileY < kLong_seperation_fraction) {
                LAB.L = 1.0;     // white
            }
            else {
                LAB.L = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LABLLongRamp.tif", res, TIFF_MODE_CIELAB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            LABColor LAB;
            LAB.a = LAB.b = 0.5;
            
            if (tileY < kLong_seperation_fraction) {
                LAB.L = 1.0;     // white
            }
            else {
                LAB.L = 0.5;
                LAB.a = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LABALongRamp.tif", res, TIFF_MODE_CIELAB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            LABColor LAB;
            LAB.a = LAB.b = 0.5;
            
            if (tileY < kLong_seperation_fraction) {
                LAB.L = 1.0;     // white
            }
            else {
                LAB.L = 0.5;
                LAB.b = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LABBLongRamp.tif", res, TIFF_MODE_CIELAB );

}

/******************************************************************************/

void makeLongCMYKRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;
    const double half = (depth == 16) ? (0.5/65535.0) : (0.5/255.0);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 4 );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            CMYKColor CMYK;
            CMYK.c = CMYK.m = CMYK.y = CMYK.k = 0.0;
            
            if (tileY < kLong_seperation_fraction) {
                CMYK.k = 0.0;     // white
            }
            else {
                CMYK.c = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKCLongRamp.tif", res, TIFF_MODE_CMYK );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            CMYKColor CMYK;
            CMYK.c = CMYK.m = CMYK.y = CMYK.k = 0.0;
            
            if (tileY < kLong_seperation_fraction) {
                CMYK.k = 0.0;     // white
            }
            else {
                CMYK.m = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKMLongRamp.tif", res, TIFF_MODE_CMYK );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            CMYKColor CMYK;
            CMYK.c = CMYK.m = CMYK.y = CMYK.k = 0.0;
            
            if (tileY < kLong_seperation_fraction) {
                CMYK.k = 0.0;     // white
            }
            else {
                CMYK.y = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKYLongRamp.tif", res, TIFF_MODE_CMYK );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            CMYKColor CMYK;
            CMYK.c = CMYK.m = CMYK.y = CMYK.k = 0.0;
            
            if (tileY < kLong_seperation_fraction) {
                CMYK.k = 0.0;     // white
            }
            else {
                CMYK.k = (tile + xfraction) * tile_fraction + half;
            }
            
            image.SetPixel( col, row, CMYK.c, CMYK.m, CMYK.y, CMYK.k );
		}
	}
    
    image.WriteTIFF( "CMYKKLongRamp.tif", res, TIFF_MODE_CMYK );

}

/******************************************************************************/

void makeLongHSLRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

    /* HSL L == RGB Gray */

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            HSLColor	HSL;
            RGBColor RGB;
            HSL.h = HSL.s = HSL.l = 0.0;
            
            if (tileY < kLong_seperation_fraction)
                HSL.l = 1.0;     // white
            else {
                HSL.s = 0.60;
                HSL.l = 0.50;
                HSL.h = (tile + xfraction) * tile_fraction;
            }

            // do the conversions
            ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLHLongRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            HSLColor	HSL;
            RGBColor RGB;
            HSL.h = HSL.s = HSL.l = 0.0;
            
            if (tileY < kLong_seperation_fraction)
                HSL.l = 1.0;     // white
            else {
                HSL.h = 0.0;
                HSL.l = 0.50;
                HSL.s = (tile + xfraction) * tile_fraction;
            }

            // do the conversions
            ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLSLongRamp.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

void makeLongLCHRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kLong_vertical_steps * res);
    const double tile_fraction = 1.0 / kLong_vertical_steps;

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

    /* LCH L == LAB L */

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            LABColor LAB;
            LAB.a = LAB.b = 0.5;
            
            if (tileY < kLong_seperation_fraction) {
                LAB.L = 1.0;     // white
            }
            else {
                double H = 0.0;
                double C = (tile + xfraction) * tile_fraction;
                LAB.L = 0.6;
				LAB.a = 0.5 * C * cos( H ) + 0.5;
				LAB.b = 0.5 * C * sin( H ) + 0.5;
                LAB.a = std::min( std::max( LAB.a, 0.0 ), 1.0 );
                LAB.b = std::min( std::max( LAB.b, 0.0 ), 1.0 );
            }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LCHCLongRamp.tif", res, TIFF_MODE_CIELAB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kLong_vertical_steps);
		double tileY = yfraction*kLong_vertical_steps - tile;
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-3);
            LABColor LAB;
            LAB.a = LAB.b = 0.5;
            
            if (tileY < kLong_seperation_fraction) {
                LAB.L = 1.0;     // white
            }
            else {
                double C = 0.5;
                double H = 2.0 * M_PI * ((tile + xfraction) * tile_fraction);
                LAB.L = 0.5;
				LAB.a = 0.5 * C * cos( H ) + 0.5;
				LAB.b = 0.5 * C * sin( H ) + 0.5;
                LAB.a = std::min( std::max( LAB.a, 0.0 ), 1.0 );
                LAB.b = std::min( std::max( LAB.b, 0.0 ), 1.0 );
            }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "LCHHLongRamp.tif", res, TIFF_MODE_CIELAB );

}

/******************************************************************************/

void makeLongPrimaryRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );
    
    makeLongGrayRamp(res,depth);
    makeLongGrayRamp(res,depth,true);
    makeLongRGBRamps(res,depth);
    makeLongLABRamps(res,depth);
    makeLongCMYKRamps(res,depth);
    makeLongHSLRamps(res,depth);
    makeLongLCHRamps(res,depth);

// CIECAM?  Too many artifacts - but might be avoided with the right values?
// is there any real need?

}

/******************************************************************************/

// 256 steps to 8 steps
// can do more for 16 bit images, but few displays show it
const int kQuantizationCount = 6;
const int kQuantizationBaseShift = 2;        // 8 steps, 1 << 3 - but we stop one early

void makeQuantizedGrayRamp(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kQuantizationCount * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth, 1 );
    
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            image.SetPixel( col, row, gray );
		}
	}
    
    image.WriteTIFF( "QuantizedGrayRamp.tif", res, TIFF_MODE_GRAY_BLACKZERO );
}

/******************************************************************************/

void makeQuantizedRGBRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kQuantizationCount * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            RGBColor RGB;
			RGB.g = RGB.b = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            RGB.r = gray;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedRGBRedRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            RGBColor RGB;
			RGB.r = RGB.b = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            RGB.g = gray;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedRGBGreenRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            RGBColor RGB;
			RGB.r = RGB.g = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            RGB.b = gray;
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedRGBBlueRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
		double tileY = yfraction*kQuantizationCount - tile;
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            RGBColor RGB;
			RGB.r = RGB.g = RGB.b = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            int channel = floor(tileY * 3.0);
            
            switch (channel)
                {
                default:
                case 0:
                    RGB.r = gray;
                    break;
                case 1:
                    RGB.g = gray;
                    break;
                case 2:
                    RGB.b = gray;
                    break;
                }
            
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedRGBBRamp.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

void makeQuantizedLABRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kQuantizationCount * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = 0.6;
            LAB.b = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            LAB.a = gray;
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLABARamp.tif", res, TIFF_MODE_CIELAB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = 0.6;
            LAB.a = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            LAB.b = gray;
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLABBRamp.tif", res, TIFF_MODE_CIELAB );


// TODO - shouldn't L be quantized in terms of 100 to match ab steps?
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
		double tileY = yfraction*kQuantizationCount - tile;
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = 0.7;
            LAB.a = LAB.b = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            int channel = floor(tileY * 3.0);
            
            switch (channel)
                {
                default:
                case 0:
                    LAB.L = gray;
                    break;
                case 1:
                    LAB.a = gray;
                    break;
                case 2:
                    LAB.b = gray;
                    break;
                }
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLABRamp.tif", res, TIFF_MODE_CIELAB );

}

/******************************************************************************/

void makeQuantizedLCHRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kQuantizationCount * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            double H = 0.0;
            double C = gray;
            
            LAB.a = 0.5 * C * cos( H ) + 0.5;
            LAB.b = 0.5 * C * sin( H ) + 0.5;
            LAB.a = std::min( std::max( LAB.a, 0.0 ), 1.0 );
            LAB.b = std::min( std::max( LAB.b, 0.0 ), 1.0 );
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLCHCRamp.tif", res, TIFF_MODE_CIELAB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            double H = gray * 2.0 * M_PI;
            double C = 0.5;
            
            LAB.a = 0.5 * C * cos( H ) + 0.5;
            LAB.b = 0.5 * C * sin( H ) + 0.5;
            LAB.a = std::min( std::max( LAB.a, 0.0 ), 1.0 );
            LAB.b = std::min( std::max( LAB.b, 0.0 ), 1.0 );
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLCHHRamp.tif", res, TIFF_MODE_CIELAB );


// TODO - shouldn't L be quantized in terms of 100 to match ab steps?
	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
		double tileY = yfraction*kQuantizationCount - tile;
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            LABColor LAB;
            LAB.L = LAB.a = LAB.b = 0.5;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            int channel = floor(tileY * 3.0);
            
            double H = 0.0;
            double C = 0.5;
            
            switch (channel)
                {
                default:
                case 0:
                    LAB.L = gray;
                    C = 0.0;
                    break;
                case 1:
                    C = gray;
                    break;
                case 2:
                    H = gray * 2.0 * M_PI;
                    break;
                }
            
            LAB.a = 0.5 * C * cos( H ) + 0.5;
            LAB.b = 0.5 * C * sin( H ) + 0.5;
            LAB.a = std::min( std::max( LAB.a, 0.0 ), 1.0 );
            LAB.b = std::min( std::max( LAB.b, 0.0 ), 1.0 );
            
            image.SetPixel( col, row, LAB.L, LAB.a, LAB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedLCHRamp.tif", res, TIFF_MODE_CIELAB );

}

/******************************************************************************/

void makeQuantizedHSLRamps(double res, int depth)
{
    const int PAGE_WIDTH = (int)(8.0 * res);
    const int PAGE_HEIGHT = (int)(kQuantizationCount * res);

    imageBuffer image( PAGE_WIDTH, PAGE_HEIGHT, depth );

	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            HSLColor	HSL;
            RGBColor RGB;
            HSL.h = HSL.s = HSL.l = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            HSL.s = 0.60;
            HSL.l = 0.50;
            HSL.h = gray;

            // do the conversions
            ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedHSLHRamp.tif", res, TIFF_MODE_RGB );



	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            HSLColor	HSL;
            RGBColor RGB;
            HSL.h = HSL.s = HSL.l = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            HSL.h = 0.0;
            HSL.l = 0.50;
            HSL.s = gray;

            // do the conversions
            ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedHSLSRamp.tif", res, TIFF_MODE_RGB );


	for ( int row = 0; row < PAGE_HEIGHT; ++row ) {
		double yfraction = row / (double)PAGE_HEIGHT;
		
		double tile = floor(yfraction*kQuantizationCount);
		double tileY = yfraction*kQuantizationCount - tile;
        int iTile = tile;
        int64_t steps = (1LL << (kQuantizationCount - iTile + kQuantizationBaseShift));
		
		for ( int col = 0; col < PAGE_WIDTH; ++col ) {
			double xfraction = col / (double)(PAGE_WIDTH-1);
            HSLColor	HSL;
            RGBColor RGB;
            HSL.h = HSL.s = HSL.l = 0.0;
            
            double gray = floor(xfraction * steps) / double(steps-1);
            gray = std::max( 0.0, std::min( 1.0, gray ));
            
            int channel = floor(tileY * 3.0);
            
            switch (channel)
                {
                default:
                case 0:
                    HSL.s = 0.60;
                    HSL.l = 0.50;
                    HSL.h = gray;
                    break;
                case 1:
                    HSL.h = 0.0;
                    HSL.l = 0.50;
                    HSL.s = gray;
                    break;
                case 2:
                    HSL.l = gray;
                    break;
                }

            // do the conversions
            ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "QuantizedHSLRamp.tif", res, TIFF_MODE_RGB );

}

/******************************************************************************/

void makeQuantizedRamps(double res, int depth)
{
    assert( depth == 8 || depth == 16 );
    
    makeQuantizedGrayRamp(res,depth);
    makeQuantizedRGBRamps(res,depth);
    makeQuantizedLABRamps(res,depth);
    makeQuantizedLCHRamps(res,depth);
    makeQuantizedHSLRamps(res,depth);

}

/******************************************************************************/

void DoHSLCircle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSLColor	HSL;
                RGBColor	RGB;
				
				// setup coordinates based on image position
				HSL.h = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
				HSL.s = 192.0 / 255.0;
				HSL.l = 2.0 * (0.5 - radius);	// white center to black outside

				// do the conversions
				ConvertHSLtoRGB( HSL, RGB );
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0, 0 );
			}
		}
	}
    
    image.WriteTIFF( "HSLCircle1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSLCircle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSLColor	HSL;
                RGBColor	RGB;
				
				// setup coordinates based on image position
				HSL.h = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
				HSL.s = 2.0 * (0.5 - radius);
				HSL.l = 192.0 / 255.0;

				// do the conversions
				ConvertHSLtoRGB( HSL, RGB );
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0, 0 );
			}
		}
	}
    
    image.WriteTIFF( "HSLCircle2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSLCircle3(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    const double hue_steps = 12;
    const double light_steps = 5;
    const double divide_fraction = 0.07;
    const double background_gray = 0.35;
    const double radial_divide_fraction = divide_fraction * light_steps / 5.0;
    const double angle_divide_fraction = hue_steps * divide_fraction / 120;

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSLColor	HSL;
                RGBColor	RGB;
                
				// setup coordinates based on image position
                double hue = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
                double light = 2.0 * radius;	// dark center to light outside
                
                double fractionh = hue * hue_steps;
                double fractionl = light * light_steps;
                
                double qh = floor(fractionh);
                double ql = floor(light * light_steps);
                
                fractionh -= qh;
                fractionl -= ql;
                
                double rfractionh = (1.0 - fractionh) * radius;
                fractionh *= radius;
                
                if ( fractionh < angle_divide_fraction || rfractionh < angle_divide_fraction
                    || fractionl < radial_divide_fraction ) {
                    RGB.r = RGB.g = RGB.b = background_gray;
                } else {
                    
                    double scaledh = qh / hue_steps;
                    double scaledl = (0.9 + ql) / (light_steps+1);
                    
                    HSL.h = scaledh;
                    HSL.s = 192.0 / 255.0;
                    HSL.l = scaledl;
                    
                    // do the conversions
                    ConvertHSLtoRGB( HSL, RGB );
                }
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with gray
                image.SetPixel( col, row, background_gray, background_gray, background_gray );
			}
		}
	}
    
    image.WriteTIFF( "HSLCircle3.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSVCircle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSVColor	HSV;
                RGBColor	RGB;
				
				// setup coordinates based on image position
				HSV.h = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
				HSV.s = 192.0 / 255.0;
				HSV.v = 2.0 * (0.5 - radius);	// white center to black outside

				// do the conversions
				ConvertHSVtoRGB( HSV, RGB );
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0, 0 );
			}
		}
	}
    
    image.WriteTIFF( "HSVCircle1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSVCircle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSVColor	HSV;
                RGBColor	RGB;
				
				// setup coordinates based on image position
				HSV.h = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
				HSV.s = 2.0 * (0.5 - radius);
				HSV.v = 192.0 / 255.0;

				// do the conversions
				ConvertHSVtoRGB( HSV, RGB );
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0, 0 );
			}
		}
	}
    
    image.WriteTIFF( "HSVCircle2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSVCircle3(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    const double hue_steps = 12;
    const double light_steps = 5;
    const double divide_fraction = 0.07;
    const double background_gray = 0.35;
    const double radial_divide_fraction = divide_fraction * light_steps / 5.0;
    const double angle_divide_fraction = hue_steps * divide_fraction / 120;

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                HSVColor	HSV;
                RGBColor	RGB;
                
				// setup coordinates based on image position
                double hue = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
                double light = 2.0 * radius;	// dark center to light outside
                
                double fractionh = hue * hue_steps;
                double fractionl = light * light_steps;
                
                double qh = floor(fractionh);
                double ql = floor(light * light_steps);
                
                fractionh -= qh;
                fractionl -= ql;
                
                double rfractionh = (1.0 - fractionh) * radius;
                fractionh *= radius;
                
                if ( fractionh < angle_divide_fraction || rfractionh < angle_divide_fraction
                    || fractionl < radial_divide_fraction ) {
                    RGB.r = RGB.g = RGB.b = background_gray;
                } else {
                    
                    double scaledh = qh / hue_steps;
                    double scaledl = (0.9 + ql) / (light_steps+1);
                    
                    HSV.h = scaledh;
                    HSV.s = 192.0 / 255.0;
                    HSV.v = scaledl;
                    
                    // do the conversions
                    ConvertHSVtoRGB( HSV, RGB );
                }
				
                image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
			}
			else {
				// fill with gray
                image.SetPixel( col, row, background_gray, background_gray, background_gray );
			}
		}
	}
    
    image.WriteTIFF( "HSVCircle3.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoLCHCircle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                LABColor	LAB;
				
				// setup coordinates based on image position
				double hue = atan2( yfraction, xfraction ) + M_PI;
				double saturation = 0.75;
				
				LAB.L = (255.0 * 2.0 * (0.5 - radius)) + 0.1;	// white center to black outside
				
				LAB.a = Pin255( 127.0 * saturation * cos( hue ) + 128.0 );
				LAB.b = Pin255( 127.0 * saturation * sin( hue ) + 128.0 );
				
                image.SetPixel( col, row, LAB.L/255.0, LAB.a/255.0, LAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "LCHCircle1.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoLCHCircle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                LABColor	LAB;
				
				// setup coordinates based on image position
				double hue = atan2( yfraction, xfraction ) + M_PI;
				double saturation = 2.0 * (0.5 - radius);	// white center to black outside
				
				LAB.L = 0.75 * 255.0;
				
				LAB.a = Pin255( 127.0 * saturation * cos( hue ) + 128.0 );
				LAB.b = Pin255( 127.0 * saturation * sin( hue ) + 128.0 );
				
                image.SetPixel( col, row, LAB.L/255.0, LAB.a/255.0, LAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "LCHCircle2.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoLCHCircle3(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    const double hue_steps = 12;
    const double light_steps = 5;
    const double divide_fraction = 0.07;
    const double background_gray = 0.38;    // closer to HSL value of 0.35 (89/255)
    const double radial_divide_fraction = divide_fraction * light_steps / 5.0;
    const double angle_divide_fraction = hue_steps * divide_fraction / 120;
    // to make this align better with HSL/HSV circles
    const double red_hue_adjust = 0.65; // approx for sRGB and AdobeRGB

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
                LABColor	LAB;
                
				// setup coordinates based on image position
                double hue = ( atan2( yfraction, xfraction ) + M_PI ) / (2.0 * M_PI);
                double light = 2.0 * radius;	// dark center to light outside
                
                double fractionh = hue * hue_steps;
                double fractionl = light * light_steps;
                
                double qh = floor(fractionh);
                double ql = floor(light * light_steps);
                
                fractionh -= qh;
                fractionl -= ql;
                
                double rfractionh = (1.0 - fractionh) * radius;
                fractionh *= radius;
                
                if ( fractionh < angle_divide_fraction || rfractionh < angle_divide_fraction
                    || fractionl < radial_divide_fraction ) {
                    image.SetPixel( col, row, background_gray, 0.5, 0.5 );
                } else {
                    
                    double scaledh = qh / hue_steps;
                    double scaledl = (0.9 + ql) / (light_steps+1);
                    double saturation = 0.65;
                    
                    // reduce lightness to decrease clipping, and look more like HSL/HSV circle
                    LAB.L = (230.0 * scaledl);	// dark center to light outside
                    
                    LAB.a = Pin255( 127.0 * saturation * cos( scaledh * (2.0 * M_PI) + red_hue_adjust ) + 128.0 );
                    LAB.b = Pin255( 127.0 * saturation * sin( scaledh * (2.0 * M_PI) + red_hue_adjust ) + 128.0 );
                    
                    image.SetPixel( col, row, LAB.L/255.0, LAB.a/255.0, LAB.b/255.0 );
                }
				
			}
			else {
				// fill with gray
                image.SetPixel( col, row, background_gray, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "LCHCircle3.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM97Circle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	// calculate a few useful values
	SCENE scene;
		scene.BackgroundColor.x = whitePointD50.x;
		scene.BackgroundColor.y = whitePointD50.y;
		scene.BackgroundColor.z = whitePointD50.z;
		scene.BackgroundLuminance = 20.0;
		scene.AdaptingLuminance = 318;		// (cd/m^2)

    // average surround, small samples
	VIEW view;
		view.SurroundImpact = 0.69;
		view.ChromaticInduction = 1.0;
		view.LightnessContrast = 1.0;
		view.AdaptationDegree = 1.0;
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
				TRISTIMULUS jch, xyz;
				
				// setup coordinates based on image position
				jch.j = 100.0 * 2.0 * (0.5 - radius) + 0.1;	// white center to not-quite-black outside
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);
				jch.c = 95.25;		// closer match to LCH circles  (127.0 * 0.75)
			
				xyz = JCH2XYZ( jch, scene, view );
				
				myXYZ.x = xyz.x;
				myXYZ.y = xyz.y;
				myXYZ.z = xyz.z;
                
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM97Circle1.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM97Circle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	// calculate a few useful values
	SCENE scene;
		scene.BackgroundColor.x = whitePointD50.x;  // D50
		scene.BackgroundColor.y = whitePointD50.y;
		scene.BackgroundColor.z = whitePointD50.z;
		scene.BackgroundLuminance = 20.0;
		scene.AdaptingLuminance = 318;		// (cd/m^2)

    // average surround, small samples
	VIEW view;
		view.SurroundImpact = 0.69;
		view.ChromaticInduction = 1.0;
		view.LightnessContrast = 1.0;
		view.AdaptationDegree = 1.0;
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
				TRISTIMULUS jch, xyz;
				
				// setup coordinates based on image position
				jch.j = 75.0;
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);
				jch.c = 127.0 * 2.0 * (0.5 - radius);		// closer match to LCH circles
				
				xyz = JCH2XYZ( jch, scene, view );
				
				myXYZ.x = xyz.x;
				myXYZ.y = xyz.y;
				myXYZ.z = xyz.z;
				
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM97Circle2.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM02Circle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM02Init( (cmsContext) 1, &view );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
                cmsJCh jch;
                cmsCIEXYZ xyz;
				
				// setup coordinates based on image position
				jch.J = 100.0 * 2.0 * (0.5 - radius) + 0.1;	// white center to not-quite-black outside
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);
				jch.C = 95.25;		// closer match to LCH circles  (127.0 * 0.75)

                cmsCIECAM02Reverse( cms, &jch, &xyz );
				
				myXYZ.x = xyz.X;
				myXYZ.y = xyz.Y;
				myXYZ.z = xyz.Z;
                
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM02Circle1.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM02Done(cms);
}

/******************************************************************************/

void DoCIECAM02Circle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM02Init( (cmsContext) 1, &view );
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
                cmsJCh jch;
                cmsCIEXYZ xyz;
				
				// setup coordinates based on image position
				jch.J = 75.0;
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);
				jch.C = 127.0 * 2.0 * (0.5 - radius);		// closer match to LCH circles
				
                cmsCIECAM02Reverse( cms, &jch, &xyz );
				
				myXYZ.x = xyz.X;
				myXYZ.y = xyz.Y;
				myXYZ.z = xyz.Z;
				
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM02Circle2.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM02Done(cms);
}

/******************************************************************************/

void DoCIECAM16Circle1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );

    cmsViewingConditions view;

#if 0 || UNIT_TEST_CAM16
    {
    // examples from spec.
    view.whitePoint.X = 96.46;
    view.whitePoint.Y = 100.0;
    view.whitePoint.Z = 108.62;

    view.Yb = 16.0;
    view.La = 40.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );
    
    cmsJCh jch;
    cmsCIEXYZ xyz;
    
    xyz.X = 60.70;
    xyz.Y = 49.60;
    xyz.Z = 10.29;
    cmsCIECAM16Forward( cms, &xyz, &jch);
    assert( fabs(jch.J - 70.4406) < 1e-4 );
    assert( fabs(jch.C - 58.6035) < 1e-4 );
    assert( fabs(jch.h - 57.9145) < 1e-4 );


    jch.J = 70.4400;
    jch.C = 58.6000;
    jch.h = 57.9128;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    assert( fabs(xyz.X - 60.6988) < 1e-4 );
    assert( fabs(xyz.Y - 49.5993) < 1e-4 );
    assert( fabs(xyz.Z - 10.2917) < 1e-4 );

    cmsCIECAM16Done(cms);
    }
#endif


    view.whitePoint.X = whitePointD50.x;
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;        // average viewing cabinet
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );

	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
                cmsJCh jch;
                cmsCIEXYZ xyz;
				
				// setup coordinates based on image position
				jch.J = 100.0 * 2.0 * (0.5 - radius) + 0.1;	// white center to not-quite-black outside
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);        // 0..360
				jch.C = 95.25;		// closer match to LCH circles  (127.0 * 0.75)

                cmsCIECAM16Reverse( cms, &jch, &xyz );
				
				myXYZ.x = xyz.X;
				myXYZ.y = xyz.Y;
				myXYZ.z = xyz.Z;
                /* some XYZ components can be slightly negative, and still translate to legal LAB */
    
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM16Circle1.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM16Done(cms);
}

/******************************************************************************/

void DoCIECAM16Circle2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = (row-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = (col-(IMAGE_SIZE/2)) / (double)IMAGE_SIZE;
			double radius = hypot( xfraction, yfraction );
			
			if (radius <= 0.5) {
				XYZColor myXYZ;
				LABColor myLAB;
                cmsJCh jch;
                cmsCIEXYZ xyz;
				
				// setup coordinates based on image position
				jch.J = 75.0;
				jch.h = 360.0 * (atan2( yfraction, xfraction ) + M_PI) / (2.0*M_PI);    // 0..360
				jch.C = 127.0 * 2.0 * (0.5 - radius);		// closer match to LCH circles
				
                cmsCIECAM16Reverse( cms, &jch, &xyz );
				
				myXYZ.x = xyz.X;
				myXYZ.y = xyz.Y;
				myXYZ.z = xyz.Z;
				
				ConvertXYZtoLAB( myXYZ, myLAB );
				
                image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
			}
			else {
				// fill with black
                image.SetPixel( col, row, 0, 0.5, 0.5 );
			}
		}
	}
    
    image.WriteTIFF( "CIECAM16Circle2.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM16Done(cms);
}


/******************************************************************************/

void DoHueCircles(double res, int depth)
{
    DoHSLCircle1(res,depth);
    DoHSLCircle2(res,depth);
    DoHSLCircle3(res,depth);
    
    DoHSVCircle1(res,depth);
    DoHSVCircle2(res,depth);
    DoHSVCircle3(res,depth);
    
    DoLCHCircle1(res,depth);
    DoLCHCircle2(res,depth);
    DoLCHCircle3(res,depth);
    
    DoCIECAM97Circle1(res,depth);
    DoCIECAM97Circle2(res,depth);
    
    DoCIECAM02Circle1(res,depth);
    DoCIECAM02Circle2(res,depth);
    
    DoCIECAM16Circle1(res,depth);
    DoCIECAM16Circle2(res,depth);
}

/******************************************************************************/

void DoHSLRect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
            HSLColor	HSL;
            RGBColor	RGB;
			
			// setup coordinates based on image position
			HSL.h = xfraction;
			HSL.s = 192.0 / 255.0;
			HSL.l = (1.0 - yfraction);
			
			// do the conversions
			ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLRect1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSLRect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
            HSLColor	HSL;
            RGBColor	RGB;
			
			// setup coordinates based on image position
			HSL.h = xfraction;
			HSL.s = (1.0 - yfraction);
			HSL.l = 128.0 / 255.0;
			
			// do the conversions
			ConvertHSLtoRGB( HSL, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSLRect2.tif", res, TIFF_MODE_RGB );
	
}

/******************************************************************************/

void DoHSVRect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
            HSVColor	HSV;
            RGBColor	RGB;
			
			// setup coordinates based on image position
			HSV.h = xfraction;
			HSV.s = 192.0 / 255.0;
			HSV.v = (1.0 - yfraction);
			
			// do the conversions
			ConvertHSVtoRGB( HSV, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSVRect1.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoHSVRect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);

    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
            HSVColor	HSV;
            RGBColor	RGB;
			
			// setup coordinates based on image position
			HSV.h = xfraction;
			HSV.s = (1.0 - yfraction);
			HSV.v = 192.0 / 255.0;
			
			// do the conversions
			ConvertHSVtoRGB( HSV, RGB );
            
            image.SetPixel( col, row, RGB.r, RGB.g, RGB.b );
		}
	}
    
    image.WriteTIFF( "HSVRect2.tif", res, TIFF_MODE_RGB );
}

/******************************************************************************/

void DoLCHRect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			double hue, saturation;
            LABColor	LAB;
			
			// setup coordinates based on image position
			hue = 2.0 * M_PI * xfraction;
			saturation = 0.5;
			
			LAB.L = (255.0 * (1.0 - yfraction)) + 0.5;
			
			LAB.a = Pin255( 127.0 * saturation * cos( hue ) + 128.0 );
			LAB.b = Pin255( 127.0 * saturation * sin( hue ) + 128.0 );
			
            image.SetPixel( col, row, LAB.L/255.0, LAB.a/255.0, LAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "LCHRect1.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoLCHRect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			double hue, saturation;
            LABColor	LAB;
			
			// setup coordinates based on image position
			hue = 2.0 * M_PI * xfraction;
			saturation = (1.0 - yfraction);
			
			LAB.L = 0.5 * 255.0;
			
			LAB.a = Pin255( 127.0 * saturation * cos( hue ) + 128.0 );
			LAB.b = Pin255( 127.0 * saturation * sin( hue ) + 128.0 );
			
            image.SetPixel( col, row, LAB.L/255.0, LAB.a/255.0, LAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "LCHRect2.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM97Rect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	// calculate a few useful values
	SCENE scene;
		scene.BackgroundColor.x = whitePointD50.x;  // D50
		scene.BackgroundColor.y = whitePointD50.y;
		scene.BackgroundColor.z = whitePointD50.z;
		scene.BackgroundLuminance = 20.0;
		scene.AdaptingLuminance = 318;		// (cd/m^2)


	VIEW view;
		// average surround, small samples
		view.SurroundImpact = 0.69;
		view.ChromaticInduction = 1.0;
		view.LightnessContrast = 1.0;
		view.AdaptationDegree = 1.0;
	
	
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
			TRISTIMULUS jch, xyz;
			
			// setup coordinates based on image position
			jch.j = 100.0 * (1.0 - yfraction) + 0.1;
			jch.h = 360.0 * xfraction;
			jch.c = 64.0;
			
			xyz = JCH2XYZ( jch, scene, view );
			
			myXYZ.x = xyz.x;
			myXYZ.y = xyz.y;
			myXYZ.z = xyz.z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
				
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM97Rect1.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM97Rect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
	// calculate a few useful values
	SCENE scene;
		scene.BackgroundColor.x = whitePointD50.x;  // D50
		scene.BackgroundColor.y = whitePointD50.y;
		scene.BackgroundColor.z = whitePointD50.z;
		scene.BackgroundLuminance = 20.0;
		scene.AdaptingLuminance = 318;		// (cd/m^2)

	VIEW view;
		// average surround, small samples
		view.SurroundImpact = 0.69;
		view.ChromaticInduction = 1.0;
		view.LightnessContrast = 1.0;
		view.AdaptationDegree = 1.0;
    
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
			TRISTIMULUS jch, xyz;
			
			// setup coordinates based on image position
			jch.j = 50.0;
			jch.h = 360.0 * xfraction;
			jch.c = 127.0 * (1.0 - yfraction);
			
			xyz = JCH2XYZ( jch, scene, view );
			
			myXYZ.x = xyz.x;
			myXYZ.y = xyz.y;
			myXYZ.z = xyz.z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
				
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM97Rect2.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoCIECAM02Rect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM02Init( (cmsContext) 1, &view );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
            cmsJCh jch;
            cmsCIEXYZ xyz;
			
			// setup coordinates based on image position
			jch.J = 100.0 * (1.0 - yfraction) + 0.1;
			jch.h = 360.0 * xfraction;
			jch.C = 64.0;
            
            cmsCIECAM02Reverse( cms, &jch, &xyz );
            
            myXYZ.x = xyz.X;
            myXYZ.y = xyz.Y;
            myXYZ.z = xyz.Z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
            
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM02Rect1.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM02Done(cms);
}

/******************************************************************************/

void DoCIECAM02Rect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM02Init( (cmsContext) 1, &view );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
            cmsJCh jch;
            cmsCIEXYZ xyz;
			
			// setup coordinates based on image position
			jch.J = 50.0;
			jch.h = 360.0 * xfraction;
			jch.C = 127.0 * (1.0 - yfraction);
            
            cmsCIECAM02Reverse( cms, &jch, &xyz );
            
            myXYZ.x = xyz.X;
            myXYZ.y = xyz.Y;
            myXYZ.z = xyz.Z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
				
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM02Rect2.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM02Done(cms);
}

/******************************************************************************/

void DoCIECAM16Rect1(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

#if 0 || DEBUG_CIECAM16
    {
    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );
    
    XYZColor myXYZ;
    LABColor myLAB;
    cmsJCh jch;
    cmsCIEXYZ xyz;
    double xfraction;
    double yfraction;



    // good value in orange, at dark level that causes problems in blue
    /*
    LA = 318
    Yb = 20
    F = 1
    c = 0.68999999999999995
    Nc = 1
    surround = 1
    n = 0.20000000000000001
    Nbb = 1.0003040045593807
    Ncb = 1.0003040045593807
    z = 1.9272135954999579
    FL = 1.1671653197659779
    D = 0.99445011084626333
    
    clr.J = 1.000000
    clr.h = 64.79999
    clr.C = 64
    
t =	1483.8508467247993
e	cmsFloat64Number	0.70001409605457343
A	cmsFloat64Number	1.4469929689089265
p1	cmsFloat64Number	1.814994009770039
p2	cmsFloat64Number	1.7515532101376576
p3	cmsFloat64Number	1.05
hr	cmsFloat64Number	1.1309733552923256
sinhr	cmsFloat64Number	0.90482705246601957
coshr	cmsFloat64Number	0.42577929156507266
p4	cmsFloat64Number	2.0059015751390792
b	cmsFloat64Number	0.25287321837421117
a	cmsFloat64Number	0.11899310424208938
    RGBpa.r = [0]	cmsFloat64Number	0.66443895478850867
    RGBpa.g = [1]	cmsFloat64Number	0.45166907397573175
    RGBpa.b = [2]	cmsFloat64Number	-0.57987546830183012
    
     */
    xfraction = 144 / 800.0;
    yfraction = 792 / 800.0;
    jch.J = 100.0 * (1.0 - yfraction);
    jch.h = 360.0 * xfraction;
    jch.C = 64.0;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    /* xyz = 0.1221, 0.0891, -0.1245 */
    myXYZ.x = xyz.X;
    myXYZ.y = xyz.Y;
    myXYZ.z = xyz.Z;
    ConvertXYZtoLAB( myXYZ, myLAB );
    
    
    
    // good value in lighter blue
    /*
    LA = 318
    Yb = 20
    F = 1
    c = 0.68999999999999995
    Nc = 1
    surround = 1
    n = 0.20000000000000001
    Nbb = 1.0003040045593807
    Ncb = 1.0003040045593807
    z = 1.9272135954999579
    FL = 1.1671653197659779
    D = 0.99445011084626333
    
    clr.J = 33.625
    clr.h = 273.600
    clr.C = 64
    
t	cmsFloat64Number	210.49633106357339
e	cmsFloat64Number	1.1703432649989514
A	cmsFloat64Number	20.348151937488982
p1	cmsFloat64Number	21.39081783100206
p2	cmsFloat64Number	20.646967886504708
p3	cmsFloat64Number	1.05
hr	cmsFloat64Number	4.7752208334564861
sinhr	cmsFloat64Number	-0.99802672842827156
coshr	cmsFloat64Number	0.062790519529313721
p4	cmsFloat64Number	-21.433111179987225
b	cmsFloat64Number	-1.2313647237692278
a	cmsFloat64Number	0.077470901863823743
    RGBpa.r = [0]	cmsFloat64Number	6.541633331494805
    RGBpa.g = [1]	cmsFloat64Number	6.9493690998825848
    RGBpa.b = [2]	cmsFloat64Number	12.28664247265022
     */
    xfraction = 608 / 800.0;
    yfraction = 531 / 800.0;
    jch.J = 100.0 * (1.0 - yfraction);
    jch.h = 360.0 * xfraction;
    jch.C = 64.0;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    /* xyz = 15.697, 13.290, 50.641 */
    myXYZ.x = xyz.X;
    myXYZ.y = xyz.Y;
    myXYZ.z = xyz.Z;
    ConvertXYZtoLAB( myXYZ, myLAB );


    // over saturated and light error in dark saturated blues
    // almost sane
    // error occurs in reverse CAT16, leading to small negative luminance
    /*
    LA = 318
    Yb = 20
    F = 1
    c = 0.68999999999999995
    Nc = 1
    surround = 1
    n = 0.20000000000000001
    Nbb = 1.0003040045593807
    Ncb = 1.0003040045593807
    z = 1.9272135954999579
    FL = 1.1671653197659779
    D = 0.99445011084626333
    
    clr.J = 3.000
    clr.h = 273.6000
    clr.C = 64
    
t	cmsFloat64Number	805.97743892533003
e	cmsFloat64Number	1.1703432649989514
A	cmsFloat64Number	3.3057062807087054
p1	cmsFloat64Number	5.5866187493771164
p2	cmsFloat64Number	3.6097016363438641
p3	cmsFloat64Number	1.05
hr	cmsFloat64Number	4.7752208334564861
sinhr	cmsFloat64Number	-0.99802672842827156
coshr	cmsFloat64Number	0.062790519529313721
p4	cmsFloat64Number	-5.5976644615271223
b	cmsFloat64Number	-3.8726469642669792
a	cmsFloat64Number	0.24364629514771521
    RGBpa.r = [0]	cmsFloat64Number	0.46687448761290584
    RGBpa.g = [1]	cmsFloat64Number	1.7492051043586918
    RGBpa.b = [2]	cmsFloat64Number	18.534951135187207
     */
    xfraction = 608 / 800.0;
    yfraction = 776 / 800.0;
    jch.J = 100.0 * (1.0 - yfraction);
    jch.h = 360.0 * xfraction;
    jch.C = 64.0;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    /* xyz = 19.928, -0.898, 142.619 */
    myXYZ.x = xyz.X;
    myXYZ.y = xyz.Y;
    myXYZ.z = xyz.Z;
    ConvertXYZtoLAB( myXYZ, myLAB );
    /* LAB = 0, 255, 0
        FIXED */
    
    // green reversal at edge in dark saturated blue
    // changing whitepoint doesn't help
    // 0.1 J offset doesn't help
    // error at inverse correlates (RGBpa = 15, -9, -350), and it gets worse after
    /*
    LA = 318
    Yb = 20
    F = 1
    c = 0.68999999999999995
    Nc = 1
    surround = 1
    n = 0.20000000000000001
    Nbb = 1.0003040045593807
    Ncb = 1.0003040045593807
    z = 1.9272135954999579
    FL = 1.1671653197659779
    D = 0.99445011084626333

    clr.J = 2.1249999999999991
    clr.h = 274.94999999999999
    clr.C = 64
    t = 976.167         ????
    e = 1.167499
    clr.A = 2.5505988
    p1 = 4.6014
    p2 = 2.8548
    p3 = 1.05
    hr = 4.798782
    sinhr = -0.99627
    coshr = 0.086286
    p4 = -4.6186387891295722        ????
    clr.b = 80.210818889143724
    clr.a = -6.9470097906385515
    RGBpa.r = 15.168092183643717
    RGBpa.g = -9.5737841036713007   !!!
    RGBpa.b = -358.15153096116057   !!!
     */
    xfraction = 611 / 800.0;
    yfraction = 783 / 800.0;
    jch.J = 100.0 * (1.0 - yfraction);
    jch.h = 360.0 * xfraction;
    jch.C = 64.0;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    /* xyz = -690.446, 71.972, -6385.74 */
    myXYZ.x = xyz.X;
    myXYZ.y = xyz.Y;
    myXYZ.z = xyz.Z;
    ConvertXYZtoLAB( myXYZ, myLAB );
    /* LAB = 224,0,255 */
    
    // dark green reversal at edge in dark saturated blue
    // changing whitepoint doesn't help
    //0.1 J offset doesn't help
    // error at inverse correlates (RGBpa = 0.79, 0.37, -4.21), and it gets worse after
    /*
    LA = 318
    Yb = 20
    F = 1
    c = 0.68999999999999995
    Nc = 1
    surround = 1
    n = 0.20000000000000001
    Nbb = 1.0003040045593807
    Ncb = 1.0003040045593807
    z = 1.9272135954999579
    FL = 1.1671653197659779
    D = 0.99445011084626333
    
    clr.J = 1.0000000000000009
    clr.h = 270.0
    clr.C = 64.0
    t = 1483.85                     ????
    e = 1.1773243567064204
    clr.A = 1.4469929689089265
    p1 = 3.052562322704897
    p2 = 1.7515532101376576
    p3 = 1.05
    hr = 4.7123889803846897
    sinhr = -1
    coshr = -1.8369701987210297E-16
    p4 = -3.052562322704897             ????
    clr.b = 1.0660118245205876
    clr.a = 1.9582319531285512E-16
    RGBpa.r = 0.79310469146489793
    RGBpa.g = 0.37596962969597214
    RGBpa.b = -4.2125160497622094       !!!!
    RGBc.r = 0.13998372802673811
    RGBc.g = 0.055736540327482501
    RGBc.b = -4.709066025774856         !!!!
    XYZ.x = -0.38321825371401863
    XYZ.y = 0.12388136901746533
    XYZ.z = -4.1288234064855516         !!!!
     */
    xfraction = 600 / 800.0;
    yfraction = 792 / 800.0;
    jch.J = 100.0 * (1.0 - yfraction);
    jch.h = 360.0 * xfraction;
    jch.C = 64.0;
    cmsCIECAM16Reverse( cms, &jch, &xyz );
    /* xyz = -0.3832, 0.1238, -4.1288 */
    myXYZ.x = xyz.X;
    myXYZ.y = xyz.Y;
    myXYZ.z = xyz.Z;
    ConvertXYZtoLAB( myXYZ, myLAB );
    /* LAB = 2.85, 107, 207 */

    cmsCIECAM16Done(cms);
    }
#endif


    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
            cmsJCh jch;
            cmsCIEXYZ xyz;
			
			// setup coordinates based on image position
			jch.J = 100.0 * (1.0 - yfraction) + 0.1;
			jch.h = 360.0 * xfraction;
			jch.C = 64.0;
            
            cmsCIECAM16Reverse( cms, &jch, &xyz );
            
            myXYZ.x = xyz.X;
            myXYZ.y = xyz.Y;
            myXYZ.z = xyz.Z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
            
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM16Rect1.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM16Done(cms);
}

/******************************************************************************/

void DoCIECAM16Rect2(double res, int depth)
{
    const int IMAGE_SIZE = (int)(8.0 * res);
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
    
    cmsViewingConditions view;

    view.whitePoint.X = whitePointD50.x;        // D50
    view.whitePoint.Y = whitePointD50.y;
    view.whitePoint.Z = whitePointD50.z;

    view.Yb = 20.0;
    view.La = 318.0;
    view.surround = AVG_SURROUND;
    view.D_value = D_CALCULATE;

    cmsHANDLE cms = cmsCIECAM16Init( (cmsContext) 1, &view );
    
	for ( int row = 0; row < IMAGE_SIZE; ++row ) {
		double yfraction = row / (double)IMAGE_SIZE;
		
		for ( int col = 0; col < IMAGE_SIZE; ++col ) {
			double xfraction = col / (double)IMAGE_SIZE;
			XYZColor myXYZ;
			LABColor myLAB;
            cmsJCh jch;
            cmsCIEXYZ xyz;
			
			// setup coordinates based on image position
			jch.J = 50.0;
			jch.h = 360.0 * xfraction;
			jch.C = 127.0 * (1.0 - yfraction);
            
            cmsCIECAM16Reverse( cms, &jch, &xyz );
            
            myXYZ.x = xyz.X;
            myXYZ.y = xyz.Y;
            myXYZ.z = xyz.Z;
			
			ConvertXYZtoLAB( myXYZ, myLAB );
				
            image.SetPixel( col, row, myLAB.L/255.0, myLAB.a/255.0, myLAB.b/255.0 );
		}
	}
    
    image.WriteTIFF( "CIECAM16Rect2.tif", res, TIFF_MODE_CIELAB );
    
    cmsCIECAM16Done(cms);
}

/******************************************************************************/

void DoHueRectangles(double res, int depth)
{
    DoHSLRect1(res,depth);
    DoHSLRect2(res,depth);
    
    DoHSVRect1(res,depth);
    DoHSVRect2(res,depth);
    
    DoLCHRect1(res,depth);
    DoLCHRect2(res,depth);
    
    DoCIECAM97Rect1(res,depth);
    DoCIECAM97Rect2(res,depth);
    
    DoCIECAM02Rect1(res,depth);
    DoCIECAM02Rect2(res,depth);
    
    DoCIECAM16Rect1(res,depth);
    DoCIECAM16Rect2(res,depth);
}

/******************************************************************************/

void DoFullGamutRGBLAB(void)
{
    const int IMAGE_SIZE = 4096;
    const int depth = 8;
    const double res = 100.0;
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth );
	
	/* 256*256*256 = 16M = 4096x4096 pixels
        could also be done as 256x65536, but that isn't nearly as useful
        and some apps won't open files that wide
     */
	for (int b = 0; b < 256; ++b ) {
		for (int g = 0; g < 256; ++g ) {
			for (int r = 0; r < 256; ++r ) {
                int index = b*65536 + g*256 + r;
                int row = index / 4096;
                int col = index % 4096;
                image.SetPixel( col, row, r/255.0, g/255.0, b/255.0 );
			}
		}
	}
    
    image.WriteTIFF( "AllRGB.tif", res, TIFF_MODE_RGB );
    image.WriteTIFF( "AllLAB.tif", res, TIFF_MODE_CIELAB );
}

/******************************************************************************/

void DoSmallGamutCMYK(void)
{
    const int IMAGE_SIZE = 4096;
    const int depth = 8;
    const double res = 100.0;
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 4 );
	
	/* 64*64*64*64 = 16M = 4096x4096 */
	for (int k = 0; k < 64; ++k ) {
        for (int b = 0; b < 64; ++b ) {
            for (int g = 0; g < 64; ++g ) {
                for (int r = 0; r < 64; ++r ) {
                    int index = k*64*64*64 + b*64*64 + g*64 + r;
                    int row = index / 4096;
                    int col = index % 4096;
                    image.SetPixel( col, row, r/63.0, g/63.0, b/63.0, k/63.0 );
				}
			}
		}
	}
    
    image.WriteTIFF( "SubsampledCMYK.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

// this works, but so far only Photoshop will open the file due to size
void DoFullGamutCMYK(void)
{
    const int IMAGE_SIZE = 65536;
    const int depth = 8;
    const double res = 100.0;
    
    imageBuffer image( IMAGE_SIZE, IMAGE_SIZE, depth, 4 );
	
    // 65536*65536*CMYK = 4 Gig * 4 = 16 Gig raw data, but compresses to 90 Meg with LZW+Predictor
	for (int k = 0; k < 256; ++k ) {
        for (int b = 0; b < 256; ++b ) {
            for (int g = 0; g < 256; ++g ) {
                for (int r = 0; r < 256; ++r ) {
                    int64_t index = int64_t(k)*256*256*256 + int64_t(b)*256*256 + g*256 + r;
                    int64_t row = index / 65536;
                    int64_t col = index % 65536;
                    image.SetPixel( int(col), int(row), r/255.0, g/255.0, b/255.0, k/255.0 );
				}
			}
		}
	}
    
    image.WriteTIFF( "AllCMYK.tif", res, TIFF_MODE_CMYK );
}

/******************************************************************************/

void makeFullGamutFiles(void)
{
    DoFullGamutRGBLAB();
    DoSmallGamutCMYK();
//    DoFullGamutCMYK();        // 90Meg file, 65536x65536  -- Photoshop opens it, but most other apps won't
}

/******************************************************************************/
/******************************************************************************/

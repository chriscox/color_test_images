//
//  tiffiop.h
//  Color Test Images
//
//  Created by Chris Cox on 12/29/23.
//

#ifndef tiffiop_h
#define tiffiop_h

/******************************************************************************/

// changes by ccox to get tiff_lzw.c compiling stand alone

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

#define LZW_SUPPORT 1

#define COMPRESSION_LZW     5

typedef size_t tmsize_t;

/******************************************************************************/

typedef struct TIFF {
    void *tif_data; // compression private data, set by initLZW
    
    int tif_mode;   // open, read, write ????? copied but never used
    
    // output, needs to be set ahead of time
    uint8_t *tif_rawdata;       // raw output data buffer
    size_t tif_rawdatasize;     // raw output data size
    
    // also output, seems like copied from rawdata and rawdatasize
    // then reset in flush routine
    uint8_t *tif_rawcp;     // current pointer in raw buffer
    size_t tif_rawcc;       // bytes unread from fraw buffer
    
    size_t totalCompressedSize;
    FILE *output;
    
    // function pointers
    int (*tif_fixuptags)(struct TIFF *tif);
    int (*tif_setupencode)(struct TIFF *tif);
    int (*tif_preencode)(struct TIFF *tif, uint16_t s);
    int (*tif_postencode)(struct TIFF *tif);
    int (*tif_encoderow)(struct TIFF *tif, uint8_t *bp, tmsize_t cc, uint16_t s);
    int (*tif_encodestrip)(struct TIFF *tif, uint8_t *bp, tmsize_t cc, uint16_t s);
    int (*tif_encodetile)(struct TIFF *tif, uint8_t *bp, tmsize_t cc, uint16_t s);
    void (*tif_cleanup)(struct TIFF *tif);
    
} TIFF;

/******************************************************************************/

#if __cplusplus
extern "C" {
#endif

extern int TIFFInitLZW(TIFF *tif, int scheme);
extern int TIFFFlushData1(TIFF *tif);       // see tiff_write.c

#if __cplusplus
}
#endif

/******************************************************************************/


#endif /* tiffiop_h */
